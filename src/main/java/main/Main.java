package main;

import comsol.MagnetMPH;
import config.ConfigSigma;
import input.Others.T0.Magnet_T0;
import model.domains.Domain;
import server.SigmaServer;
import server.TxtSigmaServer;

import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;

public class Main {

    /**
     * Method executes model building process with SIGMA
     * @param args execution arguments (first element of the array is path to SIGMA config)
     * @throws IOException thrown in case of I/O problems of the input model
     * @throws ScriptException thrown in case of errors in parsing an iron yoke file
     */
    public static void main(String[] args) throws IOException, ScriptException {

        // LVL 1 ******************************************************************************
        // Parse config file
        ConfigSigma cfg = ConfigSigma.parseFileJSON(args[0]);

        // Create output directory if not existing
        String parentDirectory = new File(cfg.getOutputModelPath()).getParent();
        if (!new File(parentDirectory).isDirectory()) {
            new File(parentDirectory).mkdir();
        }

        // Magnet construction in Java
        Magnet_T0 magnet = new Magnet_T0();

        // LVL 2 ******************************************************************************
        // Magnet construction in COMSOL
        SigmaServer srv = new TxtSigmaServer(cfg.getOutputModelPath(), cfg.getComsolBatchPath());
        Domain[] domains = magnet.getDomains();
        srv.connect(cfg.getComsolBatchPath());
        MagnetMPH m = new MagnetMPH(cfg, srv);
        m.buildMPH(domains);
        srv.disconnect();
    }
}
