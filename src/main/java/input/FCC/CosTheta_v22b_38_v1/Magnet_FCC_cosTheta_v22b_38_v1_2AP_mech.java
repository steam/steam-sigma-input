package input.FCC.CosTheta_v22b_38_v1;


import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.AirFarFieldDomain;
import model.domains.database.CoilDomain;
import model.domains.database.IronDomain;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

import java.io.FileNotFoundException;

/**
 * Created by mprioli on 08/02/2018.
 */
public class Magnet_FCC_cosTheta_v22b_38_v1_2AP_mech {

    private final Domain[] domains;

    public Magnet_FCC_cosTheta_v22b_38_v1_2AP_mech() throws FileNotFoundException {
        //Should you need a dedicated BH characteristics for your iron yoke, please update the database under src/main/resources
        String bhCurveDatabasePath = Magnet_FCC_cosTheta_v22b_38_v1_2AP_mech.class.getClassLoader().getResource("bh-curve-database.txt").getFile();

        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("coilR", MatDatabase.MAT_COIL, coilR()),
                new CoilDomain("coilL", MatDatabase.MAT_COIL, coilL()),
                new IronDomain("ironYoke", bhCurveDatabasePath, "BH_SIGMA", iron_yoke()),
                new AirDomain("alShell", MatDatabase.MAT_AIR, al_shell()),
                new AirDomain("ssShell", MatDatabase.MAT_AIR, ss_shell()),
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1R = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar1L = ar1R.mirrorY();

        // ELEMENTS
        Element el1R = new Element("AIR_El1R", ar1R);
        Element el1L = new Element("AIR_El1L", ar1L);

        return new Element[]{el1R, el1L};
    }

    public Element[] airFarField() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        // AREAS
        Area ar1R_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});
        Area ar1L_far = ar1R_far.mirrorY();

        // ELEMENTS
        Element el1R_far = new Element("FAR_El1R", ar1R_far);
        Element el1L_far = new Element("FAR_El1L", ar1L_far);

        return new Element[]{el1R_far, el1L_far};
    }

    public Coil coilR() {

        double xAperture = 0.102;

        Point kp11 = Point.ofCartesian(0.125335-xAperture, 0.00897011);
        Point kp12 = Point.ofCartesian(0.126999-xAperture, 0.000249988);
        Point kp13 = Point.ofCartesian(0.140499-xAperture, 0.000249988);
        Point kp14 = Point.ofCartesian(0.13932-xAperture, 0.0094585);

        Point kp21 = Point.ofCartesian(0.118553-xAperture, 0.0187351);
        Point kp22 = Point.ofCartesian(0.125013-xAperture, 0.00976828);
        Point kp23 = Point.ofCartesian(0.137157-xAperture, 0.0156916);
        Point kp24 = Point.ofCartesian(0.130899-xAperture, 0.0254385);

        Point kp31 = Point.ofCartesian(0.114138-xAperture, 0.0218559);
        Point kp32 = Point.ofCartesian(0.117801-xAperture, 0.0193736);
        Point kp33 = Point.ofCartesian(0.127021-xAperture, 0.029261);
        Point kp34 = Point.ofCartesian(0.123322-xAperture, 0.0320564);

        Point kp41 = Point.ofCartesian(0.108228-xAperture, 0.0242118);
        Point kp42 = Point.ofCartesian(0.112367-xAperture, 0.022749);
        Point kp43 = Point.ofCartesian(0.117858-xAperture, 0.0350822);
        Point kp44 = Point.ofCartesian(0.113541-xAperture, 0.0367294);

        Point kp51 = Point.ofCartesian(0.137808-xAperture, 0.0154534);
        Point kp52 = Point.ofCartesian(0.140999-xAperture, 0.000249998);
        Point kp53 = Point.ofCartesian(0.154499-xAperture, 0.000249998);
        Point kp54 = Point.ofCartesian(0.1519-xAperture, 0.0163154);

        Point kp61 = Point.ofCartesian(0.122324-xAperture, 0.0332857);
        Point kp62 = Point.ofCartesian(0.137053-xAperture, 0.0170965);
        Point kp63 = Point.ofCartesian(0.148213-xAperture, 0.0249111);
        Point kp64 = Point.ofCartesian(0.13308-xAperture, 0.0423115);

        Point kp71 = Point.ofCartesian(0.116906-xAperture, 0.0360389);
        Point kp72 = Point.ofCartesian(0.120908-xAperture, 0.0341102);
        Point kp73 = Point.ofCartesian(0.128887-xAperture, 0.0450927);
        Point kp74 = Point.ofCartesian(0.124789-xAperture, 0.0472962);

        Point kp81 = Point.ofCartesian(0.147822-xAperture, 0.0266337);
        Point kp82 = Point.ofCartesian(0.154999-xAperture, 0.000249996);
        Point kp83 = Point.ofCartesian(0.1693-xAperture, 0.000249996);
        Point kp84 = Point.ofCartesian(0.162734-xAperture, 0.0289941);

        Point kp91 = Point.ofCartesian(0.1388-xAperture, 0.038141);
        Point kp92 = Point.ofCartesian(0.147286-xAperture, 0.0275345);
        Point kp93 = Point.ofCartesian(0.159673-xAperture, 0.034686);
        Point kp94 = Point.ofCartesian(0.150776-xAperture, 0.0463706);

        Point kp101 = Point.ofCartesian(0.128275-xAperture, 0.0460284);
        Point kp102 = Point.ofCartesian(0.130866-xAperture, 0.0444495);
        Point kp103 = Point.ofCartesian(0.140112-xAperture, 0.0554686);
        Point kp104 = Point.ofCartesian(0.137368-xAperture, 0.0572571);

        Point kp111 = Point.ofCartesian(0.156374-xAperture, 0.040501);
        Point kp112 = Point.ofCartesian(0.1698-xAperture, 0.000251292);
        Point kp113 = Point.ofCartesian(0.1841-xAperture, 0.000251292);
        Point kp114 = Point.ofCartesian(0.171191-xAperture, 0.044193);

        Point kp121 = Point.ofCartesian(0.143558-xAperture, 0.0535702);
        Point kp122 = Point.ofCartesian(0.1555-xAperture, 0.0416485);
        Point kp123 = Point.ofCartesian(0.167982-xAperture, 0.048855);
        Point kp124 = Point.ofCartesian(0.155619-xAperture, 0.0621722);


        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, kp0);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, kp0);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, kp0);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, kp0);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, kp0);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Arc ln51 = Arc.ofEndPointsCenter(kp52, kp51, kp0);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Arc ln53 = Arc.ofEndPointsCenter(kp53, kp54, kp0);
        Line ln54 = Line.ofEndPoints(kp51, kp54);

        Arc ln61 = Arc.ofEndPointsCenter(kp62, kp61, kp0);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Arc ln63 = Arc.ofEndPointsCenter(kp63, kp64, kp0);
        Line ln64 = Line.ofEndPoints(kp61, kp64);

        Arc ln71 = Arc.ofEndPointsCenter(kp72, kp71, kp0);
        Line ln72 = Line.ofEndPoints(kp72, kp73);
        Arc ln73 = Arc.ofEndPointsCenter(kp73, kp74, kp0);
        Line ln74 = Line.ofEndPoints(kp71, kp74);

        Arc ln81 = Arc.ofEndPointsCenter(kp82, kp81, kp0);
        Line ln82 = Line.ofEndPoints(kp82, kp83);
        Arc ln83 = Arc.ofEndPointsCenter(kp83, kp84, kp0);
        Line ln84 = Line.ofEndPoints(kp81, kp84);

        Arc ln91 = Arc.ofEndPointsCenter(kp92, kp91, kp0);
        Line ln92 = Line.ofEndPoints(kp92, kp93);
        Arc ln93 = Arc.ofEndPointsCenter(kp93, kp94, kp0);
        Line ln94 = Line.ofEndPoints(kp91, kp94);

        Arc ln101 = Arc.ofEndPointsCenter(kp102, kp101, kp0);
        Line ln102 = Line.ofEndPoints(kp102, kp103);
        Arc ln103 = Arc.ofEndPointsCenter(kp103, kp104, kp0);
        Line ln104 = Line.ofEndPoints(kp101, kp104);

        Arc ln111 = Arc.ofEndPointsCenter(kp112, kp111, kp0);
        Line ln112 = Line.ofEndPoints(kp112, kp113);
        Arc ln113 = Arc.ofEndPointsCenter(kp113, kp114, kp0);
        Line ln114 = Line.ofEndPoints(kp111, kp114);

        Arc ln121 = Arc.ofEndPointsCenter(kp122, kp121, kp0);
        Line ln122 = Line.ofEndPoints(kp122, kp123);
        Arc ln123 = Arc.ofEndPointsCenter(kp123, kp124, kp0);
        Line ln124 = Line.ofEndPoints(kp121, kp124);

        // positive winding cross section, pole 1:

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
        Area ha17p = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
        Area ha18p = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});
        Area ha19p = Area.ofHyperLines(new HyperLine[]{ln91, ln92, ln93, ln94});
        Area ha110p = Area.ofHyperLines(new HyperLine[]{ln101, ln102, ln103, ln104});
        Area ha111p = Area.ofHyperLines(new HyperLine[]{ln111, ln112, ln113, ln114});
        Area ha112p = Area.ofHyperLines(new HyperLine[]{ln121, ln122, ln123, ln124});

        // negative winding cross section, pole 1:

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();
        Area ha15n = ha15p.mirrorY();
        Area ha16n = ha16p.mirrorY();
        Area ha17n = ha17p.mirrorY();
        Area ha18n = ha18p.mirrorY();
        Area ha19n = ha19p.mirrorY();
        Area ha110n = ha110p.mirrorY();
        Area ha111n = ha111p.mirrorY();
        Area ha112n = ha112p.mirrorY();

        Cable cableHF = new CableHF_FCC_cosTheta_v22b_38_v1();
        Cable cableLF = new CableLF_FCC_cosTheta_v22b_38_v1();

        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 4, 4, cableHF);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n},  new int[]{+1, -1}, 5, 5, cableHF);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w15 = Winding.ofAreas(new Area[]{ha15p, ha15n},  new int[]{+1, -1}, 7, 7, cableHF);
        Winding w16 = Winding.ofAreas(new Area[]{ha16p, ha16n},  new int[]{+1, -1}, 10, 10, cableHF);
        Winding w17 = Winding.ofAreas(new Area[]{ha17p, ha17n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w18 = Winding.ofAreas(new Area[]{ha18p, ha18n},  new int[]{+1, -1}, 18, 18, cableLF);
        Winding w19 = Winding.ofAreas(new Area[]{ha19p, ha19n},  new int[]{+1, -1}, 9, 9, cableLF);
        Winding w110 = Winding.ofAreas(new Area[]{ha110p, ha110n},  new int[]{+1, -1}, 2, 2, cableLF);
        Winding w111 = Winding.ofAreas(new Area[]{ha111p, ha111n},  new int[]{+1, -1}, 28, 28, cableLF);
        Winding w112 = Winding.ofAreas(new Area[]{ha112p, ha112n},  new int[]{+1, -1}, 11, 11, cableLF);

        w15 = w15.reverseWindingDirection();
        w16 = w16.reverseWindingDirection();
        w17 = w17.reverseWindingDirection();
        w111 = w111.reverseWindingDirection();
        w112 = w112.reverseWindingDirection();

        Winding[] windings = {w11, w12, w13, w14, w17, w16, w15, w18, w19, w110, w112, w111};

        Pole p1_ap1 = Pole.ofWindings(windings);
//        Pole p1_ap2 = p1_ap1.mirrorY();

        p1_ap1 = p1_ap1.translate(xAperture, 0);
//        p1_ap2 = p1_ap2.translate(-xAperture, 0);

//        Pole[] poles = {p1_ap1, p1_ap2};
        Pole[] poles = {p1_ap1};

        return Coil.ofPoles(poles);
    }

    public Coil coilL() {

        double xAperture = 0.102;

        Point kp11 = Point.ofCartesian(0.125335-xAperture, 0.00897011);
        Point kp12 = Point.ofCartesian(0.126999-xAperture, 0.000249988);
        Point kp13 = Point.ofCartesian(0.140499-xAperture, 0.000249988);
        Point kp14 = Point.ofCartesian(0.13932-xAperture, 0.0094585);

        Point kp21 = Point.ofCartesian(0.118553-xAperture, 0.0187351);
        Point kp22 = Point.ofCartesian(0.125013-xAperture, 0.00976828);
        Point kp23 = Point.ofCartesian(0.137157-xAperture, 0.0156916);
        Point kp24 = Point.ofCartesian(0.130899-xAperture, 0.0254385);

        Point kp31 = Point.ofCartesian(0.114138-xAperture, 0.0218559);
        Point kp32 = Point.ofCartesian(0.117801-xAperture, 0.0193736);
        Point kp33 = Point.ofCartesian(0.127021-xAperture, 0.029261);
        Point kp34 = Point.ofCartesian(0.123322-xAperture, 0.0320564);

        Point kp41 = Point.ofCartesian(0.108228-xAperture, 0.0242118);
        Point kp42 = Point.ofCartesian(0.112367-xAperture, 0.022749);
        Point kp43 = Point.ofCartesian(0.117858-xAperture, 0.0350822);
        Point kp44 = Point.ofCartesian(0.113541-xAperture, 0.0367294);

        Point kp51 = Point.ofCartesian(0.137808-xAperture, 0.0154534);
        Point kp52 = Point.ofCartesian(0.140999-xAperture, 0.000249998);
        Point kp53 = Point.ofCartesian(0.154499-xAperture, 0.000249998);
        Point kp54 = Point.ofCartesian(0.1519-xAperture, 0.0163154);

        Point kp61 = Point.ofCartesian(0.122324-xAperture, 0.0332857);
        Point kp62 = Point.ofCartesian(0.137053-xAperture, 0.0170965);
        Point kp63 = Point.ofCartesian(0.148213-xAperture, 0.0249111);
        Point kp64 = Point.ofCartesian(0.13308-xAperture, 0.0423115);

        Point kp71 = Point.ofCartesian(0.116906-xAperture, 0.0360389);
        Point kp72 = Point.ofCartesian(0.120908-xAperture, 0.0341102);
        Point kp73 = Point.ofCartesian(0.128887-xAperture, 0.0450927);
        Point kp74 = Point.ofCartesian(0.124789-xAperture, 0.0472962);

        Point kp81 = Point.ofCartesian(0.147822-xAperture, 0.0266337);
        Point kp82 = Point.ofCartesian(0.154999-xAperture, 0.000249996);
        Point kp83 = Point.ofCartesian(0.1693-xAperture, 0.000249996);
        Point kp84 = Point.ofCartesian(0.162734-xAperture, 0.0289941);

        Point kp91 = Point.ofCartesian(0.1388-xAperture, 0.038141);
        Point kp92 = Point.ofCartesian(0.147286-xAperture, 0.0275345);
        Point kp93 = Point.ofCartesian(0.159673-xAperture, 0.034686);
        Point kp94 = Point.ofCartesian(0.150776-xAperture, 0.0463706);

        Point kp101 = Point.ofCartesian(0.128275-xAperture, 0.0460284);
        Point kp102 = Point.ofCartesian(0.130866-xAperture, 0.0444495);
        Point kp103 = Point.ofCartesian(0.140112-xAperture, 0.0554686);
        Point kp104 = Point.ofCartesian(0.137368-xAperture, 0.0572571);

        Point kp111 = Point.ofCartesian(0.156374-xAperture, 0.040501);
        Point kp112 = Point.ofCartesian(0.1698-xAperture, 0.000251292);
        Point kp113 = Point.ofCartesian(0.1841-xAperture, 0.000251292);
        Point kp114 = Point.ofCartesian(0.171191-xAperture, 0.044193);

        Point kp121 = Point.ofCartesian(0.143558-xAperture, 0.0535702);
        Point kp122 = Point.ofCartesian(0.1555-xAperture, 0.0416485);
        Point kp123 = Point.ofCartesian(0.167982-xAperture, 0.048855);
        Point kp124 = Point.ofCartesian(0.155619-xAperture, 0.0621722);


        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, kp0);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, kp0);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, kp0);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, kp0);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, kp0);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Arc ln51 = Arc.ofEndPointsCenter(kp52, kp51, kp0);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Arc ln53 = Arc.ofEndPointsCenter(kp53, kp54, kp0);
        Line ln54 = Line.ofEndPoints(kp51, kp54);

        Arc ln61 = Arc.ofEndPointsCenter(kp62, kp61, kp0);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Arc ln63 = Arc.ofEndPointsCenter(kp63, kp64, kp0);
        Line ln64 = Line.ofEndPoints(kp61, kp64);

        Arc ln71 = Arc.ofEndPointsCenter(kp72, kp71, kp0);
        Line ln72 = Line.ofEndPoints(kp72, kp73);
        Arc ln73 = Arc.ofEndPointsCenter(kp73, kp74, kp0);
        Line ln74 = Line.ofEndPoints(kp71, kp74);

        Arc ln81 = Arc.ofEndPointsCenter(kp82, kp81, kp0);
        Line ln82 = Line.ofEndPoints(kp82, kp83);
        Arc ln83 = Arc.ofEndPointsCenter(kp83, kp84, kp0);
        Line ln84 = Line.ofEndPoints(kp81, kp84);

        Arc ln91 = Arc.ofEndPointsCenter(kp92, kp91, kp0);
        Line ln92 = Line.ofEndPoints(kp92, kp93);
        Arc ln93 = Arc.ofEndPointsCenter(kp93, kp94, kp0);
        Line ln94 = Line.ofEndPoints(kp91, kp94);

        Arc ln101 = Arc.ofEndPointsCenter(kp102, kp101, kp0);
        Line ln102 = Line.ofEndPoints(kp102, kp103);
        Arc ln103 = Arc.ofEndPointsCenter(kp103, kp104, kp0);
        Line ln104 = Line.ofEndPoints(kp101, kp104);

        Arc ln111 = Arc.ofEndPointsCenter(kp112, kp111, kp0);
        Line ln112 = Line.ofEndPoints(kp112, kp113);
        Arc ln113 = Arc.ofEndPointsCenter(kp113, kp114, kp0);
        Line ln114 = Line.ofEndPoints(kp111, kp114);

        Arc ln121 = Arc.ofEndPointsCenter(kp122, kp121, kp0);
        Line ln122 = Line.ofEndPoints(kp122, kp123);
        Arc ln123 = Arc.ofEndPointsCenter(kp123, kp124, kp0);
        Line ln124 = Line.ofEndPoints(kp121, kp124);

        // positive winding cross section, pole 1:

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
        Area ha17p = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
        Area ha18p = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});
        Area ha19p = Area.ofHyperLines(new HyperLine[]{ln91, ln92, ln93, ln94});
        Area ha110p = Area.ofHyperLines(new HyperLine[]{ln101, ln102, ln103, ln104});
        Area ha111p = Area.ofHyperLines(new HyperLine[]{ln111, ln112, ln113, ln114});
        Area ha112p = Area.ofHyperLines(new HyperLine[]{ln121, ln122, ln123, ln124});

        // negative winding cross section, pole 1:

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();
        Area ha15n = ha15p.mirrorY();
        Area ha16n = ha16p.mirrorY();
        Area ha17n = ha17p.mirrorY();
        Area ha18n = ha18p.mirrorY();
        Area ha19n = ha19p.mirrorY();
        Area ha110n = ha110p.mirrorY();
        Area ha111n = ha111p.mirrorY();
        Area ha112n = ha112p.mirrorY();

        Cable cableHF = new CableHF_FCC_cosTheta_v22b_38_v1();
        Cable cableLF = new CableLF_FCC_cosTheta_v22b_38_v1();

        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 4, 4, cableHF);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n},  new int[]{+1, -1}, 5, 5, cableHF);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w15 = Winding.ofAreas(new Area[]{ha15p, ha15n},  new int[]{+1, -1}, 7, 7, cableHF);
        Winding w16 = Winding.ofAreas(new Area[]{ha16p, ha16n},  new int[]{+1, -1}, 10, 10, cableHF);
        Winding w17 = Winding.ofAreas(new Area[]{ha17p, ha17n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w18 = Winding.ofAreas(new Area[]{ha18p, ha18n},  new int[]{+1, -1}, 18, 18, cableLF);
        Winding w19 = Winding.ofAreas(new Area[]{ha19p, ha19n},  new int[]{+1, -1}, 9, 9, cableLF);
        Winding w110 = Winding.ofAreas(new Area[]{ha110p, ha110n},  new int[]{+1, -1}, 2, 2, cableLF);
        Winding w111 = Winding.ofAreas(new Area[]{ha111p, ha111n},  new int[]{+1, -1}, 28, 28, cableLF);
        Winding w112 = Winding.ofAreas(new Area[]{ha112p, ha112n},  new int[]{+1, -1}, 11, 11, cableLF);

        w15 = w15.reverseWindingDirection();
        w16 = w16.reverseWindingDirection();
        w17 = w17.reverseWindingDirection();
        w111 = w111.reverseWindingDirection();
        w112 = w112.reverseWindingDirection();

        Winding[] windings = {w11, w12, w13, w14, w17, w16, w15, w18, w19, w110, w112, w111};

        Pole p1_ap1 = Pole.ofWindings(windings);
        Pole p1_ap2 = p1_ap1.mirrorY();

        p1_ap2 = p1_ap2.translate(-xAperture, 0);

        Pole[] poles = {p1_ap2};

        return Coil.ofPoles(poles);
    }

    public Element[] iron_yoke() {

        //file generated by Xhermes 10.0
        //HyperMesh;
        //COMMENTS
        //''

        //VARIABLES
        double ang      = Math.PI/180;
        double oradius  = 300e-3;
        double padthick = 2e-3;
        double holerad  = 52.5e-3;
        double apcenter = 102e-3;
        double holecent = 235e-3;
        double padx     = 203.315e-3;
        double pady     = 115e-3;

        //NODES
        Point kp1  = Point.ofCartesian(padx+padthick,0);
        Point kp2  = Point.ofCartesian(oradius,0);
        Point kp3  = Point.ofPolar(oradius, Math.atan((pady+padthick/2)/(padx+padthick)));
        Point kp4  = Point.ofCartesian(padx+padthick,pady+padthick/2);
        Point kp5  = Point.ofCartesian(0,pady+padthick);
        Point kp6  = Point.ofCartesian(padx+padthick/2,pady+padthick);
        Point kp7  = Point.ofPolar(oradius, Math.atan((pady+padthick)/(padx+padthick/2)));
        Point kp8  = Point.ofCartesian(0,oradius);
        Point kp9  = Point.ofCartesian(0,holecent+holerad);
        Point kp10 = Point.ofCartesian(0,holecent-holerad);
        Point kp11 = Point.ofCartesian(holerad,holecent);

        //LINES
        Line ln1  = Line.ofEndPoints(kp1,kp2);
        Arc ln2  = Arc.ofEndPointsRadius(kp3,kp2,oradius);
        Line ln3  = Line.ofEndPoints(kp4,kp3);
        Line ln4  = Line.ofEndPoints(kp1,kp4);
        Line ln5  = Line.ofEndPoints(kp5,kp6);
        Line ln6  = Line.ofEndPoints(kp6,kp7);
        Arc ln7  = Arc.ofEndPointsRadius(kp8,kp7,oradius);
        Line ln8  = Line.ofEndPoints(kp8,kp9);
        Arc ln9  = Arc.ofEndPointsRadius(kp9,kp11,holerad);
        Arc ln10 = Arc.ofEndPointsRadius(kp11,kp10,holerad);
        Line ln11 = Line.ofEndPoints(kp10,kp5);

        //AREAS
        Area ar1R = Area.ofHyperLines(new HyperLine[]{ln1,ln2,ln3,ln4});
        Area ar2R = Area.ofHyperLines(new HyperLine[]{ln5,ln6,ln7,ln8,ln9,ln10,ln11});
        Area ar1L = ar1R.mirrorY();
        Area ar2L = ar2R.mirrorY();

        // ELEMENTS
        Element el1R = new Element("IY_El1R", ar1R);
        Element el2R = new Element("IY_El2R", ar2R);
        Element el1L = new Element("IY_El1L", ar1L);
        Element el2L = new Element("IY_El2L", ar2L);

        return new Element[]{el1R, el2R, el1L, el2L};
    }

    private Element[] al_shell(){
        double thickShell = 50 * 1e-3;
        double innerRadius  = 300e-3;

        Point kpins8    = Point.ofCartesian(0,0);
        Point kpShell_1_in  = Point.ofCartesian(innerRadius,0);
        Point kpShell_1_out = Point.ofCartesian(innerRadius+thickShell,0);
        Point kpShell_2_in  = Point.ofCartesian(0,innerRadius);
        Point kpShell_2_out = Point.ofCartesian(0,innerRadius+thickShell);

        Line lnShell1 = Line.ofEndPoints(kpShell_1_in,kpShell_1_out);
        Arc lnShell2 = Arc.ofEndPointsCenter(kpShell_1_out,kpShell_2_out,kpins8);
        Line lnShell3 = Line.ofEndPoints(kpShell_2_out,kpShell_2_in);
        Arc lnShell4 = Arc.ofEndPointsCenter(kpShell_2_in,kpShell_1_in,kpins8);

        Area arShellR = Area.ofHyperLines(new HyperLine[]{lnShell1,lnShell2,lnShell3,lnShell4});
        Area arShellL = arShellR.mirrorY();

        Element el1R = new Element("AL_SHELL_El1R", arShellR);
        Element el1L = new Element("AL_SHELL_El1L", arShellL);

        return new Element[] {el1R, el1L};
    }

    private Element[] ss_shell(){
        double thickShell = 20 * 1e-3;
        double innerRadius  = 350e-3;

        Point kpins8    = Point.ofCartesian(0,0);
        Point kpShell_1_in  = Point.ofCartesian(innerRadius,0);
        Point kpShell_1_out = Point.ofCartesian(innerRadius+thickShell,0);
        Point kpShell_2_in  = Point.ofCartesian(0,innerRadius);
        Point kpShell_2_out = Point.ofCartesian(0,innerRadius+thickShell);

        Line lnShell1 = Line.ofEndPoints(kpShell_1_in,kpShell_1_out);
        Arc lnShell2 = Arc.ofEndPointsCenter(kpShell_1_out,kpShell_2_out,kpins8);
        Line lnShell3 = Line.ofEndPoints(kpShell_2_out,kpShell_2_in);
        Arc lnShell4 = Arc.ofEndPointsCenter(kpShell_2_in,kpShell_1_in,kpins8);

        Area arShellR = Area.ofHyperLines(new HyperLine[]{lnShell1,lnShell2,lnShell3,lnShell4});
        Area arShellL = arShellR.mirrorY();

        Element el1R = new Element("SS_SHELL_El1R", arShellR);
        Element el1L = new Element("SS_SHELL_El1L", arShellL);

        return new Element[] {el1R, el1L};
    }

}
