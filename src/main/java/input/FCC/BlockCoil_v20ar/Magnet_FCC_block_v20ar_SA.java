package input.FCC.BlockCoil_v20ar;

import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 17/10/2016.
 */
public class Magnet_FCC_block_v20ar_SA {

    private final Domain[] domains;

    public Magnet_FCC_block_v20ar_SA() {


        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("coil", MatDatabase.MAT_COIL, coil()),
                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, iron_yoke()),
                new HoleDomain("holesYoke", MatDatabase.MAT_AIR, holes_yoke())
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);

        return new Element[]{el1};
    }

    public Element[] airFarField() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        // AREAS
        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});

        // ELEMENTS
        Element el1_far = new Element("FAR_El1", ar1_far);

        return new Element[]{el1_far};
    }

    public Coil coil() {
        Point kp11 = Point.ofCartesian(38.75e-3, 15.1e-3);
        Point kp12 = Point.ofCartesian(26.75e-3, 15.1e-3);
        Point kp13 = Point.ofCartesian(26.75e-3, 1.75e-3);
        Point kp14 = Point.ofCartesian(38.75e-3, 1.75e-3);

        Point kp21 = Point.ofCartesian(38.75e-3, 28.95e-3);
        Point kp22 = Point.ofCartesian(26.75e-3, 28.95e-3);
        Point kp23 = Point.ofCartesian(26.75e-3, 15.6e-3);
        Point kp24 = Point.ofCartesian(38.75e-3, 15.6e-3);

        Point kp31 = Point.ofCartesian(37.2e-3, 42.8e-3);
        Point kp32 = Point.ofCartesian(13.2e-3, 42.8e-3);
        Point kp33 = Point.ofCartesian(13.2e-3, 29.45e-3);
        Point kp34 = Point.ofCartesian(37.2e-3, 29.45e-3);

        Point kp41 = Point.ofCartesian(37.2e-3, 56.65e-3);
        Point kp42 = Point.ofCartesian(13.2e-3, 56.65e-3);
        Point kp43 = Point.ofCartesian(13.2e-3, 43.3e-3);
        Point kp44 = Point.ofCartesian(37.2e-3, 43.3e-3);

        Point kp51 = Point.ofCartesian(66.65e-3, 15.1e-3);
        Point kp52 = Point.ofCartesian(38.75e-3, 15.1e-3);
        Point kp53 = Point.ofCartesian(38.75e-3, 1.75e-3);
        Point kp54 = Point.ofCartesian(66.65e-3, 1.75e-3);

        Point kp61 = Point.ofCartesian(66.65e-3, 28.95e-3);
        Point kp62 = Point.ofCartesian(38.75e-3, 28.95e-3);
        Point kp63 = Point.ofCartesian(38.75e-3, 15.6e-3);
        Point kp64 = Point.ofCartesian(66.65e-3, 15.6e-3);

        Point kp71 = Point.ofCartesian(66.65e-3, 42.8e-3);
        Point kp72 = Point.ofCartesian(37.2e-3, 42.8e-3);
        Point kp73 = Point.ofCartesian(37.2e-3, 29.45e-3);
        Point kp74 = Point.ofCartesian(66.65e-3, 29.45e-3);

        Point kp81 = Point.ofCartesian(66.65e-3, 56.65e-3);
        Point kp82 = Point.ofCartesian(37.2e-3, 56.65e-3);
        Point kp83 = Point.ofCartesian(37.2e-3, 43.3e-3);
        Point kp84 = Point.ofCartesian(66.65e-3, 43.3e-3);


        Line ln11 = Line.ofEndPoints(kp12, kp11);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Line ln13 = Line.ofEndPoints(kp13, kp14);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Line ln21 = Line.ofEndPoints(kp22, kp21);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Line ln23 = Line.ofEndPoints(kp23, kp24);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Line ln31 = Line.ofEndPoints(kp32, kp31);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Line ln33 = Line.ofEndPoints(kp33, kp34);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Line ln41 = Line.ofEndPoints(kp42, kp41);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Line ln43 = Line.ofEndPoints(kp43, kp44);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Line ln51 = Line.ofEndPoints(kp52, kp51);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Line ln53 = Line.ofEndPoints(kp53, kp54);
        Line ln54 = Line.ofEndPoints(kp51, kp54);

        Line ln61 = Line.ofEndPoints(kp62, kp61);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Line ln63 = Line.ofEndPoints(kp63, kp64);
        Line ln64 = Line.ofEndPoints(kp61, kp64);

        Line ln71 = Line.ofEndPoints(kp72, kp71);
        Line ln72 = Line.ofEndPoints(kp72, kp73);
        Line ln73 = Line.ofEndPoints(kp73, kp74);
        Line ln74 = Line.ofEndPoints(kp71, kp74);

        Line ln81 = Line.ofEndPoints(kp82, kp81);
        Line ln82 = Line.ofEndPoints(kp82, kp83);
        Line ln83 = Line.ofEndPoints(kp83, kp84);
        Line ln84 = Line.ofEndPoints(kp81, kp84);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
        Area ha17p = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
        Area ha18p = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});

        // negative winding cross section, pole 1:

//        Area ha11n = ha11p.mirrorY();
//        Area ha12n = ha12p.mirrorY();
//        Area ha13n = ha13p.mirrorY();
//        Area ha14n = ha14p.mirrorY();
//        Area ha15n = ha15p.mirrorY();
//        Area ha16n = ha16p.mirrorY();
//        Area ha17n = ha17p.mirrorY();
//        Area ha18n = ha18p.mirrorY();

        Cable cableHF = new CableHF_FCC_block_v20ar();
        Cable cableLF = new CableLF_FCC_block_v20ar();
//
//        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, 5, 2, cableHF);
//        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n}, 5, 2, cableHF);
//        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n}, 10, 2, cableHF);
//        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n}, 10, 2, cableHF);
//        Winding w15 = Winding.ofAreas(new Area[]{ha15p, ha15n}, 18, 2, new CableLF_FCC_block_v26cmag());
//        Winding w16 = Winding.ofAreas(new Area[]{ha16p, ha16n}, 18, 2, new CableLF_FCC_block_v26cmag());
//        Winding w17 = Winding.ofAreas(new Area[]{ha17p, ha17n}, 19, 2, new CableLF_FCC_block_v26cmag());
//        Winding w18 = Winding.ofAreas(new Area[]{ha18p, ha18n}, 19, 2, new CableLF_FCC_block_v26cmag());

        Winding w11 = Winding.ofAreas(new Area[]{ha11p}, new int[]{+1}, 5, 5, cableHF);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p}, new int[]{+1}, 5, 5, cableHF);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p}, new int[]{+1}, 10, 10, cableHF);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p}, new int[]{+1}, 10, 10, cableHF);
        Winding w15 = Winding.ofAreas(new Area[]{ha15p}, new int[]{+1}, 18, 18, cableLF);
        Winding w16 = Winding.ofAreas(new Area[]{ha16p}, new int[]{+1}, 18, 18, cableLF);
        Winding w17 = Winding.ofAreas(new Area[]{ha17p}, new int[]{+1}, 19, 19, cableLF);
        Winding w18 = Winding.ofAreas(new Area[]{ha18p}, new int[]{+1}, 19, 19, cableLF);


        // windings pole 2:
//        Winding w21 = w11.mirrorX();
//        Winding w22 = w12.mirrorX();
//        Winding w23 = w13.mirrorX();
//        Winding w24 = w14.mirrorX();
//        Winding w25 = w15.mirrorX();
//        Winding w26 = w16.mirrorX();
//        Winding w27 = w17.mirrorX();
//        Winding w28 = w18.mirrorX();

        w11 = w11.reverseWindingDirection();
        w13 = w13.reverseWindingDirection();
        w15 = w15.reverseWindingDirection();
        w17 = w17.reverseWindingDirection();

//        w22 = w22.reverseWindingDirection();
//        w24 = w24.reverseWindingDirection();
//        w26 = w26.reverseWindingDirection();
//        w28 = w28.reverseWindingDirection();

        Winding[] windings_p1 = {w15, w11, w12, w16, w17, w13, w14, w18};

//        Winding[][] windings= {{w15, w11, w12, w16, w17, w13, w14, w18}, {w28, w24, w23, w27, w26, w22, w21, w25}};

        Pole p1 = Pole.ofWindings(windings_p1);
        Pole[] poles = {p1};

        Coil coil = Coil.ofPoles(poles);

        return coil;
    }

    public Element[] iron_yoke() {
        // VARIABLES// VARIABLES
        double BEAM1 = 125 * 1e-3;
        double XI = 205 * 1e-3 - BEAM1; // bord de la culasse selon l’axe x
        double YI = 63 * 1e-3; //bord de la culasse selon l’axe y,
        double RO = 275 * 1e-3; // rayon extérieur culasse 400 mm en double (275 mm en simple)
        double LO = 95 * 1e-3; // écartement du décrochement dans le pad horizontal
        double AL = 45; // angle du décrochement du pad horizontal
        double BEAMO = 0; // demi-écartement faisceau
        double HO = 27 * 1e-3; // hauteur du décrochement dans le pad horizontal
        double CO = 0.5 * 1e-3; // inutile, j’ai oublié de le retirer
        double L2 = 200 * 1e-3; // écartement des trous dans le pad horizontal
        double H2 = 200 * 1e-3; // hauteur des trous dans le pad horizontal
        double R2 = 20 * 1e-3; // rayon des trous
        double YOKEX = 2.5 * 1e-3; // demi-ouverture de la culasse suivant l’axe vertical
        double PV1T = 6 * 1e-3; // PadVertical1Thickness (épaisseur du pad vertical en acier, dépend de l’épaisseur de la bobine 210 mm et de XI 220 mm   /!\)
        double PV2T = 40 * 1e-3; // PadVertical2Thickness (épaisseur du pad vertical en fer 2)
        double PH1T = 2 * 5 * 1e-3 + 15 * 1e-3; // PadHorizontal1Thickness (hauteur du pad horizontal au niveau du décrochement)
        double KT = 10 * 1e-3; // KeyThickness ( épaisseur des clés)
        double PHSW = 10 * 1e-3; // PadHorizontalShoulderWidth (largueur des épaulettes du pad horizontal)
        double KHXP = 40 * 1e-3; // je n’ai pas utilisé
        double CPYP = 0; // CentralPadYPosition, position du pas central en hauteur, figé à YI, mais peut être modifié

        double pi = Math.PI;
        double degr = pi / 180;

        // NODES
        // Outer Yoke
        Point kp1 = Point.ofCartesian(YOKEX, Math.sqrt(RO * RO - YOKEX * YOKEX));
        Point kp2 = Point.ofCartesian(YOKEX, YI + HO + PH1T + KT);
        Point kp3 = Point.ofCartesian(XI, YI + HO + PH1T + KT);
        Point kp4 = Point.ofCartesian(XI + (PV2T + KT) / Math.sqrt(2), YI + HO + PH1T - PV2T + (PV2T + KT) / Math.sqrt(2));
        Point kp5 = Point.ofCartesian(XI + PV2T + KT, YI + HO + PH1T - PV2T);
        Point kp6 = Point.ofCartesian(XI + PV2T + KT, 0);
        Point kp7 = Point.ofCartesian(RO, 0);
        Point kp8 = Point.ofCartesian(RO / Math.sqrt(2), RO / Math.sqrt(2));

        // Vertical pad
        Point kp9 = Point.ofCartesian(XI, 0);
        Point kp10 = Point.ofCartesian(XI + PV2T, 0);
        Point kp101 = Point.ofCartesian(XI + PV2T, YI);
        Point kp102 = Point.ofCartesian(XI + PV2T + KT / 2, YI);
        Point kp11 = Point.ofCartesian(XI + PV2T + KT / 2, YI + HO + PH1T - PV2T); // [RO @ 45.*degr];
        Point kp12 = Point.ofCartesian(XI + (PV2T + KT / 2) / Math.sqrt(2), YI + HO + PH1T - PV2T + (PV2T + KT / 2) / Math.sqrt(2));
        Point kp13 = Point.ofCartesian(XI, YI + HO + PH1T + KT / 2);

//        // Center pad
//        Point kp14 = Point.ofCartesian(0, CPYP);
//        Point kp15 = Point.ofCartesian(BEAMO - (XI - BEAMO), CPYP);
//        Point kp16 = Point.ofCartesian(BEAMO - (XI - BEAMO), YI + HO + PH1T + KT / 2);
//        Point kp17 = Point.ofCartesian(0, YI + HO + PH1T + KT / 2);

        // Horizontal pad
        Point kp210 = Point.ofCartesian(0, YI + HO);
//        Point kp18 = Point.ofCartesian(BEAMO - (XI - BEAMO) + PV1T / 2, YI);
//        Point kp19 = Point.ofCartesian(BEAMO - LO / 2, YI);
//        Point kp20 = Point.ofCartesian(BEAMO - LO / 2 + HO / Math.tan(AL * degr), YI + HO);
        Point kp21 = Point.ofCartesian(BEAMO + LO / 2 - HO / Math.tan(AL * degr), YI + HO);
        Point kp22 = Point.ofCartesian(BEAMO + LO / 2, YI);
        Point kp23 = Point.ofCartesian(XI - PV1T / 2, YI);
        Point kp24 = Point.ofCartesian(XI - PV1T / 2, YI + HO + PH1T + KT / 2);
        Point kp25 = Point.ofCartesian(XI - PV1T / 2 - PHSW, YI + HO + PH1T + KT / 2);
        Point kp26 = Point.ofCartesian(XI - PV1T / 2 - PHSW, YI + HO + PH1T);
//        Point kp27 = Point.ofCartesian(BEAMO - (XI - PV1T / 2 - PHSW - BEAMO), YI + HO + PH1T);
//        Point kp28 = Point.ofCartesian(BEAMO - (XI - PV1T / 2 - PHSW - BEAMO), YI + HO + PH1T + KT / 2);
//        Point kp29 = Point.ofCartesian(BEAMO - (XI - PV1T / 2 - BEAMO), YI + HO + PH1T + KT / 2);
        Point kp260 = Point.ofCartesian(0, YI + HO + PH1T);

        // LINES
        // Outer Yoke
        Line ln1 = Line.ofEndPoints(kp1, kp2);
        Line ln2 = Line.ofEndPoints(kp2, kp3);
        Arc ln3 = Arc.ofThreePoints(kp3, kp4, kp5);
        Line ln4 = Line.ofEndPoints(kp5, kp6);
        Line ln5 = Line.ofEndPoints(kp6, kp7);
        Arc ln6 = Arc.ofThreePoints(kp7, kp8, kp1);
        // Vertical pad
        Line ln7 = Line.ofEndPoints(kp9, kp10);
        Line ln71 = Line.ofEndPoints(kp10, kp101);
        Line ln72 = Line.ofEndPoints(kp101, kp102);
        Line ln8 = Line.ofEndPoints(kp102, kp11);
        Arc ln9 = Arc.ofThreePoints(kp11, kp12, kp13);
        Line ln10 = Line.ofEndPoints(kp13, kp9);
//        // Center pad
//        Line ln11 = Line.ofEndPoints(kp14, kp15);
//        Line ln12 = Line.ofEndPoints(kp15, kp16);
//        Line ln13 = Line.ofEndPoints(kp16, kp17);
//        Line ln14 = Line.ofEndPoints(kp17, kp14);
//        // Horizontal pad
        Line ln17 = Line.ofEndPoints(kp210, kp21);
//        Line ln15 = Line.ofEndPoints(kp18, kp19);
//        Line ln16 = Line.ofEndPoints(kp19, kp20);
//        Line ln17 = Line.ofEndPoints(kp20, kp21);
        Line ln18 = Line.ofEndPoints(kp21, kp22);
        Line ln19 = Line.ofEndPoints(kp22, kp23);
        Line ln20 = Line.ofEndPoints(kp23, kp24);
        Line ln21 = Line.ofEndPoints(kp24, kp25);
        Line ln22 = Line.ofEndPoints(kp25, kp26);
//        Line ln23 = Line.ofEndPoints(kp26, kp27);
//        Line ln24 = Line.ofEndPoints(kp27, kp28);
//        Line ln25 = Line.ofEndPoints(kp28, kp29);
//        Line ln26 = Line.ofEndPoints(kp29, kp18);
        Line ln23 = Line.ofEndPoints(kp26, kp260);
        Line ln24 = Line.ofEndPoints(kp260, kp210);

        // AREAS
        // Outer yoke
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4, ln5, ln6});
        // Vertical pad
        Area ar2 = Area.ofHyperLines(new HyperLine[]{ln7, ln71, ln72, ln8, ln9, ln10});
//        // Center pad
//        Area ar3 = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        // Horizontal pad
//        Area ar4 = Area.ofHyperLines(new HyperLine[]{ln15, ln16, ln17, ln18, ln19, ln20, ln21, ln22, ln23, ln24, ln25, ln26});
        Area ar4 = Area.ofHyperLines(new HyperLine[]{ln17, ln18, ln19, ln20, ln21, ln22, ln23, ln24});

        // ELEMENTS
        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);
//        Element el3 = new Element("IY_El3", ar3);
        Element el4 = new Element("IY_El4", ar4);

        return new Element[]{el1, el2, el4};
    }

    public Element[] holes_yoke() {
        // VARIABLES
        double BEAMO = 0; // demi-écartement faisceau
        double L2 = 200 * 1e-3; // écartement des trous dans le pad horizontal
        double H2 = 200 * 1e-3; // hauteur des trous dans le pad horizontal
        double R2 = 20 * 1e-3; // rayon des trous

        // POINTS
//        //Hole1
//        Point kp30 = Point.ofCartesian(BEAMO - L1 / 2 - R1, H1);
//        Point kp31 = Point.ofCartesian(BEAMO - L1 / 2, H1 - R1);
//        Point kp32 = Point.ofCartesian(BEAMO - L1 / 2 + R1, H1);
//        Point kp33 = Point.ofCartesian(BEAMO - L1 / 2, H1 + R1);
        //Hole2
        Point kp34 = Point.ofCartesian(BEAMO + L2 / 2 - R2, H2);
        Point kp35 = Point.ofCartesian(BEAMO + L2 / 2, H2 - R2);
        Point kp36 = Point.ofCartesian(BEAMO + L2 / 2 + R2, H2);
        Point kp37 = Point.ofCartesian(BEAMO + L2 / 2, H2 + R2);

        // LINES
//        //Hole1
//        Arc ln27 = Arc.ofThreePoints(kp30, kp31, kp32);
//        Arc ln28 = Arc.ofThreePoints(kp32, kp33, kp30);
        //Hole2
//        Arc ln29 = Arc.ofThreePoints(kp34, kp35, kp36);
//        Arc ln30 = Arc.ofThreePoints(kp36, kp37, kp34); //Not plotting the circle in this way
        Circumference ln29 = Circumference.ofDiameterEndPoints(kp34, kp36);

        // AREAS
//        //Hole1
//        Area ar5 = Area.ofHyperLines(new HyperLine[]{ln27, ln28});
        //Hole2
        Area ar6 = Area.ofHyperLines(new HyperLine[]{ln29});


        // ELEMENTS
//        //Hole1
//        Element ho1 = new Element("IY_HOLE1", ar5);
        //Hole2
        Element ho2 = new Element("IY_HOLE2", ar6);

        return new Element[]{ho2};
    }

}

