package input.FCC.MS;

import input.UtilsUserInput;
import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

//public class Magnet_FCC_MS_1pole {
    /**
     * Created by A. LOUZGUITI on 04/10/2018.
     */
    public class Magnet_FCC_MS_1pole extends UtilsUserInput {

//        public int[] quadrantsRequired = {1};
//        public BoundaryConditions xAxisBC = BoundaryConditions.Neumann;
//        public BoundaryConditions yAxisBC = BoundaryConditions.Neumann;

        private final Domain[] domains;


        public Magnet_FCC_MS_1pole() {

            domains = new Domain[]{

                    new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                    new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()),
//                    new BoundaryConditionsDomain("BC", null, this.xAxisBC, this.yAxisBC, BoundaryConditions()),

                    new CoilDomain("CR", MatDatabase.MAT_COIL, coil().translate(102e-3, 0)),
//                    new CoilDomain("CL", MatDatabase.MAT_COIL, coil().translate(-97e-3, 0)),

//                new WedgeDomain("wedgesCR", MatDatabase.MAT_COPPER, wedgesCoil("CR", 97e-3, 0)),
//                    new WedgeDomain("wedgesCL", MatDatabase.MAT_COPPER, wedgesCoil("CL", -97e-3, 0)),

//                new InsulationDomain("foilCoil1", "poly1", fishBone(coil_ApR, coil_ApR_Name, 97e-3, 0)),
//                new InsulationDomain("foilCoil2", "poly2", fishBone(coil_ApL, coil_ApL_Name, -97e-3, 0)),

                    new IronDomain("IY", MatDatabase.MAT_IRON1, iron_yoke()),
                    //new IronDomain("I", MatDatabase.MAT_IRON2, iron_insert()),
                    //new HoleDomain("HY", MatDatabase.MAT_AIR, holes_yoke()),
                    //new HoleDomain("HI", MatDatabase.MAT_AIR, holes_insert()),
            };
        }

        public Domain[] getDomains() {
            return domains.clone();
        }

        public Element[] air() {
            // POINTS

            double r = 1.0;

            Point kpc = Point.ofCartesian(0, 0);
            Point kp1 = Point.ofCartesian(r, 0);
            Point kp2 = Point.ofCartesian(0, r);

            // LINES
            Line ln1 = Line.ofEndPoints(kpc, kp1);
            Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
            Line ln3 = Line.ofEndPoints(kp2, kpc);

            // AREAS
            Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
            Area ar2 = ar1.mirrorY();
            Area ar3 = ar2.mirrorX();
            Area ar4 = ar1.mirrorX();

            // ELEMENTS
            Element el1 = new Element("AIR_El1", ar1);
            Element el2 = new Element("AIR_El2", ar2);
            Element el3 = new Element("AIR_El3", ar3);
            Element el4 = new Element("AIR_El4", ar4);

            // ELEMENTS DISTRIBUTED OVER QUADRANTS
            Element[] quad1 = {el1};
            Element[] quad2 = {el2};
            Element[] quad3 = {el3};
            Element[] quad4 = {el4};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
            return new Element[]{el1};
        }

        public Element[] airFarField() {
            // POINTS

            double r = 1;

            Point kpc = Point.ofCartesian(0, 0);
            Point kp1 = Point.ofCartesian(r, 0);
            Point kp2 = Point.ofCartesian(0, r);

            Point kp1_far = Point.ofCartesian(r * 1.05, 0);
            Point kp2_far = Point.ofCartesian(0, r * 1.05);

            // LINES
            Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
            Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
            Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
            Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

            Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});
            Area ar2_far = ar1_far.mirrorY();
            Area ar3_far = ar2_far.mirrorX();
            Area ar4_far = ar1_far.mirrorX();

            Element el1_far = new Element("FAR_El1", ar1_far);
            Element el2_far = new Element("FAR_El2", ar2_far);
            Element el3_far = new Element("FAR_El3", ar3_far);
            Element el4_far = new Element("FAR_El4", ar4_far);

            // ELEMENTS DISTRIBUTED OVER QUADRANTS
            Element[] quad1 = {el1_far};
            Element[] quad2 = {el2_far};
            Element[] quad3 = {el3_far};
            Element[] quad4 = {el4_far};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
            return new Element[]{el1_far};
        }

        public Coil coil() {
            Point kp11 = Point.ofCartesian(23.3849e-3, 9.56e-3);
            Point kp12 = Point.ofCartesian(24.9992e-3, 0.2e-3);
            Point kp13 = Point.ofCartesian(26.2392e-3, 0.2e-3);
            Point kp14 = Point.ofCartesian(24.6249e-3, 9.56e-3);

            Point kp21 = Point.ofCartesian(24.6249e-3, 9.56e-3);
            Point kp22 = Point.ofCartesian(26.2392e-3, 0.2e-3);
            Point kp23 = Point.ofCartesian(27.4792e-3, 0.2e-3);
            Point kp24 = Point.ofCartesian(25.8649e-3, 9.56e-3);

            Point kp31 = Point.ofCartesian(25.8649e-3, 9.56e-3);
            Point kp32 = Point.ofCartesian(27.4792e-3, 0.2e-3);
            Point kp33 = Point.ofCartesian(28.7192e-3, 0.2e-3);
            Point kp34 = Point.ofCartesian(27.1049e-3, 9.56e-3);

            Point kp41 = Point.ofCartesian(27.1049e-3, 9.56e-3);
            Point kp42 = Point.ofCartesian(28.7192e-3, 0.2e-3);
            Point kp43 = Point.ofCartesian(29.9592e-3, 0.2e-3);
            Point kp44 = Point.ofCartesian(28.3449e-3, 9.56e-3);

            Point kp51 = Point.ofCartesian(28.3449e-3, 9.56e-3);
            Point kp52 = Point.ofCartesian(29.9592e-3, 0.2e-3);
            Point kp53 = Point.ofCartesian(31.1992e-3, 0.2e-3);
            Point kp54 = Point.ofCartesian(29.5849e-3, 9.56e-3);

            Point kp61 = Point.ofCartesian(29.5849e-3, 9.56e-3);
            Point kp62 = Point.ofCartesian(31.1992e-3, 0.2e-3);
            Point kp63 = Point.ofCartesian(32.4392e-3, 0.2e-3);
            Point kp64 = Point.ofCartesian(30.8249e-3, 9.56e-3);

            Point kp71 = Point.ofCartesian(30.8249e-3, 9.56e-3);
            Point kp72 = Point.ofCartesian(32.4392e-3, 0.2e-3);
            Point kp73 = Point.ofCartesian(33.6792e-3, 0.2e-3);
            Point kp74 = Point.ofCartesian(32.0649e-3, 9.56e-3);

            Point kp81 = Point.ofCartesian(32.0649e-3, 9.56e-3);
            Point kp82 = Point.ofCartesian(33.6792e-3, 0.2e-3);
            Point kp83 = Point.ofCartesian(34.9192e-3, 0.2e-3);
            Point kp84 = Point.ofCartesian(33.3049e-3, 9.56e-3);

            Point kp91 = Point.ofCartesian(33.3049e-3, 9.56e-3);
            Point kp92 = Point.ofCartesian(34.9192e-3, 0.2e-3);
            Point kp93 = Point.ofCartesian(36.1592e-3, 0.2e-3);
            Point kp94 = Point.ofCartesian(34.5449e-3, 9.56e-3);

            Point kp101 = Point.ofCartesian(34.5449e-3, 9.56e-3);
            Point kp102 = Point.ofCartesian(36.1592e-3, 0.2e-3);
            Point kp103 = Point.ofCartesian(37.3992e-3, 0.2e-3);
            Point kp104 = Point.ofCartesian(35.7849e-3, 9.56e-3);

            Point kp111 = Point.ofCartesian(35.7849e-3, 9.56e-3);
            Point kp112 = Point.ofCartesian(37.3992e-3, 0.2e-3);
            Point kp113 = Point.ofCartesian(38.6392e-3, 0.2e-3);
            Point kp114 = Point.ofCartesian(37.0249e-3, 9.56e-3);

            Point kp121 = Point.ofCartesian(37.0249e-3, 9.56e-3);
            Point kp122 = Point.ofCartesian(38.6392e-3, 0.2e-3);
            Point kp123 = Point.ofCartesian(39.8792e-3, 0.2e-3);
            Point kp124 = Point.ofCartesian(38.2649e-3, 9.56e-3);

            Point kp131 = Point.ofCartesian(38.2649e-3, 9.56e-3);
            Point kp132 = Point.ofCartesian(39.8792e-3, 0.2e-3);
            Point kp133 = Point.ofCartesian(41.1192e-3, 0.2e-3);
            Point kp134 = Point.ofCartesian(39.5049e-3, 9.56e-3);

            Point kp141 = Point.ofCartesian(39.5049e-3, 9.56e-3);
            Point kp142 = Point.ofCartesian(41.1192e-3, 0.2e-3);
            Point kp143 = Point.ofCartesian(42.3592e-3, 0.2e-3);
            Point kp144 = Point.ofCartesian(40.7449e-3, 9.56e-3);

            Point kp151 = Point.ofCartesian(40.7449e-3, 9.56e-3);
            Point kp152 = Point.ofCartesian(42.3592e-3, 0.2e-3);
            Point kp153 = Point.ofCartesian(43.5992e-3, 0.2e-3);
            Point kp154 = Point.ofCartesian(41.9849e-3, 9.56e-3);

            Point kp161 = Point.ofCartesian(41.9849e-3, 9.56e-3);
            Point kp162 = Point.ofCartesian(43.5992e-3, 0.2e-3);
            Point kp163 = Point.ofCartesian(44.8392e-3, 0.2e-3);
            Point kp164 = Point.ofCartesian(43.2249e-3, 9.56e-3);

            Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, Point.ofCartesian(0e-3, 0));
            Line ln12 = Line.ofEndPoints(kp12, kp13);
            Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, Point.ofCartesian(1.24e-3, 0));
            Line ln14 = Line.ofEndPoints(kp11, kp14);

            Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, Point.ofCartesian(1.24e-3, 0));
            Line ln22 = Line.ofEndPoints(kp22, kp23);
            Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, Point.ofCartesian(2.48e-3, 0));
            Line ln24 = Line.ofEndPoints(kp21, kp24);

            Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, Point.ofCartesian(2.48e-3, 0));
            Line ln32 = Line.ofEndPoints(kp32, kp33);
            Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, Point.ofCartesian(3.72e-3, 0));
            Line ln34 = Line.ofEndPoints(kp31, kp34);

            Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, Point.ofCartesian(3.72e-3, 0));
            Line ln42 = Line.ofEndPoints(kp42, kp43);
            Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, Point.ofCartesian(4.96e-3, 0));
            Line ln44 = Line.ofEndPoints(kp41, kp44);

            Arc ln51 = Arc.ofEndPointsCenter(kp52, kp51, Point.ofCartesian(4.96e-3, 0));
            Line ln52 = Line.ofEndPoints(kp52, kp53);
            Arc ln53 = Arc.ofEndPointsCenter(kp53, kp54, Point.ofCartesian(6.2e-3, 0));
            Line ln54 = Line.ofEndPoints(kp51, kp54);

            Arc ln61 = Arc.ofEndPointsCenter(kp62, kp61, Point.ofCartesian(6.2e-3, 0));
            Line ln62 = Line.ofEndPoints(kp62, kp63);
            Arc ln63 = Arc.ofEndPointsCenter(kp63, kp64, Point.ofCartesian(7.44e-3, 0));
            Line ln64 = Line.ofEndPoints(kp61, kp64);

            Arc ln71 = Arc.ofEndPointsCenter(kp72, kp71, Point.ofCartesian(7.44e-3, 0));
            Line ln72 = Line.ofEndPoints(kp72, kp73);
            Arc ln73 = Arc.ofEndPointsCenter(kp73, kp74, Point.ofCartesian(8.68e-3, 0));
            Line ln74 = Line.ofEndPoints(kp71, kp74);

            Arc ln81 = Arc.ofEndPointsCenter(kp82, kp81, Point.ofCartesian(8.68e-3, 0));
            Line ln82 = Line.ofEndPoints(kp82, kp83);
            Arc ln83 = Arc.ofEndPointsCenter(kp83, kp84, Point.ofCartesian(9.92e-3, 0));
            Line ln84 = Line.ofEndPoints(kp81, kp84);

            Arc ln91 = Arc.ofEndPointsCenter(kp92, kp91, Point.ofCartesian(9.92e-3, 0));
            Line ln92 = Line.ofEndPoints(kp92, kp93);
            Arc ln93 = Arc.ofEndPointsCenter(kp93, kp94, Point.ofCartesian(11.16e-3, 0));
            Line ln94 = Line.ofEndPoints(kp91, kp94);

            Arc ln101 = Arc.ofEndPointsCenter(kp102, kp101, Point.ofCartesian(11.16e-3, 0));
            Line ln102 = Line.ofEndPoints(kp102, kp103);
            Arc ln103 = Arc.ofEndPointsCenter(kp103, kp104, Point.ofCartesian(12.4e-3, 0));
            Line ln104 = Line.ofEndPoints(kp101, kp104);

            Arc ln111 = Arc.ofEndPointsCenter(kp112, kp111, Point.ofCartesian(12.4e-3, 0));
            Line ln112 = Line.ofEndPoints(kp112, kp113);
            Arc ln113 = Arc.ofEndPointsCenter(kp113, kp114, Point.ofCartesian(13.64e-3, 0));
            Line ln114 = Line.ofEndPoints(kp111, kp114);

            Arc ln121 = Arc.ofEndPointsCenter(kp122, kp121, Point.ofCartesian(13.64e-3, 0));
            Line ln122 = Line.ofEndPoints(kp122, kp123);
            Arc ln123 = Arc.ofEndPointsCenter(kp123, kp124, Point.ofCartesian(14.88e-3, 0));
            Line ln124 = Line.ofEndPoints(kp121, kp124);

            Arc ln131 = Arc.ofEndPointsCenter(kp132, kp131, Point.ofCartesian(14.88e-3, 0));
            Line ln132 = Line.ofEndPoints(kp132, kp133);
            Arc ln133 = Arc.ofEndPointsCenter(kp133, kp134, Point.ofCartesian(16.12e-3, 0));
            Line ln134 = Line.ofEndPoints(kp131, kp134);

            Arc ln141 = Arc.ofEndPointsCenter(kp142, kp141, Point.ofCartesian(16.12e-3, 0));
            Line ln142 = Line.ofEndPoints(kp142, kp143);
            Arc ln143 = Arc.ofEndPointsCenter(kp143, kp144, Point.ofCartesian(17.36e-3, 0));
            Line ln144 = Line.ofEndPoints(kp141, kp144);

            Arc ln151 = Arc.ofEndPointsCenter(kp152, kp151, Point.ofCartesian(17.36e-3, 0));
            Line ln152 = Line.ofEndPoints(kp152, kp153);
            Arc ln153 = Arc.ofEndPointsCenter(kp153, kp154, Point.ofCartesian(18.6e-3, 0));
            Line ln154 = Line.ofEndPoints(kp151, kp154);

            Arc ln161 = Arc.ofEndPointsCenter(kp162, kp161, Point.ofCartesian(18.6e-3, 0));
            Line ln162 = Line.ofEndPoints(kp162, kp163);
            Arc ln163 = Arc.ofEndPointsCenter(kp163, kp164, Point.ofCartesian(19.84e-3, 0));
            Line ln164 = Line.ofEndPoints(kp161, kp164);

            Area ha11p1 = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
            Area ha11n1 = ha11p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha11p2 = ha11p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha11n2 = ha11n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha11p3 = ha11p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha11n3 = ha11n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha12p1 = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
            Area ha12n1 = ha12p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha12p2 = ha12p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha12n2 = ha12n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha12p3 = ha12p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha12n3 = ha12n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha13p1 = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
            Area ha13n1 = ha13p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha13p2 = ha13p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha13n2 = ha13n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha13p3 = ha13p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha13n3 = ha13n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha14p1 = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
            Area ha14n1 = ha14p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha14p2 = ha14p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha14n2 = ha14n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha14p3 = ha14p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha14n3 = ha14n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha15p1 = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
            Area ha15n1 = ha15p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha15p2 = ha15p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha15n2 = ha15n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha15p3 = ha15p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha15n3 = ha15n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha16p1 = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
            Area ha16n1 = ha16p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha16p2 = ha16p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha16n2 = ha16n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha16p3 = ha16p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha16n3 = ha16n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha17p1 = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
            Area ha17n1 = ha17p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha17p2 = ha17p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha17n2 = ha17n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha17p3 = ha17p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha17n3 = ha17n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha18p1 = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});
            Area ha18n1 = ha18p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha18p2 = ha18p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha18n2 = ha18n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha18p3 = ha18p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha18n3 = ha18n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha19p1 = Area.ofHyperLines(new HyperLine[]{ln91, ln92, ln93, ln94});
            Area ha19n1 = ha19p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha19p2 = ha19p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha19n2 = ha19n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha19p3 = ha19p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha19n3 = ha19n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha110p1 = Area.ofHyperLines(new HyperLine[]{ln101, ln102, ln103, ln104});
            Area ha110n1 = ha110p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha110p2 = ha110p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha110n2 = ha110n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha110p3 = ha110p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha110n3 = ha110n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha111p1 = Area.ofHyperLines(new HyperLine[]{ln111, ln112, ln113, ln114});
            Area ha111n1 = ha111p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha111p2 = ha111p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha111n2 = ha111n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha111p3 = ha111p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha111n3 = ha111n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha112p1 = Area.ofHyperLines(new HyperLine[]{ln121, ln122, ln123, ln124});
            Area ha112n1 = ha112p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha112p2 = ha112p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha112n2 = ha112n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha112p3 = ha112p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha112n3 = ha112n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha113p1 = Area.ofHyperLines(new HyperLine[]{ln131, ln132, ln133, ln134});
            Area ha113n1 = ha113p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha113p2 = ha113p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha113n2 = ha113n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha113p3 = ha113p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha113n3 = ha113n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha114p1 = Area.ofHyperLines(new HyperLine[]{ln141, ln142, ln143, ln144});
            Area ha114n1 = ha114p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha114p2 = ha114p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha114n2 = ha114n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha114p3 = ha114p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha114n3 = ha114n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha115p1 = Area.ofHyperLines(new HyperLine[]{ln151, ln152, ln153, ln154});
            Area ha115n1 = ha115p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha115p2 = ha115p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha115n2 = ha115n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha115p3 = ha115p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha115n3 = ha115n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Area ha116p1 = Area.ofHyperLines(new HyperLine[]{ln161, ln162, ln163, ln164});
            Area ha116n1 = ha116p1.mirrorX().rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha116p2 = ha116p1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha116n2 = ha116n1.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha116p3 = ha116p2.rotate(Math.PI/3, Point.ofCartesian(0, 0));
            Area ha116n3 = ha116n2.rotate(Math.PI/3, Point.ofCartesian(0, 0));

            Winding w1_1 = Winding.ofAreas(new Area[]{ha11p1, ha11n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w1_2 = Winding.ofAreas(new Area[]{ha11p2, ha11n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w1_3 = Winding.ofAreas(new Area[]{ha11p3, ha11n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w2_1 = Winding.ofAreas(new Area[]{ha12p1, ha12n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w2_2 = Winding.ofAreas(new Area[]{ha12p2, ha12n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w2_3 = Winding.ofAreas(new Area[]{ha12p3, ha12n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w3_1 = Winding.ofAreas(new Area[]{ha13p1, ha13n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w3_2 = Winding.ofAreas(new Area[]{ha13p2, ha13n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w3_3 = Winding.ofAreas(new Area[]{ha13p3, ha13n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w4_1 = Winding.ofAreas(new Area[]{ha14p1, ha14n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w4_2 = Winding.ofAreas(new Area[]{ha14p2, ha14n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w4_3 = Winding.ofAreas(new Area[]{ha14p3, ha14n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w5_1 = Winding.ofAreas(new Area[]{ha15p1, ha15n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w5_2 = Winding.ofAreas(new Area[]{ha15p2, ha15n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w5_3 = Winding.ofAreas(new Area[]{ha15p3, ha15n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w6_1 = Winding.ofAreas(new Area[]{ha16p1, ha16n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w6_2 = Winding.ofAreas(new Area[]{ha16p2, ha16n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w6_3 = Winding.ofAreas(new Area[]{ha16p3, ha16n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w7_1 = Winding.ofAreas(new Area[]{ha17p1, ha17n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w7_2 = Winding.ofAreas(new Area[]{ha17p2, ha17n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w7_3 = Winding.ofAreas(new Area[]{ha17p3, ha17n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w8_1 = Winding.ofAreas(new Area[]{ha18p1, ha18n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w8_2 = Winding.ofAreas(new Area[]{ha18p2, ha18n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w8_3 = Winding.ofAreas(new Area[]{ha18p3, ha18n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w9_1 = Winding.ofAreas(new Area[]{ha19p1, ha19n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w9_2 = Winding.ofAreas(new Area[]{ha19p2, ha19n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w9_3 = Winding.ofAreas(new Area[]{ha19p3, ha19n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w10_1 = Winding.ofAreas(new Area[]{ha110p1, ha110n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w10_2 = Winding.ofAreas(new Area[]{ha110p2, ha110n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w10_3 = Winding.ofAreas(new Area[]{ha110p3, ha110n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w11_1 = Winding.ofAreas(new Area[]{ha111p1, ha111n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w11_2 = Winding.ofAreas(new Area[]{ha111p2, ha111n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w11_3 = Winding.ofAreas(new Area[]{ha111p3, ha111n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w12_1 = Winding.ofAreas(new Area[]{ha112p1, ha112n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w12_2 = Winding.ofAreas(new Area[]{ha112p2, ha112n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w12_3 = Winding.ofAreas(new Area[]{ha112p3, ha112n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w13_1 = Winding.ofAreas(new Area[]{ha113p1, ha113n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w13_2 = Winding.ofAreas(new Area[]{ha113p2, ha113n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w13_3 = Winding.ofAreas(new Area[]{ha113p3, ha113n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w14_1 = Winding.ofAreas(new Area[]{ha114p1, ha114n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w14_2 = Winding.ofAreas(new Area[]{ha114p2, ha114n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w14_3 = Winding.ofAreas(new Area[]{ha114p3, ha114n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w15_1 = Winding.ofAreas(new Area[]{ha115p1, ha115n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w15_2 = Winding.ofAreas(new Area[]{ha115p2, ha115n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w15_3 = Winding.ofAreas(new Area[]{ha115p3, ha115n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Winding w16_1 = Winding.ofAreas(new Area[]{ha116p1, ha116n1}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w16_2 = Winding.ofAreas(new Area[]{ha116p2, ha116n2}, new int[]{-1,+1},13, 13, new input.FCC.MS.Cable_FCC_MS());
//            Winding w16_3 = Winding.ofAreas(new Area[]{ha116p3, ha116n3}, new int[]{+1,-1},13, 13, new input.FCC.MS.Cable_FCC_MS());

            Pole p1 = Pole.ofWindings(new Winding[]{w1_1, w2_1, w3_1, w4_1, w5_1, w6_1, w7_1, w8_1, w9_1, w10_1, w11_1, w12_1, w13_1, w14_1, w15_1, w16_1});

//            Pole p2 = Pole.ofWindings(new Winding[]{w1_2, w2_2, w3_2, w4_2, w5_2, w6_2, w7_2, w8_2, w9_2, w10_2, w11_2, w12_2, w13_2, w14_2, w15_2, w16_2});

//            Pole p3 = Pole.ofWindings(new Winding[]{w1_3, w2_3, w3_3, w4_3, w5_3, w6_3, w7_3, w8_3, w9_3, w10_3, w11_3, w12_3, w13_3, w14_3, w15_3, w16_3});

            // Coil:
            Coil c1 = Coil.ofPoles(new Pole[]{p1});
            return c1;



        }


        //---------------------------added by D. Pracht. 04.10.2018-----------------------------


        public Element[] iron_yoke() {
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //  -- BEAMTUNNEL - FCC

            double mm = 0.001;
            double Pi = 3.14159265;
            double deg = Math.PI / 180;
//-- Design variables added by Per HAGEN


            //-- shrinking factor for supports/yokes
            double DCONT = 0.0;


            double shrink = 1.0 * (1 - DCONT);


//
// -- shrinking factor for collars
            double DCCONT = 0.0;

            double cshrink = 1.0 * (1 - DCCONT);


            double DMCONT = 0.0;

            double mshrink = 1.0 * (1 - DMCONT);
            //-- shrinking factor for modules

            double LHCBEAMD = 97.00;
            //-- half of the beam distance in LHC

            double BEAMD = 102.00;

            double beamd = BEAMD * mm;    //
            // -- half of the beam distance in FCC


            double LHCRBT = 94.80;
            double RBT = BEAMD - 2 * (LHCBEAMD - LHCRBT);
            double rbt = RBT * mm * shrink;


            //-- thickness of iron in between coils is chosen twice larger in FCC than in LHC


            double PHIBT = 45.00;
            double phibt = PHIBT * deg;
            double HBARBT = 7.00;
            double hbarbt = HBARBT * mm * shrink;
            double DBARBT = 4.00;
            double dbarbt = DBARBT * mm * shrink;

//-- OUTER FORM VARIABLES

            double RSUPP = 226.00;
            double rsupp = RSUPP * mm * shrink;

            double PHIBAR = 45.00;
            double phibar = PHIBAR * deg;

            double DBAR = 16.00;
            double dbar = DBAR * mm * shrink;
            double HBAR = 17.00;
            double hbar = HBAR * mm * shrink;

            double psibar = Math.asin(dbar / (2 * rsupp));

            double YEDGE = 173.00;
            double yedge = YEDGE * mm * shrink;
            double xedge = Math.sqrt(rsupp * rsupp - yedge * yedge);

            double GCORNX = 125.00;
            double gcornx = GCORNX * mm * shrink;
            double GCORNY = 125.00;
            double gcorny = GCORNY * mm * shrink;
            double delt = xedge - gcornx;
            double GCORNR = 10.00;
            double gcornr = GCORNR * mm * shrink;

            double XH1 = 115.00;
            double xh1 = XH1 * mm * shrink;
            double YH1 = 110.00;
            double yh1 = YH1 * mm * shrink;
            double RH1 = 6.1;
            double rh1 = RH1 * mm * shrink;
            double XH2 = 187.00;
            double xh2 = XH2 * mm * shrink;
            double YH2 = 80.00;
            double yh2 = YH2 * mm * shrink;
            double RH2 = 10.5;
            double rh2 = RH2 * mm * shrink;


            /////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////

            // KEYPOINTS

            Point kp0_0 = Point.ofCartesian(0, 0);
            Point kp0 = Point.ofCartesian(beamd, 0);
            Point kp0_1 = Point.ofCartesian(rsupp, 0);


//        kpbt1 = [beamd+rbt,0];
            Point kpbt1r = Point.ofCartesian(beamd + rbt, 0); //199,6 mm


            // *right*
            //kpbt1r = [beamd+rbt,0];
            //   kpbt2r = [beamd+(rbt+dbarbt)*Cos(phibt),(rbt-dbarbt)*Sin(phibt)];
            //  kpbt3r = [beamd+(rbt-dbarbt)*Cos(phibt),(rbt+dbarbt)*Sin(phibt)];
            //kpbt4r = [beamd+(rbt-dbarbt)*Cos(phibt+Pi/2),(rbt+dbarbt)*Sin(phibt+Pi/2)];
            // kpbt5r = [beamd+(rbt+dbarbt)*Cos(phibt+Pi/2),(rbt-dbarbt)*Sin(phibt+Pi/2)];
            //  kpbt6r = [beamd-rbt,0];

            Point kpbt2r = Point.ofCartesian(beamd + (rbt + dbarbt) * Math.cos(phibt), (rbt - dbarbt) * Math.sin(phibt));
            Point kpbt3r = Point.ofCartesian(beamd + (rbt - dbarbt) * Math.cos(phibt), (rbt + dbarbt) * Math.sin(phibt));
            Point kpbt4r = Point.ofCartesian(beamd + (rbt - dbarbt) * Math.cos(phibt + Math.PI / 2), (rbt + dbarbt) * Math.sin(phibt + Math.PI / 2));
            Point kpbt5r = Point.ofCartesian(beamd + (rbt + dbarbt) * Math.cos(phibt + Math.PI / 2), (rbt - dbarbt) * Math.sin(phibt + Math.PI / 2));
            Point kpbt6r = Point.ofCartesian(beamd - rbt, 0);


            // kpsupp_1r = [rsupp,0];
            //   kpsupp_2r = [rsupp @ Pi/2-phibar-psibar];
            //kpsupp_3r = [rsupp @ Pi/2-phibar+psibar];
            //    kpsupp_4r = [xedge,yedge];
            //kpsupp_5r = [gcornx,yedge-delt];
            //kpsupp_6r = [gcornx,gcorny+gcornr];
            //kpsupp_7r = [gcornx-gcornr,gcorny];

            Point kpsupp_1r = Point.ofCartesian(rsupp, 0);
            Point kpsupp_2r = Point.ofPolar(rsupp, (Math.PI / 2) - phibar - psibar);
            Point kpsupp_3r = Point.ofPolar(rsupp, Math.PI / 2 - phibar + psibar);
            Point kpsupp_4r = Point.ofCartesian(xedge, yedge);
            Point kpsupp_5r = Point.ofCartesian(gcornx, yedge - delt);
            Point kpsupp_6r = Point.ofCartesian(gcornx, gcorny + gcornr);
            Point kpsupp_7r = Point.ofCartesian(gcornx - gcornr, gcorny);


//       Point kpyoke_6 = [rcorn @ Pi/2-phibar+psicorn_2];
            //      Point kpyoke_6 = Point.ofPolar(rcorn, Math.PI/2-phibar+psicorn_2);


//       Point kpyoke_7 = [ryok @ Pi/2-phibar+psicorn_2];
            //Point kpyoke_7 = Point.ofPolar(ryok, Math.PI/2-phibar+psicorn_2);


            // kph1_1r = [xh1,yh1+rh1];
            Point kph1_1r = Point.ofCartesian(xh1, yh1 + rh1);

            // kph1_2r = [xh1,yh1-rh1];
            Point kph1_2r = Point.ofCartesian(xh1, yh1 - rh1);

            // kph2_1r = [xh2,yh2+rh2];
            Point kph2_1r = Point.ofCartesian(xh2, yh2 + rh2);
            // kph2_2r = [xh2,yh2-rh2];
            Point kph2_2r = Point.ofCartesian(xh2, yh2 - rh2);


            //-- *middle*
            //          kpsupp_8 = [0,gcorny];
            Point kpsupp_8 = Point.ofCartesian(0, gcorny);


//        kpsupp_9 = [0,0];
            Point kpsupp_9 = Point.ofCartesian(0, 0);

            //-- LINES

            //      -- *right*
            Point kptest_0 = Point.ofCartesian(rsupp, 0);

            //   lnsupp_1r = HyperLine(kpsupp_2r,kpsupp_1r,"Arc",rsupp);
            Arc lnsupp_1r = Arc.ofEndPointsCenter(kpsupp_2r, kpsupp_1r, kp0_0);

            //Arc lnsupp_1r = Arc.ofPointCenterAngle(kpsupp_1r, kpsupp_2r, (Math.PI/2)-phibar-psibar);


            /////////////////////nochmal schauen////////////////////
            //   lnsupp_2r = HyperLine(kpsupp_2r,kpsupp_3r,"Bar",hbar,0.5);
            Line lnsupp_2r = Line.ofEndPoints(kpsupp_2r, kpsupp_3r);
            //Arc lnsupp_2r = Arc.ofEndPointsCenter(kpsupp_2r, kpsupp_1r, kpsupp_1r);
            ////////////////////////////////////////


            //lnsupp_3r = HyperLine(kpsupp_4r,kpsupp_3r,"Arc",rsupp);
            Arc lnsupp_3r = Arc.ofEndPointsCenter(kpsupp_4r, kpsupp_3r, kp0_0);


            // lnsupp_4r = Line(kpsupp_4r,kpsupp_5r);
            Line lnsupp_4r = Line.ofEndPoints(kpsupp_4r, kpsupp_5r);
            // lnsupp_5r = Line(kpsupp_5r,kpsupp_6r);


            Line lnsupp_5r = Line.ofEndPoints(kpsupp_5r, kpsupp_6r);


            Point kptest_1 = Point.ofCartesian(gcornr, 0);

            //lnsupp_6r = HyperLine(kpsupp_6r,kpsupp_7r,"Arc",kptest_1);
            // Arc lnsupp_6r = Arc.ofEndPointsCenter(kpsupp_6r, kpsupp_7r, kptest_1);

            Arc lnsupp_6r = Arc.ofEndPointsRadius(kpsupp_6r, kpsupp_7r, gcornr);
            //lnsupp_7r = Line(kpsupp_7r,kpsupp_8);
            Line lnsupp_7r = Line.ofEndPoints(kpsupp_7r, kpsupp_8);


            //lnsupp_9r = Line(kpsupp_9,kpbt6r);
            Line lnsupp_9r = Line.ofEndPoints(kpsupp_9, kpbt6r);


            Point kptest_4 = Point.ofCartesian(hbarbt, 0);
            // lnbt2r = HyperLine(kpbt3r,kpbt2r,"Bar",hbarbt);
            Arc lnbt2r = Arc.ofEndPointsCenter(kpbt3r, kpbt2r, kptest_4);


            //////////////////////nochmal anschauen//////////////////
            //lnbt3r = HyperLine(kpbt4r,kpbt3r,"Arc",rbt,0.5);
            Arc lnbt3r = Arc.ofEndPointsCenter(kpbt4r, kpbt3r, kp0);
            //Line  lnbt3r = Line.ofEndPoints(kpbt4r,kpbt3r);
            /////////////////////////////////////////////////////////


            //lnbt4r = HyperLine(kpbt5r,kpbt4r,"Bar",hbarbt);
            // Arc lnbt4r = Arc.ofEndPointsCenter(kpbt5r, kpbt4r, kp0);
            //Line  lnbt4r = Line.ofEndPoints(kpbt5r,kpbt4r);


            ///////////////////////1.1////////////////////
            //lnbt5r = HyperLine(kpbt6r,kpbt5r,"Arc",rbt);
            Arc lnbt5r = Arc.ofEndPointsCenter(kpbt6r, kpbt1r, kp0);
            //Point kptest_3 = Point.ofCartesian(rbt,0);
            // lnbt1r = HyperLine(kpbt2r,kpbt1r,"Arc",rbt);
            // Arc lnbt1r = Arc.ofEndPointsCenter(kpbt2r, kpbt1r, kp0);


            //  lnsupp_10r = Line(kpbt1r,kpsupp_1r);
            Line lnsupp_10r = Line.ofEndPoints(kpbt1r, kpsupp_1r);

            ///////////////////////nochmal anschauen///////////////////////////////////
            //lnh1r = HyperLine(kph1_1r,kph1_2r,"Circle");
            //Arc lnh1r = Arc.ofEndPointsCenter(kph1_1r, kph1_2r, rbt);
            Line lnh1r = Line.ofEndPoints(kph1_1r, kph1_2r);
            //////////////////////////////////////////////////////////


            ///////////////////////nochmal anschauen///////////////////////////////////
            //lnh2r = HyperLine(kph2_1r,kph2_2r,"Circle");
            // Arc lnh1r = Arc.ofEndPointsCenter(kph1_1r, kph1_2r, rbt);
            Line lnh2r = Line.ofEndPoints(kph2_1r, kph2_2r);
            //////////////////////////////////////////////////////////


            //-- *middle

            //Point kptest_2 = Point.ofCartesian(650e-3,0);
            // lnsupp_8 = HyperLine(kpsupp_8,kpsupp_9,"Line",0.65);
            //Arc lnsupp_8 = Arc.ofEndPointsCenter(kpsupp_8, kpsupp_9, kptest_2);
            Line lnsupp_8 = Line.ofEndPoints(kpsupp_8, kpsupp_9);


//        Arc lnyoke_1 = Arc.ofEndPointsCenter(kpbar_1, kpyoke_1, kp0);

            //      Line  lnyoke_4 = Line.ofEndPoints(kpyoke_3,kpyoke_2);

            //-- AREAS

            //      -- *right*

            Area arsuppr = Area.ofHyperLines(new HyperLine[]{lnsupp_1r, lnsupp_2r, lnsupp_3r, lnsupp_4r, lnsupp_5r, lnsupp_6r, lnsupp_7r, lnsupp_8, lnsupp_9r, lnbt5r, lnsupp_10r});


            ////////////////////////////////////2nd part///////////////////////////////////////////
        /*

        -- * * * * * * * * * * * * * * *
        -- * * * V A R I A B L E S * * *
        -- * * * * * * * * * * * * * * *
*/

            double LHCra = 28.2; //-- aperture radius of LHC
            double FCCra = 25; //-- aperture radius of FCC
            double LHCw = 9.79; //-- coil width of LHC MS
            double FCCw = 19.88; //-- coil width of FCC MS
            double LHCMSCB_ROUT = 79.9;
            double MSCB_ROUT = LHCMSCB_ROUT + RBT - LHCRBT; //
            double mscb_rout = MSCB_ROUT * mm * mshrink;        //-- to conserve thickness of aluminium
            double LHCMSCB_RIN = 41.75;
            //-- MSCB_RIN = LHCMSCB_RIN/LHCMSCB_ROUT*MSCB_ROUT;
            // double mscb_rin = MSCB_RIN*mm*mshrink;		//-- homothetic enlargement of iron shield around coil
            double MSCB_RIN = LHCMSCB_RIN + FCCra - LHCra + FCCw - LHCw;
            double mscb_rin = MSCB_RIN * mm * mshrink;        //-- to conserve LHC distance between coil and iron shield around coil

/*

        -- * * * * * * * * * * * * * * *
        -- * * * S T R U C T U R E * * *
        -- * * * * * * * * * * * * * * *
*/

/*

        -- Keypoints
*/

            //kpmscbr1 = [2*beamd-(beamd-mscb_rout),0];
            Point kpmscbr1 = Point.ofCartesian(2 * beamd - (beamd - mscb_rout), 0);

            //kpmscbr2 = [2*beamd-(beamd+mscb_rout),0];
            Point kpmscbr2 = Point.ofCartesian(2 * beamd - (beamd + mscb_rout), 0);

            // kpmscbr3 = [2*beamd-(beamd-mscb_rin),0];
            Point kpmscbr3 = Point.ofCartesian(2 * beamd - (beamd - mscb_rin), 0);

            //kpmscbr4 = [2*beamd-(beamd+mscb_rin),0];
            Point kpmscbr4 = Point.ofCartesian(2 * beamd - (beamd + mscb_rin), 0);

            //kpmscbr5 = [2*beamd-beamd,mscb_rout];
            Point kpmscbr5 = Point.ofCartesian(2 * beamd - beamd, mscb_rout);

            //kpmscbr6 = [2*beamd-beamd,mscb_rin];
            Point kpmscbr6 = Point.ofCartesian(2 * beamd - beamd, mscb_rin);

            // -- LINES


            Point kptest_5 = Point.ofCartesian(mscb_rout, 0);
            //lnmscbr1 = HyperLine(kpmscbr2,kpmscbr5,"Arc",mscb_rout);
            Arc lnmscbr1 = Arc.ofEndPointsCenter(kpmscbr2, kpmscbr5, kp0);

            //lnmscbr1b = HyperLine(kpmscbr5,kpmscbr1,"Arc",mscb_rout);
            Arc lnmscbr1b = Arc.ofEndPointsCenter(kpmscbr5, kpmscbr1, kp0);

            Point kptest_6 = Point.ofCartesian(mscb_rin, 0);
            //lnmscbr2 = HyperLine(kpmscbr4,kpmscbr6,"Arc",mscb_rin);
            Arc lnmscbr2 = Arc.ofEndPointsCenter(kpmscbr4, kpmscbr6, kp0);

            //lnmscbr2b = HyperLine(kpmscbr6,kpmscbr3,"Arc",mscb_rin);
            Arc lnmscbr2b = Arc.ofEndPointsCenter(kpmscbr6, kpmscbr3, kp0);

            //lnmscbr3 = Line(kpmscbr1,kpmscbr3,0.54);
            Line lnmscbr3 = Line.ofEndPoints(kpmscbr1, kpmscbr3);

            //lnmscbr4 = Line(kpmscbr2,kpmscbr4,0.54);
            Line lnmscbr4 = Line.ofEndPoints(kpmscbr2, kpmscbr4);

            //lnmscbr5 = Line(kpmscbr5,kpmscbr6,0.54);
            Line lnmscbr5 = Line.ofEndPoints(kpmscbr5, kpmscbr6);


            //    -- AREAS

            Area armscbr1 = Area.ofHyperLines(new HyperLine[]{lnmscbr4, lnmscbr2, lnmscbr5, lnmscbr1});
            Area armscbr2 = Area.ofHyperLines(new HyperLine[]{lnmscbr3, lnmscbr1b, lnmscbr5, lnmscbr2b});


            Element el1_1 = new Element("IY_El1", arsuppr);
            Element el1_2 = new Element("IY_El2", armscbr1);
            Element el1_3 = new Element("IY_El3", armscbr2);
//        Element el1_4 = new Element("IY_El4", aryoke_mid_l);

            Element[] quad1 = {el1_1};
            Element[] quad2 = {el1_2};
            Element[] quad3 = {el1_3};

            return new Element[]{el1_1, el1_2, el1_3};


//////////////////////////////////////////////////////////////////////////////////////////////


        }


        public Element[] BoundaryConditions() {
            double length = 10;
            double eps = 1e-4;

            //POINTS
            Point kpx1 = Point.ofCartesian(-length, 0 - eps);
            Point kpx2 = Point.ofCartesian(length, 0 - eps);
            Point kpx3 = Point.ofCartesian(length, 0 + eps);
            Point kpx4 = Point.ofCartesian(-length, 0 + eps);

            Point kpy1 = Point.ofCartesian(0 - eps, -length);
            Point kpy2 = Point.ofCartesian(0 + eps, -length);
            Point kpy3 = Point.ofCartesian(0 + eps, length);
            Point kpy4 = Point.ofCartesian(0 - eps, length);

            // LINES
            Line lnx1 = Line.ofEndPoints(kpx2, kpx1);
            Line lnx2 = Line.ofEndPoints(kpx2, kpx3);
            Line lnx3 = Line.ofEndPoints(kpx3, kpx4);
            Line lnx4 = Line.ofEndPoints(kpx1, kpx4);

            Line lny1 = Line.ofEndPoints(kpy2, kpy1);
            Line lny2 = Line.ofEndPoints(kpy2, kpy3);
            Line lny3 = Line.ofEndPoints(kpy3, kpy4);
            Line lny4 = Line.ofEndPoints(kpy1, kpy4);

            // AREAS
            Area xBC_Sel = Area.ofHyperLines(new HyperLine[]{lnx1, lnx2, lnx3, lnx4});
            Area yBC_Sel = Area.ofHyperLines(new HyperLine[]{lny1, lny2, lny3, lny4});

            // ELEMENTS
            Element xBC = new Element("xBC", xBC_Sel);
            Element yBC = new Element("yBC", yBC_Sel);

            return new Element[]{xBC, yBC};
        }
    }



