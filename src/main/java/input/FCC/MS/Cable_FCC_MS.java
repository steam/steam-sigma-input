package input.FCC.MS;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class Cable_FCC_MS extends Cable {

    public Cable_FCC_MS() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_FCC_MS";

        //Insulation
        this.wInsulNarrow = 30e-6; // [m];
        this.wInsulWide = 30e-6; // [m];
        //Filament
        this.dFilament = 7.0e-6; // [m]
        //Strand
        this.dstrand = 0.996e-3; // [m];
        this.fracCu = 2.70 /(1+2.70);
        this.fracSc = 1/(1+2.70);
        this.RRR = 150;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 18e-3; // [m];
        //Cable
        this.wBare = 1.18e-3; // [m];
        this.hInBare = 0.66e-3; // [m];
        this.hOutBare = 0.66e-3; // [m];
        this.noOfStrands = 1;
        this.noOfStrandsPerLayer = 1;
        this.noOfLayers = 1;
        this.lTpStrand = 1e6; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 92073.5; // [A];
        this.C2 = -6869.4; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_NbTi_GSI;
        this.insulationMaterial = MatDatabase.MAT_KAPTON;
        this.materialInnerVoids = MatDatabase.MAT_VOID;
        this.materialOuterVoids = MatDatabase.MAT_VOID;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_CUDI;
    }


}

