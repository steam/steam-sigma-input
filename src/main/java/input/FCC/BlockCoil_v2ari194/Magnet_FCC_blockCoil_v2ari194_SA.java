package input.FCC.BlockCoil_v2ari194;

import input.FCC.BlockCoil_v2ari194.CableHF_FCC_block_v2ari194;
import input.FCC.BlockCoil_v2ari194.CableLF_FCC_block_v2ari194;
import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

/**
 * Created by mprioli on 14/12/2017.
 */
public class Magnet_FCC_blockCoil_v2ari194_SA {

    private final Domain[] domains;

    public Magnet_FCC_blockCoil_v2ari194_SA() {

        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("coil", MatDatabase.MAT_COIL, coil()),
                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, iron_yoke()),
                new HoleDomain("holesYoke", MatDatabase.MAT_AIR, holes_yoke())
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);

        return new Element[]{el1};
    }

    public Element[] airFarField() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        // AREAS
        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});

        // ELEMENTS
        Element el1_far = new Element("FAR_El1", ar1_far);

        return new Element[]{el1_far};
    }

    public Coil coil() {

        double dy = 0.5e-3; // No gaps among blocks for the COMSOL - ANSYS coupling
//        double dy = 0;    // Original ROXIE model with gaps among the blocks

        Point kp11 = Point.ofCartesian(38.40e-3, 15.25e-3);
        Point kp12 = Point.ofCartesian(26.90e-3, 15.25e-3);
        Point kp13 = Point.ofCartesian(26.90e-3, 02.35e-3 - dy);
        Point kp14 = Point.ofCartesian(38.40e-3, 02.35e-3 - dy);

        Point kp21 = Point.ofCartesian(38.40e-3, 28.65e-3);
        Point kp22 = Point.ofCartesian(26.90e-3, 28.65e-3);
        Point kp23 = Point.ofCartesian(26.90e-3, 15.75e-3 - dy);
        Point kp24 = Point.ofCartesian(38.40e-3, 15.75e-3 - dy);

        Point kp31 = Point.ofCartesian(36.83e-3, 42.05e-3);
        Point kp32 = Point.ofCartesian(13.83e-3, 42.05e-3);
        Point kp33 = Point.ofCartesian(13.83e-3, 29.15e-3 - dy);
        Point kp34 = Point.ofCartesian(36.83e-3, 29.15e-3 - dy);

        Point kp41 = Point.ofCartesian(36.83e-3, 55.45e-3);
        Point kp42 = Point.ofCartesian(13.83e-3, 55.45e-3);
        Point kp43 = Point.ofCartesian(13.83e-3, 42.55e-3 - dy);
        Point kp44 = Point.ofCartesian(36.83e-3, 42.55e-3 - dy);

        Point kp51 = Point.ofCartesian(71.37e-3, 15.25e-3);
        Point kp52 = Point.ofCartesian(38.40e-3, 15.25e-3);
        Point kp53 = Point.ofCartesian(38.40e-3, 02.35e-3 - dy);
        Point kp54 = Point.ofCartesian(71.37e-3, 02.35e-3 - dy);

        Point kp61 = Point.ofCartesian(71.37e-3, 28.65e-3);
        Point kp62 = Point.ofCartesian(38.40e-3, 28.65e-3);
        Point kp63 = Point.ofCartesian(38.40e-3, 15.75e-3 - dy);
        Point kp64 = Point.ofCartesian(71.37e-3, 15.75e-3 - dy);

        Point kp71 = Point.ofCartesian(71.37e-3, 42.05e-3);
        Point kp72 = Point.ofCartesian(36.83e-3, 42.05e-3);
        Point kp73 = Point.ofCartesian(36.83e-3, 29.15e-3 - dy);
        Point kp74 = Point.ofCartesian(71.37e-3, 29.15e-3 - dy);

        Point kp81 = Point.ofCartesian(71.37e-3, 55.45e-3);
        Point kp82 = Point.ofCartesian(36.83e-3, 55.45e-3);
        Point kp83 = Point.ofCartesian(36.83e-3, 42.55e-3 - dy);
        Point kp84 = Point.ofCartesian(71.37e-3, 42.55e-3 - dy);


        Line ln11 = Line.ofEndPoints(kp12, kp11);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Line ln13 = Line.ofEndPoints(kp13, kp14);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Line ln21 = Line.ofEndPoints(kp22, kp21);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Line ln23 = Line.ofEndPoints(kp23, kp24);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Line ln31 = Line.ofEndPoints(kp32, kp31);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Line ln33 = Line.ofEndPoints(kp33, kp34);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Line ln41 = Line.ofEndPoints(kp42, kp41);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Line ln43 = Line.ofEndPoints(kp43, kp44);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Line ln51 = Line.ofEndPoints(kp52, kp51);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Line ln53 = Line.ofEndPoints(kp53, kp54);
        Line ln54 = Line.ofEndPoints(kp51, kp54);

        Line ln61 = Line.ofEndPoints(kp62, kp61);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Line ln63 = Line.ofEndPoints(kp63, kp64);
        Line ln64 = Line.ofEndPoints(kp61, kp64);

        Line ln71 = Line.ofEndPoints(kp72, kp71);
        Line ln72 = Line.ofEndPoints(kp72, kp73);
        Line ln73 = Line.ofEndPoints(kp73, kp74);
        Line ln74 = Line.ofEndPoints(kp71, kp74);

        Line ln81 = Line.ofEndPoints(kp82, kp81);
        Line ln82 = Line.ofEndPoints(kp82, kp83);
        Line ln83 = Line.ofEndPoints(kp83, kp84);
        Line ln84 = Line.ofEndPoints(kp81, kp84);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
        Area ha17p = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
        Area ha18p = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});

        // negative winding cross section, pole 1:

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();
        Area ha15n = ha15p.mirrorY();
        Area ha16n = ha16p.mirrorY();
        Area ha17n = ha17p.mirrorY();
        Area ha18n = ha18p.mirrorY();

        Cable cableHF = new CableHF_FCC_block_v2ari194();
        Cable cableLF = new CableLF_FCC_block_v2ari194();

//        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 5, 5, cableHF);
//        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n}, new int[]{+1, -1}, 5, 5, cableHF);
//        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n}, new int[]{+1, -1}, 10, 10, cableHF);
//        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n}, new int[]{+1, -1}, 10, 10, cableHF);
//        Winding w15 = Winding.ofAreas(new Area[]{ha15p, ha15n}, new int[]{+1, -1}, 21, 21, cableLF);
//        Winding w16 = Winding.ofAreas(new Area[]{ha16p, ha16n}, new int[]{+1, -1}, 21, 21, cableLF);
//        Winding w17 = Winding.ofAreas(new Area[]{ha17p, ha17n}, new int[]{+1, -1}, 22, 22, cableLF);
//        Winding w18 = Winding.ofAreas(new Area[]{ha18p, ha18n}, new int[]{+1, -1}, 22, 22, cableLF);

        Winding w11 = Winding.ofAreas(new Area[]{ha11p}, new int[]{+1}, 5, 5, cableHF);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p}, new int[]{+1}, 5, 5, cableHF);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p}, new int[]{+1}, 10, 10, cableHF);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p}, new int[]{+1}, 10, 10, cableHF);
        Winding w15 = Winding.ofAreas(new Area[]{ha15p}, new int[]{+1}, 21, 21, cableLF);
        Winding w16 = Winding.ofAreas(new Area[]{ha16p}, new int[]{+1}, 21, 21, cableLF);
        Winding w17 = Winding.ofAreas(new Area[]{ha17p}, new int[]{+1}, 22, 22, cableLF);
        Winding w18 = Winding.ofAreas(new Area[]{ha18p}, new int[]{+1}, 22, 22, cableLF);

        w11 = w11.reverseWindingDirection();
        w14 = w14.reverseWindingDirection();
        w15 = w15.reverseWindingDirection();
        w18 = w18.reverseWindingDirection();

        Winding[] windings_p1 = {w15, w11, w12, w16, w18, w14, w13, w17};

        Pole p1 = Pole.ofWindings(windings_p1);
        Pole[] poles = {p1};

        Coil coil = Coil.ofPoles(poles);
//        coil = coil.translate(97e-3, 0);

        return coil;
    }

    public Element[] iron_yoke() {


        //VARIABLES
        double XI = 192e-3;
        double YI = 57e-3;
        double RO = 570e-3 / 2;
        double LO = 90e-3;
        double AL = 45;
        double BEAMO = 97e-3;
        double HO = 25e-3;
//        double CO       = 0.5;
        double YOKEX = 3.0e-3;
        double PV1T = 5e-3;
        double PV2T = 40e-3;
        double PH1T = 25e-3;
        double KT = 5e-3;
        double PHSW = 10e-3;
//        double KHXP = 40;
        double CPYP = YI;
        double CTRPAD = 0;
        double ANGY = 0.3;
        double centreHe = 225e-3;
        double rayonHe = 53e-3;

        double degr = Math.PI / 180;
        double dist = RO * Math.sin(ANGY * degr);
        double beta = Math.atan((YI + HO + PH1T + KT) / (XI + PV2T + KT));

        //-------------------------------------------NODES-------------------------------------
        //Upper yoke
        Point kp1 = Point.ofCartesian(YOKEX, Math.sqrt(RO * RO - YOKEX * YOKEX));
        Point kp2 = Point.ofCartesian(YOKEX, centreHe + rayonHe);
        Point kp3 = Point.ofCartesian(YOKEX + rayonHe, centreHe);
        Point kp4 = Point.ofCartesian(YOKEX, centreHe - rayonHe);
        Point kp5 = Point.ofCartesian(YOKEX, YI + HO + PH1T + KT + dist * Math.cos(beta));
        Point kp6 = Point.ofCartesian(XI + PV2T + KT, YI + HO + PH1T + KT);
        Point kp7 = Point.ofPolar(RO * Math.cos(ANGY * degr), beta);
        Point kp8 = Point.ofCartesian(kp6.getX() - dist * Math.sin(beta), kp6.getY() + dist * Math.cos(beta));
        Point kp9 = Point.ofCartesian(kp7.getX() - dist * Math.sin(beta), kp7.getY() + dist * Math.cos(beta));
        Point kp10 = Point.ofPolar(RO, 80 * degr);

        //Lateral yoke
        Point kp11 = Point.ofCartesian(XI + PV2T + KT + dist * Math.sin(beta), 0);
        Point kp12 = Point.ofCartesian(kp6.getX() + dist * Math.sin(beta), kp6.getY() - dist * Math.cos(beta));
        Point kp13 = Point.ofCartesian(kp7.getX() + dist * Math.sin(beta), kp7.getY() - dist * Math.cos(beta));
        Point kp14 = Point.ofPolar(RO, 10 * degr);
        Point kp15 = Point.ofCartesian(RO, 0);

        //Center pad
        Point kp16 = Point.ofCartesian(0, CPYP);
        Point kp17 = Point.ofCartesian(BEAMO - (XI - BEAMO + CTRPAD), CPYP);
        Point kp18 = Point.ofCartesian(BEAMO - (XI - BEAMO + CTRPAD), YI + HO + PH1T + KT / 2);
        Point kp19 = Point.ofCartesian(0, YI + HO + PH1T + KT / 2);
        //Horizontal pad
        Point kp20 = Point.ofCartesian(BEAMO - (XI - BEAMO) + PV1T / 2, YI);
        Point kp21 = Point.ofCartesian(BEAMO - LO / 2, YI);
        Point kp22 = Point.ofCartesian(BEAMO - LO / 2 + HO / Math.tan(AL * degr), YI + HO);
        Point kp23 = Point.ofCartesian(BEAMO + LO / 2 - HO / Math.tan(AL * degr), YI + HO);
        Point kp24 = Point.ofCartesian(BEAMO + LO / 2, YI);
        Point kp25 = Point.ofCartesian(XI - PV1T / 2, YI);
        Point kp26 = Point.ofCartesian(XI - PV1T / 2, YI + HO + PH1T + KT / 2);
        Point kp27 = Point.ofCartesian(XI - PV1T / 2 - PHSW, YI + HO + PH1T + KT / 2);
        Point kp28 = Point.ofCartesian(XI - PV1T / 2 - PHSW, YI + HO + PH1T);
        Point kp29 = Point.ofCartesian(BEAMO - (XI - PV1T / 2 - PHSW - BEAMO), YI + HO + PH1T);
        Point kp30 = Point.ofCartesian(BEAMO - (XI - PV1T / 2 - PHSW - BEAMO), YI + HO + PH1T + KT / 2);
        Point kp31 = Point.ofCartesian(BEAMO - (XI - PV1T / 2 - BEAMO), YI + HO + PH1T + KT / 2);

        //Vertical pad
        Point kp32 = Point.ofCartesian(XI, 0);
        Point kp33 = Point.ofCartesian(XI + PV2T, 0);
        Point kp34 = Point.ofCartesian(XI + PV2T, YI);
        Point kp35 = Point.ofCartesian(XI + PV2T + KT / 2, YI);
        Point kp36 = Point.ofCartesian(XI + PV2T + KT / 2, YI + HO + PH1T + KT / 2);
        Point kp37 = Point.ofCartesian(XI, YI + HO + PH1T + KT / 2);
//        Point kp38 = Point.ofCartesian(XI + (PV2T+ KT/2)/Math.sqrt(2),YI + HO + PH1T - PV2T + (PV2T+ KT/2)/Math.sqrt(2));

        //-----------------------------------------LINES----------------------------
        //Upper yoke
        Line ln1 = Line.ofEndPoints(kp1, kp2);
        Arc ln2 = Arc.ofThreePoints(kp2, kp3, kp4);
        Line ln3 = Line.ofEndPoints(kp4, kp5);
        Line ln4 = Line.ofEndPoints(kp5, kp8);
        Line ln5 = Line.ofEndPoints(kp8, kp9);
        Arc ln6 = Arc.ofThreePoints(kp9, kp10, kp1);

        //Lateral yoke
        Line ln7 = Line.ofEndPoints(kp13, kp12);
        Line ln8 = Line.ofEndPoints(kp12, kp11);
        Line ln9 = Line.ofEndPoints(kp11, kp15);
        Arc ln10 = Arc.ofThreePoints(kp15, kp14, kp13);

        //Center pad
        Line ln11 = Line.ofEndPoints(kp16, kp17);
        Line ln12 = Line.ofEndPoints(kp17, kp18);
        Line ln13 = Line.ofEndPoints(kp18, kp19);
        Line ln14 = Line.ofEndPoints(kp19, kp16);

        //Horizontal pad
        Line ln15 = Line.ofEndPoints(kp20, kp21);
        Line ln16 = Line.ofEndPoints(kp21, kp22);
        Line ln17 = Line.ofEndPoints(kp22, kp23);
        Line ln18 = Line.ofEndPoints(kp23, kp24);
        Line ln19 = Line.ofEndPoints(kp24, kp25);
        Line ln20 = Line.ofEndPoints(kp25, kp26);
        Line ln21 = Line.ofEndPoints(kp26, kp27);
        Line ln22 = Line.ofEndPoints(kp27, kp28);
        Line ln23 = Line.ofEndPoints(kp28, kp29);
        Line ln24 = Line.ofEndPoints(kp29, kp30);
        Line ln25 = Line.ofEndPoints(kp30, kp31);
        Line ln26 = Line.ofEndPoints(kp31, kp20);

        //Vertical pad
        Line ln27 = Line.ofEndPoints(kp32, kp33);
        Line ln28 = Line.ofEndPoints(kp33, kp34);
        Line ln29 = Line.ofEndPoints(kp34, kp35);
        Line ln30 = Line.ofEndPoints(kp35, kp36);
        Line ln31 = Line.ofEndPoints(kp36, kp37);
        Line ln32 = Line.ofEndPoints(kp37, kp32);

        //--------------------------------------AREAS----------------------------------

        //Upper yoke
        HyperArea ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4, ln5, ln6});
        //Lateral yoke
        HyperArea ar2 = Area.ofHyperLines(new HyperLine[]{ln7, ln8, ln9, ln10});
        //Center pad
        HyperArea ar3 = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        //Horizontal pad
        HyperArea ar4 = Area.ofHyperLines(new HyperLine[]{ln15, ln16, ln17, ln18, ln19, ln20, ln21, ln22, ln23, ln24, ln25, ln26});
        //Vertical pad
        HyperArea ar5 = Area.ofHyperLines(new HyperLine[]{ln27, ln28, ln29, ln30, ln31, ln32});


        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);
        Element el3 = new Element("IY_El3", ar3);
        Element el4 = new Element("IY_El4", ar4);
        Element el5 = new Element("IY_El5", ar5);


        return new Element[]{el1, el2, el3, el4, el5};
    }

    public Element[] holes_yoke() {

        double L1 = 120e-3;
        double H1 = 200e-3;
        double R1 = 16e-3;

        //Points
        Point kp38 = Point.ofCartesian(L1 - R1, H1);
        Point kp39 = Point.ofCartesian(L1, H1 - R1);
        Point kp40 = Point.ofCartesian(L1 + R1, H1);
        Point kp41 = Point.ofCartesian(L1, H1 + R1);

        //Lines
        Arc ln33 = Arc.ofThreePoints(kp38, kp39, kp40);
        Arc ln34 = Arc.ofThreePoints(kp40, kp41, kp38);

        //Areas
        HyperArea ar6 = Area.ofHyperLines(new HyperLine[]{ln33, ln34});

        Element el6 = new Element("IY_El1_HOLE1", ar6);

        return new Element[]{el6};
    }
}
