package input.FCC.CosTheta_v22b_37_optd7f8;


import model.domains.Domain;
import model.domains.database.AirDomain;
import model.domains.database.AirFarFieldDomain;
import model.domains.database.CoilDomain;
import model.domains.database.IronDomain;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

/**
 * Created by mprioli on 13/07/2016.
 */
public class Magnet_FCC_cosTheta_v22b_37_optd7f8_DA {

    private final Domain[] domains;

    public Magnet_FCC_cosTheta_v22b_37_optd7f8_DA() {

        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("coil", MatDatabase.MAT_COIL, coil()),
                new IronDomain("ironYoke", MatDatabase.MAT_IRON1, iron_yoke()),
                new AirDomain("alShell", MatDatabase.MAT_AIR, al_shell()),
                new AirDomain("ssShell", MatDatabase.MAT_AIR, ss_shell()),
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);


        return new Element[]{el1};
    }

    public Element[] airFarField() {
        double r = 1.5;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        // AREAS
        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});

        // ELEMENTS
        Element el1_far = new Element("FAR_El1", ar1_far);

        return new Element[]{el1_far};
    }

    public Coil coil() {

        Point kp11 = Point.ofCartesian(23.9896e-3,8.9930e-3);
        Point kp12 = Point.ofCartesian(24.9988e-3,0.2500e-3);
        Point kp13 = Point.ofCartesian(38.4989e-3,0.2500e-3);
        Point kp14 = Point.ofCartesian(37.4815e-3,9.4641e-3);
        
        Point kp21 = Point.ofCartesian(17.1648e-3,19.0673e-3);
        Point kp22 = Point.ofCartesian(23.0488e-3,9.7859e-3);
        Point kp23 = Point.ofCartesian(35.1826e-3,15.704e-3);
        Point kp24 = Point.ofCartesian(29.029e-3,25.5091e-3);

        Point kp31 = Point.ofCartesian(12.413e-3,22.1618e-3);
        Point kp32 = Point.ofCartesian(15.8007e-3,19.3736e-3);
        Point kp33 = Point.ofCartesian(25.0078e-3,29.247e-3);
        Point kp34 = Point.ofCartesian(21.4464e-3,32.1944e-3);

        Point kp41 = Point.ofCartesian(6.3225e-3,24.4343e-3);
        Point kp42 = Point.ofCartesian(10.3682e-3,22.751e-3);
        Point kp43 = Point.ofCartesian(15.8592e-3,35.084e-3);
        Point kp44 = Point.ofCartesian(11.5974e-3,36.8612e-3);

        Point kp51 = Point.ofCartesian(15.1535e-3,36.392e-3);
        Point kp52 = Point.ofCartesian(18.9076e-3,34.1102e-3);
        Point kp53 = Point.ofCartesian(26.8428e-3,45.032e-3);
        Point kp54 = Point.ofCartesian(22.8968e-3,47.4507e-3);

        Point kp61 = Point.ofCartesian(20.8415e-3,33.7199e-3);
        Point kp62 = Point.ofCartesian(35.2931e-3,17.2646e-3);
        Point kp63 = Point.ofCartesian(46.3518e-3,25.008e-3);
        Point kp64 = Point.ofCartesian(31.1832e-3,42.3977e-3);

        Point kp71 = Point.ofCartesian(36.534e-3,15.4978e-3);
        Point kp72 = Point.ofCartesian(38.9992e-3,0.25e-3);
        Point kp73 = Point.ofCartesian(52.4993e-3,0.25e-3);
        Point kp74 = Point.ofCartesian(50.009e-3,16.322e-3);

        Point kp81 = Point.ofCartesian(46.3776e-3,26.7214e-3);
        Point kp82 = Point.ofCartesian(52.9994e-3,0.25e-3);
        Point kp83 = Point.ofCartesian(66.9495e-3,0.25e-3);
        Point kp84 = Point.ofCartesian(60.1561e-3,28.9033e-3);

        Point kp91 = Point.ofCartesian(37.0397e-3,38.3055e-3);
        Point kp92 = Point.ofCartesian(45.2863e-3,27.5345e-3);
        Point kp93 = Point.ofCartesian(57.3675e-3,34.5096e-3);
        Point kp94 = Point.ofCartesian(48.5365e-3,46.2068e-3);

        Point kp101 = Point.ofCartesian(26.4199e-3,46.2072e-3);
        Point kp102 = Point.ofCartesian(28.8659e-3,44.4495e-3);
        Point kp103 = Point.ofCartesian(37.8328e-3,55.136e-3);
        Point kp104 = Point.ofCartesian(35.1991e-3,57.0485e-3);

        Point kp111 = Point.ofCartesian(41.6312e-3,53.6045e-3);
        Point kp112 = Point.ofCartesian(53.2237e-3,41.4335e-3);
        Point kp113 = Point.ofCartesian(65.3049e-3,48.4086e-3);
        Point kp114 = Point.ofCartesian(52.9883e-3,61.7052e-3);

        Point kp121 = Point.ofCartesian(54.5494e-3,40.6359e-3);
        Point kp122 = Point.ofCartesian(67.4496e-3,0.25e-3);
        Point kp123 = Point.ofCartesian(81.3997e-3,0.25e-3);
        Point kp124 = Point.ofCartesian(68.0853e-3,44.0102e-3);


        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, kp0);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, kp0);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, kp0);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, kp0);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, kp0);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Arc ln51 = Arc.ofEndPointsCenter(kp52, kp51, kp0);
        Line ln52 = Line.ofEndPoints(kp52, kp53);
        Arc ln53 = Arc.ofEndPointsCenter(kp53, kp54, kp0);
        Line ln54 = Line.ofEndPoints(kp51, kp54);

        Arc ln61 = Arc.ofEndPointsCenter(kp62, kp61, kp0);
        Line ln62 = Line.ofEndPoints(kp62, kp63);
        Arc ln63 = Arc.ofEndPointsCenter(kp63, kp64, kp0);
        Line ln64 = Line.ofEndPoints(kp61, kp64);

        Arc ln71 = Arc.ofEndPointsCenter(kp72, kp71, kp0);
        Line ln72 = Line.ofEndPoints(kp72, kp73);
        Arc ln73 = Arc.ofEndPointsCenter(kp73, kp74, kp0);
        Line ln74 = Line.ofEndPoints(kp71, kp74);

        Arc ln81 = Arc.ofEndPointsCenter(kp82, kp81, kp0);
        Line ln82 = Line.ofEndPoints(kp82, kp83);
        Arc ln83 = Arc.ofEndPointsCenter(kp83, kp84, kp0);
        Line ln84 = Line.ofEndPoints(kp81, kp84);

        Arc ln91 = Arc.ofEndPointsCenter(kp92, kp91, kp0);
        Line ln92 = Line.ofEndPoints(kp92, kp93);
        Arc ln93 = Arc.ofEndPointsCenter(kp93, kp94, kp0);
        Line ln94 = Line.ofEndPoints(kp91, kp94);

        Arc ln101 = Arc.ofEndPointsCenter(kp102, kp101, kp0);
        Line ln102 = Line.ofEndPoints(kp102, kp103);
        Arc ln103 = Arc.ofEndPointsCenter(kp103, kp104, kp0);
        Line ln104 = Line.ofEndPoints(kp101, kp104);

        Arc ln111 = Arc.ofEndPointsCenter(kp112, kp111, kp0);
        Line ln112 = Line.ofEndPoints(kp112, kp113);
        Arc ln113 = Arc.ofEndPointsCenter(kp113, kp114, kp0);
        Line ln114 = Line.ofEndPoints(kp111, kp114);

        Arc ln121 = Arc.ofEndPointsCenter(kp122, kp121, kp0);
        Line ln122 = Line.ofEndPoints(kp122, kp123);
        Arc ln123 = Arc.ofEndPointsCenter(kp123, kp124, kp0);
        Line ln124 = Line.ofEndPoints(kp121, kp124);

        // positive winding cross section, pole 1:

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});
        Area ha15p = Area.ofHyperLines(new HyperLine[]{ln51, ln52, ln53, ln54});
        Area ha16p = Area.ofHyperLines(new HyperLine[]{ln61, ln62, ln63, ln64});
        Area ha17p = Area.ofHyperLines(new HyperLine[]{ln71, ln72, ln73, ln74});
        Area ha18p = Area.ofHyperLines(new HyperLine[]{ln81, ln82, ln83, ln84});
        Area ha19p = Area.ofHyperLines(new HyperLine[]{ln91, ln92, ln93, ln94});
        Area ha110p = Area.ofHyperLines(new HyperLine[]{ln101, ln102, ln103, ln104});
        Area ha111p = Area.ofHyperLines(new HyperLine[]{ln111, ln112, ln113, ln114});
        Area ha112p = Area.ofHyperLines(new HyperLine[]{ln121, ln122, ln123, ln124});

        // negative winding cross section, pole 1:

        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();
        Area ha15n = ha15p.mirrorY();
        Area ha16n = ha16p.mirrorY();
        Area ha17n = ha17p.mirrorY();
        Area ha18n = ha18p.mirrorY();
        Area ha19n = ha19p.mirrorY();
        Area ha110n = ha110p.mirrorY();
        Area ha111n = ha111p.mirrorY();
        Area ha112n = ha112p.mirrorY();

        Cable cableHF = new CableHF_FCC_cosTheta_v22b_37_optd7f8();
        Cable cableLF = new CableLF_FCC_cosTheta_v22b_37_optd7f8();

        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 4, 4, cableHF);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n},  new int[]{+1, -1}, 5, 5, cableHF);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w15 = Winding.ofAreas(new Area[]{ha15p, ha15n},  new int[]{+1, -1}, 2, 2, cableHF);
        Winding w16 = Winding.ofAreas(new Area[]{ha16p, ha16n},  new int[]{+1, -1}, 10, 10, cableHF);
        Winding w17 = Winding.ofAreas(new Area[]{ha17p, ha17n},  new int[]{+1, -1}, 7, 7, cableHF);
        Winding w18 = Winding.ofAreas(new Area[]{ha18p, ha18n},  new int[]{+1, -1}, 18, 18, cableLF);
        Winding w19 = Winding.ofAreas(new Area[]{ha19p, ha19n},  new int[]{+1, -1}, 9, 9, cableLF);
        Winding w110 = Winding.ofAreas(new Area[]{ha110p, ha110n},  new int[]{+1, -1}, 2, 2, cableLF);
        Winding w111 = Winding.ofAreas(new Area[]{ha111p, ha111n},  new int[]{+1, -1}, 11, 11, cableLF);
        Winding w112 = Winding.ofAreas(new Area[]{ha112p, ha112n},  new int[]{+1, -1}, 28, 28, cableLF);

        // windings pole 2:
//        Winding w21 = w11.mirrorX();
//        Winding w22 = w12.mirrorX();
//        Winding w23 = w13.mirrorX();
//        Winding w24 = w14.mirrorX();
//        Winding w25 = w15.mirrorX();
//        Winding w26 = w16.mirrorX();
//        Winding w27 = w17.mirrorX();
//        Winding w28 = w18.mirrorX();
//        Winding w29 = w19.mirrorX();
//        Winding w210 = w110.mirrorX();
//        Winding w211 = w111.mirrorX();
//        Winding w212 = w112.mirrorX();

        w15 = w15.reverseWindingDirection();
        w16 = w16.reverseWindingDirection();
        w17 = w17.reverseWindingDirection();
        w111 = w111.reverseWindingDirection();
        w112 = w112.reverseWindingDirection();

//        w25 = w25.reverseWindingDirection();
//        w26 = w26.reverseWindingDirection();
//        w27 = w27.reverseWindingDirection();
//        w211 = w211.reverseWindingDirection();
//        w212 = w212.reverseWindingDirection();

//        w21 = w21.reverseWindingDirection();
//        w22 = w22.reverseWindingDirection();
//        w23 = w23.reverseWindingDirection();
//        w24 = w24.reverseWindingDirection();
//        w28 = w28.reverseWindingDirection();
//        w29 = w29.reverseWindingDirection();
//        w210 = w210.reverseWindingDirection();

        Winding[] windings = {w11, w12, w13, w14, w15, w16, w17, w18, w19, w110, w111, w112};

//        Winding[][] windings= {{w11, w12, w13, w14, w15, w16, w17, w18, w19, w110, w111, w112}, {w21, w22, w23, w24, w25, w26, w27, w28, w29, w210, w211, w212}};

//        Winding[][] windings= {{w11, w12, w13, w14, w15, w16, w17, w18, w19, w110, w111, w112}, {w212, w211, w210, w29, w28, w27, w26, w25, w24, w23, w22, w21}};

        Pole p1 = Pole.ofWindings(windings);
        Pole[] poles = {p1};

        Coil coil = Coil.ofPoles(poles);
        coil = coil.translate(102e-3, 0);

        return coil;
    }

    public Element[] iron_yoke() {

        //file generated by Xhermes 10.0
        //HyperMesh;
        //COMMENTS
        //''

        //VARIABLES
        double ang      = Math.PI/180;
        double oradius  = 300e-3;
        double padthick = 2e-3;
        double holerad  = 52.5e-3;
        double apcenter = 102e-3;
        double holecent = 235e-3;
        double padx     = 203.315e-3;
        double pady     = 115e-3;

        //NODES
        Point kp1  = Point.ofCartesian(padx+padthick,0);
        Point kp2  = Point.ofCartesian(oradius,0);
        Point kp3  = Point.ofPolar(oradius, Math.atan((pady+padthick/2)/(padx+padthick)));
        Point kp4  = Point.ofCartesian(padx+padthick,pady+padthick/2);
        Point kp5  = Point.ofCartesian(0,pady+padthick);
        Point kp6  = Point.ofCartesian(padx+padthick/2,pady+padthick);
        Point kp7  = Point.ofPolar(oradius, Math.atan((pady+padthick)/(padx+padthick/2)));
        Point kp8  = Point.ofCartesian(0,oradius);
        Point kp9  = Point.ofCartesian(0,holecent+holerad);
        Point kp10 = Point.ofCartesian(0,holecent-holerad);
        Point kp11 = Point.ofCartesian(holerad,holecent);

        //LINES
        Line ln1  = Line.ofEndPoints(kp1,kp2);
        Arc ln2  = Arc.ofEndPointsRadius(kp3,kp2,oradius);
        Line ln3  = Line.ofEndPoints(kp4,kp3);
        Line ln4  = Line.ofEndPoints(kp1,kp4);
        Line ln5  = Line.ofEndPoints(kp5,kp6);
        Line ln6  = Line.ofEndPoints(kp6,kp7);
        Arc ln7  = Arc.ofEndPointsRadius(kp8,kp7,oradius);
        Line ln8  = Line.ofEndPoints(kp8,kp9);
        Arc ln9  = Arc.ofEndPointsRadius(kp9,kp11,holerad);
        Arc ln10 = Arc.ofEndPointsRadius(kp11,kp10,holerad);
        Line ln11 = Line.ofEndPoints(kp10,kp5);

        //AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1,ln2,ln3,ln4});
        Area ar2 = Area.ofHyperLines(new HyperLine[]{ln5,ln6,ln7,ln8,ln9,ln10,ln11});

        //HOLES

        //MESH
//        Lmesh(ln1,7);
//        Lmesh(ln7,15);

        // ELEMENTS
        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);

        return new Element[]{el1, el2};
    }

    private Element[] al_shell(){
        double thickShell = 50 * 1e-3;
        double innerRadius  = 300e-3;

        Point kpins8    = Point.ofCartesian(0,0);
        Point kpShell_1_in  = Point.ofCartesian(innerRadius,0);
        Point kpShell_1_out = Point.ofCartesian(innerRadius+thickShell,0);
        Point kpShell_2_in  = Point.ofCartesian(0,innerRadius);
        Point kpShell_2_out = Point.ofCartesian(0,innerRadius+thickShell);

        Line lnShell1 = Line.ofEndPoints(kpShell_1_in,kpShell_1_out);
        Arc lnShell2 = Arc.ofEndPointsCenter(kpShell_1_out,kpShell_2_out,kpins8);
        Line lnShell3 = Line.ofEndPoints(kpShell_2_out,kpShell_2_in);
        Arc lnShell4 = Arc.ofEndPointsCenter(kpShell_2_in,kpShell_1_in,kpins8);

        Area arShell = Area.ofHyperLines(new HyperLine[]{lnShell1,lnShell2,lnShell3,lnShell4});

        // Quadrant 1
        Element el1_1 = new Element("AL_SHELL_El1", arShell);

        return new Element[] {el1_1};
    }

    private Element[] ss_shell(){
        double thickShell = 20 * 1e-3;
        double innerRadius  = 350e-3;

        Point kpins8    = Point.ofCartesian(0,0);
        Point kpShell_1_in  = Point.ofCartesian(innerRadius,0);
        Point kpShell_1_out = Point.ofCartesian(innerRadius+thickShell,0);
        Point kpShell_2_in  = Point.ofCartesian(0,innerRadius);
        Point kpShell_2_out = Point.ofCartesian(0,innerRadius+thickShell);

        Line lnShell1 = Line.ofEndPoints(kpShell_1_in,kpShell_1_out);
        Arc lnShell2 = Arc.ofEndPointsCenter(kpShell_1_out,kpShell_2_out,kpins8);
        Line lnShell3 = Line.ofEndPoints(kpShell_2_out,kpShell_2_in);
        Arc lnShell4 = Arc.ofEndPointsCenter(kpShell_2_in,kpShell_1_in,kpins8);

        Area arShell = Area.ofHyperLines(new HyperLine[]{lnShell1,lnShell2,lnShell3,lnShell4});

        // Quadrant 1
        Element el1_1 = new Element("SS_SHELL_El1", arShell);

        return new Element[] {el1_1};
    }

}