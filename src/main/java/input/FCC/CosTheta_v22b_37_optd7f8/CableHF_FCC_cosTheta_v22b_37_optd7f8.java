package input.FCC.CosTheta_v22b_37_optd7f8;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class CableHF_FCC_cosTheta_v22b_37_optd7f8 extends Cable {

    public CableHF_FCC_cosTheta_v22b_37_optd7f8() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_HF_FCC";

        //Insulation
        this.wInsulNarrow = 1.5e-4; // [m];
        this.wInsulWide = 1.5e-4; // [m];
        //Filament
        this.dFilament = 50e-6; // [m];
        //Strand
        this.dstrand = 1.1e-3; // [m];
        double CuScRatio = 0.9;
        this.fracCu = 1/(1+(1/CuScRatio));
        this.fracSc = 1/(1+CuScRatio);
        this.RRR = 100;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 14e-3; // [m];

        //Cable
        this.wBare = 13.2e-3; // [m];
        this.hInBare = 1.892e-3; // [m];
        this.hOutBare = 2.0072e-3; // [m];
        this.noOfStrands = 22;
        this.noOfStrandsPerLayer = 11;
        this.noOfLayers = 2;
        this.lTpStrand = 0.100; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 65821.9; // [A]; not used
        this.C2 = -5042.6; // [A/T]; not used
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_FCC;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_NIST;
    }



}