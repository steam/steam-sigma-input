package input;

import model.geometry.Element;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.IntStream;

/**
 * Created by STEAM on 09/11/2016.
 */
public class UtilsUserInput {

    private int[] quadrantsToBeConstructed;

    public int[] getQuadrantsToBeConstructed() {
        return quadrantsToBeConstructed;
    }

    public void setQuadrantsToBeConstructed(int[] quadrantsToBeConstructed) {
        this.quadrantsToBeConstructed = quadrantsToBeConstructed;
    }

    public Element[] quadrantsToBuild(Element[] quad1, Element[] quad2, Element[] quad3, Element[] quad4) {

        Collection<Element> elementsCollection = new ArrayList<>(quad1.length + quad2.length + quad3.length + quad4.length);

//        List<Element[]> constrName = new ArrayList<>();

        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 1)){
            if (quad1 != null){
//                constrName.add(quad1);
                for (Element val : quad1) {
                    elementsCollection.add(val);
                }

            }}
        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 2)){
            if (quad2 != null){
//                constrName.add(quad2);
                for (Element val : quad2) {
                    elementsCollection.add(val);
                }

            }}
        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 3)){
            if (quad3 != null){
//                constrName.add(quad3);
                for (Element val : quad3) {
                    elementsCollection.add(val);
                }
            }}
        if (IntStream.of(quadrantsToBeConstructed).anyMatch(x -> x == 4)){
            if (quad4 != null){
//                constrName.add(quad4);
                for (Element val : quad4) {
                    elementsCollection.add(val);
                }
            }}

//        Element[] simpleArray = new Element[constrName.size()];
//        constrName.toArray(simpleArray);
//        return simpleArray;

        Element[] elementsToBuild = new Element[elementsCollection.size()];
        elementsCollection.toArray(elementsToBuild);
        return elementsToBuild;

    }

}
