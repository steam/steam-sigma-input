package input.HL_LHC.MQXF;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 05/09/2016. TALES #492 and roxie_vs10.cadata as references
 */
public class Cable_MQXF extends Cable {

    public Cable_MQXF() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_MQXF";

        //Insulation
        this.wInsulNarrow = 1.45e-4; // [m];
        this.wInsulWide = 1.45e-4; // [m];
        //Filament
        this.dFilament = 56.0e-6; // [m];
        //Strand
        this.dstrand = 0.85e-3; // [m];
        this.fracCu = 1.15 /(1+1.15);
        this.fracSc = 1/(1+1.15);
        this.RRR = 140;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 15e-6; // [ohm]; from TALES
        this.Ra = 15e-6; // ; // [ohm];
        this.fRhoEff = 1.5; // [1]; from TALES
        this.lTp = 19e-3; // [m];
        //Bare cable
        this.wBare = 1.8363e-2; // [m]; Roxie/Tales sheet 3 width (not sheet 4)
        this.hInBare = 1.53e-3; // [m];
        this.hOutBare = 1.658e-3; // [m];
        this.noOfStrands = 40;
        this.noOfStrandsPerLayer = 20;
        this.noOfLayers = 2;
        this.lTpStrand = 0.135; // [m];
        this.wCore = 12e-3; // [m];
        this.hCore = 25e-6; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 0; // [A];
        this.C2 = 0; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_Nb3Sn_NIST;
        this.insulationMaterial = MatDatabase.MAT_GLASSFIBER;
        this.materialInnerVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialOuterVoids = MatDatabase.MAT_GLASSFIBER;
        this.materialCore = MatDatabase.MAT_STEEL;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_CUDI;
    }


}