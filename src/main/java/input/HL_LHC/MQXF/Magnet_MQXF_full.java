package input.HL_LHC.MQXF;


import input.UtilsUserInput;
import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;

import java.io.FileNotFoundException;


/**
 * Created by STEAM on 31/08/2016. MQXF, Q2a/b as reference.
 */
public class Magnet_MQXF_full extends UtilsUserInput {
    private final Domain[] domains;

    // VARIABLES

    // GENERAL VARIABLES

    // Design variables added by Per HAGEN (for easier to deal with warm measurements)(Taken from Magnet_MB.java)
    double mm = 0.001;
    double deg = Math.PI / 180;
    //shrink  = 0.998;			// yoke/insert shrink
    double DCONT = 0.0;
    double shrink = 1.0 * (1 - DCONT);
    //cshrink = 0.9973;			// collar shrink
    double DCCONT = 0.0;
    double cshrink = 1.0 * (1 - DCCONT);
    //csshrink = 0.996;			// coil_OLD sheet shrink
    double DCSCONT = 0.0;
    double csshrink = 1.0 * (1 - DCSCONT);
    double BEAMD = 97.00;
    double beamd = BEAMD * mm;

    // YOKE SPECIFIC VARIABLES

    double pi           = 3.14159265;
    double deg2r        = pi/180;
    double wkeypole  = 15*mm;
    double dshellth  = 0*mm;
    double dvesselth = 0*mm;
    double dpadth    = 0*mm;
    double dkeyy     = 0*mm;
    double dkeyh     = 0*mm;
    double deng      = 0*mm;
    double dbusdeg   = 0;
    double dtooldeg  = 0;
    double dblady    = 0*mm;
    double dbladh    = 0*mm;
    double dhcorner  = 0*mm;
    double r1        = 75*mm;
    double r21       = 93.813*mm;
    double r22       = 94.313*mm;
    double r3        = 113.126*mm;
    double collarin  = 115*mm;
    double padin     = 133*mm;
    double collarth  = padin-collarin;
    double fillth    = collarin-r3;
    double padsep    = 40*mm;
    double eng       = 33*mm+deng;
    double hcorner   = 60*mm+dhcorner;
    double rcoltie   = 9*mm/2;
    double xcoltie   = 118*mm;
    double ycoltie   = 45*mm;
    double identd    = 4*mm;
    double identw    = 12*mm;
    double padth     = 41.5*mm+dpadth;
    double padout    = padin+padth;
    double colpadth  = collarth+padth;
    double rpadtie   = 16*mm/2;
    double xpadtie   = 140*mm;
    double ypadtie   = 82*mm;
    double rrod      = 36*mm/2;
    double rhole     = 40*mm/2;
    double rwash     = 28*mm;
    double xhole     = 155*mm*Math.sqrt(2)/2;
    double yhole     = 155*mm*Math.sqrt(2)/2;
    double alkeyh    = 12*mm;
    double keyy      = 24.5*mm+dkeyy-dkeyh;
    double keyh      = 13.5*mm+dkeyh;
    double blady     = keyy+keyh+4*mm-dkeyy-dbladh+dblady;
    double bladh     = 58*mm+dbladh;
    double padcut    = 6*mm;
    double slot      = 3*mm;
    double mastersep = 3*mm;
    double slotb     = 3*mm;
    double slota     = 5*mm;
    double sloti     = 5*mm;
    double pmasterth = 15*mm;
    double pmastery  = blady+bladh+pmasterth-dblady+1*mm;
    double outerd    = 630*mm;
    double vesselout = outerd/2;
    double vesselth  = 8*mm+dvesselth;
    double vesselin  = vesselout-vesselth;
    double vesselgap = 0*mm;
    double shellout  = vesselin-vesselgap;
    double shellth   = 29*mm+dshellth;
    double shellin   = shellout-shellth;
    double ymasterth = pmasterth;
    double ymastery  = pmastery;
    double yokein    = padout+mastersep;
    double yokeout   = shellin;
    double yokesep   = 12*mm;
    double grwidth   = 9.5*mm;
    double grdepth   = 3*mm;
    double ryoketie  = 24*mm/2;
    double xyoketie  = 226*mm;
    double yyoketie  = 78*mm;
    double ryokecool = 77*mm/2;
    double xyokecool = 227.5*mm*Math.sqrt(2)/2;
    double yyokecool = 227.5*mm*Math.sqrt(2)/2;
    double busdeg    = 30+dbusdeg;
    double busw      = 20*mm;
    double bush      = 50*mm;
    double tooldeg   = 17+dtooldeg;
    double toolcut   = 20*mm;
    double rcryo1    = 0.445;
    double rcryo2    = 0.457;
    double rvessel1  = vesselin;
    double rvessel2  = vesselout;
    double off       = 0.080;
    double slot_sep  = 24*mm;
    double slot_h    = 15*mm;
    double slot_w    = 10*mm;
    double slot_hcut = 0*mm;
    double slot_wcut = 2*mm;
    double rfillet   = 15*mm;
    double xpin = yokeout;
    double ypin = 0*mm;
    double dpin = 8*mm;
    double rpin = dpin/2;
    double midp = (padout+pmastery)/2;
    double dist = Math.sqrt( (xyokecool-midp)*(xyokecool-midp)+(yyokecool-midp)*(yyokecool-midp) );
    double ang = Math.acos(dist/ryokecool)+5/4*pi;
    // distfill = rfillet*Tan(22.5);
    // distfill = rfillet*Sin(22.5)/Cos(22.5);
    // distfill = 0.06-0.0537867965644;
    double distfill = 0.0062132034356;

    double midp2 = (padin+hcorner)/2;


    public Magnet_MQXF_full() throws FileNotFoundException {
        //Should you need a dedicated BH characteristics for your iron yoke, please update the database under src/main/resources
        String bhCurveDatabasePath = Magnet_MQXF_full.class.getClassLoader().getResource("bh-curve-database.txt").getFile();

        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, elem_air()),
                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, elem_airFarField()),
                new CoilDomain("c1", MatDatabase.MAT_COIL, elem_coil()),
                new WedgeDomain("wed1", MatDatabase.MAT_COPPER, wedges()),
//                new InsulationDomain("foilCoil1", "copperFoil1", insulation1(coil_OLD,"coil_OLD", 0, 0)), //Missing to set the proper insulation material G10
//                new InsulationDomain("foilCoil2", "poly1", insulation2(coil_OLD,"coil_OLD", 0, 0)),
                new IronDomain("ironYoke", bhCurveDatabasePath, "BH_SIGMA", iron_yoke()),
                new HoleDomain("holesYoke", MatDatabase.MAT_AIR, holes_yoke()),
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    // Cryostat model
    public Element[] elem_air() {
        // POINTS

        double r = 1.0;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar1.mirrorY().mirrorX();
        Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);
        Element el3 = new Element("AIR_El3", ar3);
        Element el4 = new Element("AIR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1};
        Element[] quad2 = {el2};
        Element[] quad3 = {el3};
        Element[] quad4 = {el4};
        Element[] elementsToBuild = {el1, el2, el3, el4};

        return elementsToBuild;
    }

    public Element[] elem_airFarField() {
        // POINTS

        double r = 1;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);
//        Point kp1 = Point.ofCartesian(r, 0);
//        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(kp1.getX() * 1.05, kp1.getY() * 1.05);
        Point kp2_far = Point.ofCartesian(kp2.getX() * 1.05, kp2.getY() * 1.05);

        // LINES
        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});
        Area ar2_far = ar1_far.mirrorY();
        Area ar3_far = ar1_far.mirrorY().mirrorX();
        Area ar4_far = ar1_far.mirrorX();

        Element el1_far = new Element("FAR_El1", ar1_far);
        Element el2_far = new Element("FAR_El2", ar2_far);
        Element el3_far = new Element("FAR_El3", ar3_far);
        Element el4_far = new Element("FAR_El4", ar4_far);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_far};
        Element[] quad2 = {el2_far};
        Element[] quad3 = {el3_far};
        Element[] quad4 = {el4_far};
        Element[] elementsToBuild = {el1_far, el2_far, el3_far, el4_far};
        return elementsToBuild;
    }

    public Coil elem_coil() {
        Point kp11 = Point.ofCartesian(90.1696e-3, 29.2393e-3);
        Point kp12 = Point.ofCartesian(94.3082e-3, 0.3750e-3);
        Point kp13 = Point.ofCartesian(112.9613e-3, 0.3750e-3);
        Point kp14 = Point.ofCartesian(108.7068e-3, 31.3153e-3);

        Point kp21 = Point.ofCartesian(79.9152e-3, 50.4593e-3);
        Point kp22 = Point.ofCartesian(89.2050e-3, 30.7168e-3);
        Point kp23 = Point.ofCartesian(106.6419e-3, 37.3421e-3);
        Point kp24 = Point.ofCartesian(96.7375e-3, 58.5183e-3);

        Point kp31 = Point.ofCartesian(69.3076e-3, 30.9361e-3);
        Point kp32 = Point.ofCartesian(74.9936e-3, 0.3750e-3);
        Point kp33 = Point.ofCartesian(93.6467e-3, 0.3750e-3);
        Point kp34 = Point.ofCartesian(87.8299e-3, 33.1413e-3);

        Point kp41 = Point.ofCartesian(61.3600e-3, 43.6988e-3);
        Point kp42 = Point.ofCartesian(65.8958e-3, 35.7799e-3);
        Point kp43 = Point.ofCartesian(82.9363e-3, 43.3669e-3);
        Point kp44 = Point.ofCartesian(78.1257e-3, 51.8749e-3);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, kp0);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, kp0);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, kp0);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, kp0);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, kp0);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});

        // negative WindingObj cross section, pole 1:
        Area ha11n = ha11p.mirrorY().rotate(- Math.PI/2);
        Area ha12n = ha12p.mirrorY().rotate(- Math.PI/2);
        Area ha13n = ha13p.mirrorY().rotate(- Math.PI/2);
        Area ha14n = ha14p.mirrorY().rotate(- Math.PI/2);

        Cable cable_MQXF  = new Cable_MQXF();

        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 16, 16, cable_MQXF.setLabel("cable_MQXF_RRR140").setRRR(140));
        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n}, new int[]{+1, -1}, 12, 12, cable_MQXF.setLabel("cable_MQXF_RRR140").setRRR(140));
        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n}, new int[]{+1, -1}, 17, 17, cable_MQXF.setLabel("cable_MQXF_RRR140").setRRR(140));
        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n}, new int[]{+1, -1},  5, 5, cable_MQXF.setLabel("cable_MQXF_RRR140").setRRR(140));

        Pole p1 = Pole.ofWindings(new Winding[]{w11,w12,w13,w14});
        Pole p2 = p1.mirrorY();
        Pole p3 = p1.mirrorY().mirrorX();
        Pole p4 = p1.mirrorX();

        // Coil:
        Coil c1 = Coil.ofPoles(new Pole[]{p1,p2,p3,p4});
        return c1;

//        // windings pole 2:
//        Winding w21_R = w11_R.mirrorY();
//        Winding w22_R = w12_R.mirrorY();
//        Winding w23_R = w13_R.mirrorY();
//        Winding w24_R = w14_R.mirrorY();
//
//        // windings pole 3:
//        Winding_OLD w31_R = w21_R.mirrorX();
//        Winding_OLD w32_R = w22_R.mirrorX();
//        Winding_OLD w33_R = w23_R.mirrorX();
//        Winding_OLD w34_R = w24_R.mirrorX();
//
//        // windings pole 4:
//        Winding_OLD w41_R = w11_R.mirrorX();
//        Winding_OLD w42_R = w12_R.mirrorX();
//        Winding_OLD w43_R = w13_R.mirrorX();
//        Winding_OLD w44_R = w14_R.mirrorX();
//
//        w11_R = w11_R.reverseWindingDirection();
//        w12_R = w12_R.reverseWindingDirection();
//
//        w23_R = w23_R.reverseWindingDirection();
//        w24_R = w24_R.reverseWindingDirection();
//
//        w43_R = w43_R.reverseWindingDirection();
//        w44_R = w44_R.reverseWindingDirection();
//
//        w31_R = w31_R.reverseWindingDirection();
//        w32_R = w32_R.reverseWindingDirection();

//        Winding_OLD[][] windings = {{w11_R, w12_R, w13_R, w14_R, w21_R, w22_R, w23_R, w24_R}};
//        Winding_OLD[][] windings = {{w13_R, w14_R, w12_R, w11_R}, {w21_R, w22_R, w24_R, w23_R}, {w41_R, w42_R, w44_R, w43_R}, {w33_R, w34_R, w32_R, w31_R}};
//        Winding_OLD[][] windings= {{w13_R, w14_R, w12_R, w11_R}};

//        int n_poles = 4;
//        Coil_OLD coil = Coil_OLD.ofWindings(windings, n_poles);
//        return coil;
    }

    public Element[] iron_yoke() {

        // NODES

        // pad 1st octant
        Point kppident1 = Point.ofCartesian(padin-identd, 0);
        Point kppident2 = Point.ofCartesian(padin-identd, identw/2);
        Point kppident3 = Point.ofCartesian(padin, identw/2);
        Point kppcor = Point.ofCartesian(padin, hcorner);
        Point kppfllt1 = Point.ofCartesian(padin, hcorner-distfill);
        Point kppfllt2 = Point.ofCartesian(padin-distfill/Math.sqrt(2.0), hcorner+distfill/Math.sqrt(2.0));
        Point kppalkey1 = Point.ofCartesian(padout-slota, 0);
        Point kppalkey2 = Point.ofCartesian(padout-slota, alkeyh/2);
        Point kppalkey3 = Point.ofCartesian(padout, alkeyh/2);
        Point kppkey1 = Point.ofCartesian(padout, keyy);
        Point kppkey2 = Point.ofCartesian(padout-sloti, keyy);
        Point kppkey3 = Point.ofCartesian(padout-sloti, keyy+keyh);
        Point kppkey4 = Point.ofCartesian(padout, keyy+keyh);
        Point kppblad1 = Point.ofCartesian(padout, blady);
        Point kppblad2 = Point.ofCartesian(padout-slotb, blady);
        Point kppblad3 = Point.ofCartesian(padout-slotb, blady+bladh);
        Point kppblad4 = Point.ofCartesian(padout, blady+bladh);
        Point kppmtop = Point.ofCartesian(padout, pmastery);
        // Point kppeng1 = Point.ofCartesian(padin-eng/Math.sqrt(2), hcorner+eng/Math.sqrt(2));
        // Point kppcool1 = Point.ofCartesian(xyokecool+ryokecool*Math.cos(ang), yyokecool+ryokecool*Math.sin(ang));
        // Point kppcool2 = Point.ofCartesian(xyokecool+ryokecool*Math.cos(Asin(Math.sqrt(2*(hcorner+2*eng/Math.sqrt(2)-padin)*(hcorner+2*eng/Math.sqrt(2)-padin))/2/ryokecool)-3*pi/4), yyokecool+ryokecool*Math.sin(Asin(Math.sqrt(2*(hcorner+2*eng/Math.sqrt(2)-padin)*(hcorner+2*eng/Math.sqrt(2)-padin))/2/ryokecool)-3*pi/4));
        Point kppeng1 = Point.ofCartesian(midp2+padsep/2/Math.sqrt(2), midp2-padsep/2/Math.sqrt(2));
        // Point kppcool1 = Point.ofCartesian(kppmtop.getX-padcut, kppmtop.getY+padcut);
        Point kppcool1 = Point.ofCartesian(kppmtop.getX(), kppmtop.getY() +padcut);
        Point kppcool2 = Point.ofCartesian(kppcool1.getY()-(kppeng1.getY()-kppeng1.getX()),kppcool1.getY());

        // pad 2nd octant
        Point kppident1s = Point.ofCartesian(kppident1.getY(),kppident1.getX());
        Point kppident2s = Point.ofCartesian(kppident2.getY(),kppident2.getX());
        Point kppident3s = Point.ofCartesian(kppident3.getY(),kppident3.getX());
        Point kppcors    = Point.ofCartesian(kppcor.getY(),kppcor.getX());
        Point kppfllt1s  = Point.ofCartesian(kppfllt1.getY(),kppfllt1.getX());
        Point kppfllt2s  = Point.ofCartesian(kppfllt2.getY(),kppfllt2.getX());
        Point kppalkey1s = Point.ofCartesian(kppalkey1.getY(),kppalkey1.getX());
        Point kppalkey2s = Point.ofCartesian(kppalkey2.getY(),kppalkey2.getX());
        Point kppalkey3s = Point.ofCartesian(kppalkey3.getY(),kppalkey3.getX());
        Point kppkey1s   = Point.ofCartesian(kppkey1.getY(),kppkey1.getX());
        Point kppkey2s   = Point.ofCartesian(kppkey2.getY(),kppkey2.getX());
        Point kppkey3s   = Point.ofCartesian(kppkey3.getY(),kppkey3.getX());
        Point kppkey4s   = Point.ofCartesian(kppkey4.getY(),kppkey4.getX());
        Point kppblad1s  = Point.ofCartesian(kppblad1.getY(),kppblad1.getX());
        Point kppblad2s  = Point.ofCartesian(kppblad2.getY(),kppblad2.getX());
        Point kppblad3s  = Point.ofCartesian(kppblad3.getY(),kppblad3.getX());
        Point kppblad4s  = Point.ofCartesian(kppblad4.getY(),kppblad4.getX());
        Point kppmtops   = Point.ofCartesian(kppmtop.getY(),kppmtop.getX());
        Point kppeng1s   = Point.ofCartesian(kppeng1.getY(),kppeng1.getX());
        Point kppcool1s  = Point.ofCartesian(kppcool1.getY(),kppcool1.getX());
        Point kppcool2s  = Point.ofCartesian(kppcool2.getY(),kppcool2.getX());

        // yoke 1st octant
        Point kpyalkey1 = Point.ofCartesian(yokein+slota, 0);
        Point kpyalkey2 = Point.ofCartesian(yokein+slota, alkeyh/2);
        Point kpyalkey3 = Point.ofCartesian(yokein, alkeyh/2);
        Point kpykey1 = Point.ofCartesian(yokein, keyy);
        Point kpykey2 = Point.ofCartesian(yokein+sloti, keyy);
        Point kpykey3 = Point.ofCartesian(yokein+sloti, keyy+keyh);
        Point kpykey4 = Point.ofCartesian(yokein, keyy+keyh);
        Point kpyblad1 = Point.ofCartesian(yokein, blady);
        Point kpyblad2 = Point.ofCartesian(yokein+slotb, blady);
        Point kpyblad3 = Point.ofCartesian(yokein+slotb, blady+bladh);
        Point kpyblad4 = Point.ofCartesian(yokein, blady+bladh);
        Point kpytool1 = Point.ofPolar(yokeout, (45-tooldeg)*deg2r);
        Point kpytool2 = Point.ofCartesian(kpytool1.getX()-toolcut, kpytool1.getY());
        Point kpytool3 = Point.ofCartesian(kpytool2.getX(), Math.sqrt(yokeout*yokeout-kpytool2.getX()*kpytool2.getX()));
        Point kpyout1 = Point.ofPolar(yokeout, 0);
        Point kpyout2 = Point.ofPolar(yokeout, (pi/4-Math.asin(yokesep/2/yokeout)));
        Point kpysep1 = Point.ofCartesian(kpyout2.getX()-Math.sqrt(0.5*grwidth*grwidth), kpyout2.getY()-Math.sqrt(0.5*grwidth*grwidth));
        Point kpysep2 = Point.ofCartesian(kpysep1.getX()-Math.sqrt(0.5*grdepth*grdepth), kpysep1.getY()+Math.sqrt(0.5*grdepth*grdepth));
        Point kpycool1 = Point.ofCartesian(xyokecool+ryokecool*Math.cos(-Math.asin((yokesep/2-grdepth)/ryokecool)+pi/4), yyokecool+ryokecool*Math.sin(-Math.asin((yokesep/2-grdepth)/ryokecool)+pi/4));
        // Point kpycool1 = Point.ofCartesian(xyokecool+ryokecool*Math.cos(-Math.asin(yokesep/2/ryokecool)+pi/4), yyokecool+ryokecool*Math.sin(-Math.asin(yokesep/2/ryokecool)+pi/4));
        Point kpycool2 = Point.ofCartesian(yokein, yyokecool-Math.sqrt(ryokecool*ryokecool-(yokein-xyokecool)*(yokein-xyokecool)));
        Point kpyslot1 = Point.ofPolar(yokeout, Math.asin(slot_sep/2/yokeout));
        Point kpyslot2 = Point.ofCartesian(kpyslot1.getX()-slot_w, kpyslot1.getY());
        Point kpyslot3 = Point.ofCartesian(kpyslot1.getX()-slot_w, kpyslot1.getY()+slot_h);
        Point kpyslot5 = Point.ofPolar(yokeout, Math.asin((slot_sep/2+slot_h+slot_hcut)/yokeout));
        Point kpyslot4 = Point.ofCartesian(kpyslot5.getX()-slot_wcut, kpyslot1.getY()+slot_h);
        Point kpypin1 = Point.ofCartesian(yokeout-rpin, 0);
        Point kpypin2 = Point.ofPolar(yokeout, 2*Math.asin(rpin/2/yokeout));

        // yoke 2nd octant
        Point kpyalkey1s = Point.ofCartesian(kpyalkey1.getY(), kpyalkey1.getX());
        Point kpyalkey2s = Point.ofCartesian(kpyalkey2.getY(), kpyalkey2.getX());
        Point kpyalkey3s = Point.ofCartesian(kpyalkey3.getY(), kpyalkey3.getX());
        Point kpykey1s   = Point.ofCartesian(kpykey1.getY(), kpykey1.getX());
        Point kpykey2s   = Point.ofCartesian(kpykey2.getY(), kpykey2.getX());
        Point kpykey3s   = Point.ofCartesian(kpykey3.getY(), kpykey3.getX());
        Point kpykey4s   = Point.ofCartesian(kpykey4.getY(), kpykey4.getX());
        Point kpyblad1s  = Point.ofCartesian(kpyblad1.getY(), kpyblad1.getX());
        Point kpyblad2s  = Point.ofCartesian(kpyblad2.getY(), kpyblad2.getX());
        Point kpyblad3s  = Point.ofCartesian(kpyblad3.getY(), kpyblad3.getX());
        Point kpyblad4s  = Point.ofCartesian(kpyblad4.getY(), kpyblad4.getX());
        Point kpytool1s  = Point.ofCartesian(kpytool1.getY(), kpytool1.getX());
        Point kpytool2s  = Point.ofCartesian(kpytool2.getY(), kpytool2.getX());
        Point kpytool3s  = Point.ofCartesian(kpytool3.getY(), kpytool3.getX());
        Point kpyout1s   = Point.ofCartesian(kpyout1.getY(), kpyout1.getX());
        Point kpyout2s   = Point.ofCartesian(kpyout2.getY(), kpyout2.getX());
        Point kpysep1s   = Point.ofCartesian(kpysep1.getY(), kpysep1.getX());
        Point kpysep2s   = Point.ofCartesian(kpysep2.getY(), kpysep2.getX());
        Point kpycool1s  = Point.ofCartesian(kpycool1.getY(), kpycool1.getX());
        Point kpycool2s  = Point.ofCartesian(kpycool2.getY(), kpycool2.getX());
        Point kpyslot1s  = Point.ofCartesian(kpyslot1.getY(), kpyslot1.getX());
        Point kpyslot2s  = Point.ofCartesian(kpyslot2.getY(), kpyslot2.getX());
        Point kpyslot3s  = Point.ofCartesian(kpyslot3.getY(), kpyslot3.getX());
        Point kpyslot4s  = Point.ofCartesian(kpyslot4.getY(), kpyslot4.getX());
        Point kpyslot5s  = Point.ofCartesian(kpyslot5.getY(), kpyslot5.getX());
        Point kpypin1s   = Point.ofCartesian(kpypin1.getY(), kpypin1.getX());
        Point kpypin2s   = Point.ofCartesian(kpypin2.getY(), kpypin2.getX());


        // LINES

        // pad 1st octant
        Line lnpmed     = Line.ofEndPoints(kppident1,kppalkey1);
        Line lnpalkey1  = Line.ofEndPoints(kppalkey1,kppalkey2);
        Line lnpalkey2  = Line.ofEndPoints(kppalkey2,kppalkey3);
        Line lnpm1      = Line.ofEndPoints(kppalkey3,kppkey1);
        Line lnpkey1    = Line.ofEndPoints(kppkey1,kppkey2);
        Line lnpkey2    = Line.ofEndPoints(kppkey2,kppkey3);
        Line lnpkey3    = Line.ofEndPoints(kppkey3,kppkey4);
        Line lnpm2      = Line.ofEndPoints(kppkey4,kppblad1);
        Line lnpblad1   = Line.ofEndPoints(kppblad1,kppblad2);
        Line lnpblad2   = Line.ofEndPoints(kppblad2,kppblad3);
        Line lnpblad3   = Line.ofEndPoints(kppblad3,kppblad4);
        Line lnpm3      = Line.ofEndPoints(kppblad4,kppmtop);
        Line lnpm4      = Line.ofEndPoints(kppmtop,kppcool1);
        // Arc lnpcool    = Arc.ofEndPointsRadius(kppcool1,kppcool2,ryokecool);
        Line lnpcool    = Line.ofEndPoints(kppcool1,kppcool2);
        Line lnpeng     = Line.ofEndPoints(kppcool2,kppeng1);
        // Line lnpcol1    = Line.ofEndPoints(kppeng1,kppcor);
        Line lnpcol1    = Line.ofEndPoints(kppeng1,kppfllt2);
        Arc lnpfllt    = Arc.ofEndPointsRadius(kppfllt2,kppfllt1,rfillet);
        Line lnpcol2    = Line.ofEndPoints(kppfllt1,kppident3);
        // Line lnpcol2    = Line.ofEndPoints(kppcor,kppident3);
        Line lnpident1  = Line.ofEndPoints(kppident3,kppident2);
        Line lnpident2  = Line.ofEndPoints(kppident2,kppident1);

        // pad 2nd octant
        Line lnpmeds     = Line.ofEndPoints(kppident1s,kppalkey1s);
        Line lnpalkey1s  = Line.ofEndPoints(kppalkey1s,kppalkey2s);
        Line lnpalkey2s  = Line.ofEndPoints(kppalkey2s,kppalkey3s);
        Line lnpm1s      = Line.ofEndPoints(kppalkey3s,kppkey1s);
        Line lnpkey1s    = Line.ofEndPoints(kppkey1s,kppkey2s);
        Line lnpkey2s    = Line.ofEndPoints(kppkey2s,kppkey3s);
        Line lnpkey3s    = Line.ofEndPoints(kppkey3s,kppkey4s);
        Line lnpm2s      = Line.ofEndPoints(kppkey4s,kppblad1s);
        Line lnpblad1s   = Line.ofEndPoints(kppblad1s,kppblad2s);
        Line lnpblad2s   = Line.ofEndPoints(kppblad2s,kppblad3s);
        Line lnpblad3s   = Line.ofEndPoints(kppblad3s,kppblad4s);
        Line lnpm3s      = Line.ofEndPoints(kppblad4s,kppmtops);
        Line lnpm4s      = Line.ofEndPoints(kppmtops,kppcool1s);
        // Arc lnpcools    = Arc.ofEndPointsRadius(kppcool2s,kppcool1s,ryokecool);
        Line lnpcools    = Line.ofEndPoints(kppcool1s,kppcool2s);
        Line lnpengs     = Line.ofEndPoints(kppcool2s,kppeng1s);
        // Line lnpcol1s    = Line.ofEndPoints(kppeng1s,kppcors);
        Line lnpcol1s    = Line.ofEndPoints(kppeng1s,kppfllt2s);
        Arc lnpfllts    = Arc.ofEndPointsRadius(kppfllt1s,kppfllt2s,rfillet);
        Line lnpcol2s    = Line.ofEndPoints(kppfllt1s,kppident3s);
        // Line lnpcol2s    = Line.ofEndPoints(kppcors,kppident3s);
        Line lnpident1s  = Line.ofEndPoints(kppident3s,kppident2s);
        Line lnpident2s  = Line.ofEndPoints(kppident2s,kppident1s);

        // yoke 1st octant
        // Line lnymed     = Line.ofEndPoints(kpyalkey1,kpyout1);
        Line lnymed     = Line.ofEndPoints(kpyalkey1,kpypin1);
        Arc lnypin     = Arc.ofEndPointsRadius(kpypin1,kpypin2,rpin);
        Arc lnyout1    = Arc.ofEndPointsRadius(kpyslot1,kpypin2,yokeout);
        // Arc lnyout1    = Arc.ofEndPointsRadius(kpyslot1,kpyout1,yokeout);
        Line lnyslot1     = Line.ofEndPoints(kpyslot1,kpyslot2);
        Line lnyslot2     = Line.ofEndPoints(kpyslot2,kpyslot3);
        Line lnyslot3     = Line.ofEndPoints(kpyslot3,kpyslot4);
        Line lnyslot4     = Line.ofEndPoints(kpyslot4,kpyslot5);
        Arc lnyout2    = Arc.ofEndPointsRadius(kpytool1,kpyslot5,yokeout);
        Line lnytool1   = Line.ofEndPoints(kpytool1,kpytool2);
        Line lnytool2   = Line.ofEndPoints(kpytool2,kpytool3);
        Arc lnyout3    = Arc.ofEndPointsRadius(kpyout2,kpytool3,yokeout);
        Line lnysep1     = Line.ofEndPoints(kpyout2,kpysep1);
        Line lnysep2     = Line.ofEndPoints(kpysep1,kpysep2);
        Line lnysep3     = Line.ofEndPoints(kpysep2,kpycool1);
        // Line lnysep     = Line.ofEndPoints(kpyout2,kpycool1);
        Arc lnycool    = Arc.ofEndPointsRadius(kpycool1,kpycool2,ryokecool);
        Line lnym1      = Line.ofEndPoints(kpycool2,kpyblad4);
        Line lnyblad1   = Line.ofEndPoints(kpyblad4,kpyblad3);
        Line lnyblad2   = Line.ofEndPoints(kpyblad3,kpyblad2);
        Line lnyblad3   = Line.ofEndPoints(kpyblad2,kpyblad1);
        Line lnym2      = Line.ofEndPoints(kpyblad1,kpykey4);
        Line lnykey1    = Line.ofEndPoints(kpykey4,kpykey3);
        Line lnykey2    = Line.ofEndPoints(kpykey3,kpykey2);
        Line lnykey3    = Line.ofEndPoints(kpykey2,kpykey1);
        Line lnym3      = Line.ofEndPoints(kpykey1,kpyalkey3);
        Line lnyalkey1  = Line.ofEndPoints(kpyalkey3,kpyalkey2);
        Line lnyalkey2  = Line.ofEndPoints(kpyalkey2,kpyalkey1);

        // yoke 2nd octant
        // Line lnymeds     = Line.ofEndPoints(kpyalkey1s,kpyout1s);
        Line lnymeds     = Line.ofEndPoints(kpyalkey1s,kpypin1s);
        Arc lnypins     = Arc.ofEndPointsRadius(kpypin2s,kpypin1s,rpin);
        Arc lnyout1s    = Arc.ofEndPointsRadius(kpypin2s,kpyslot1s,yokeout);
        // Arc lnyout1s    = Arc.ofEndPointsRadius(kpyout1s,kpyslot1s,yokeout);
        Line lnyslot1s     = Line.ofEndPoints(kpyslot1s,kpyslot2s);
        Line lnyslot2s     = Line.ofEndPoints(kpyslot2s,kpyslot3s);
        Line lnyslot3s     = Line.ofEndPoints(kpyslot3s,kpyslot4s);
        Line lnyslot4s     = Line.ofEndPoints(kpyslot4s,kpyslot5s);
        Arc lnyout2s    = Arc.ofEndPointsRadius(kpyslot5s,kpytool1s,yokeout);
        Line lnytool1s   = Line.ofEndPoints(kpytool1s,kpytool2s);
        Line lnytool2s   = Line.ofEndPoints(kpytool2s,kpytool3s);
        Arc lnyout3s    = Arc.ofEndPointsRadius(kpytool3s,kpyout2s,yokeout);
        Line lnysep1s     = Line.ofEndPoints(kpyout2s,kpysep1s);
        Line lnysep2s     = Line.ofEndPoints(kpysep1s,kpysep2s);
        Line lnysep3s     = Line.ofEndPoints(kpysep2s,kpycool1s);
        // Line lnyseps     = Line.ofEndPoints(kpyout2s,kpycool1s);
        Arc lnycools    = Arc.ofEndPointsRadius(kpycool2s,kpycool1s,ryokecool);
        Line lnym1s      = Line.ofEndPoints(kpycool2s,kpyblad4s);
        Line lnyblad1s   = Line.ofEndPoints(kpyblad4s,kpyblad3s);
        Line lnyblad2s   = Line.ofEndPoints(kpyblad3s,kpyblad2s);
        Line lnyblad3s   = Line.ofEndPoints(kpyblad2s,kpyblad1s);
        Line lnym2s      = Line.ofEndPoints(kpyblad1s,kpykey4s);
        Line lnykey1s    = Line.ofEndPoints(kpykey4s,kpykey3s);
        Line lnykey2s    = Line.ofEndPoints(kpykey3s,kpykey2s);
        Line lnykey3s    = Line.ofEndPoints(kpykey2s,kpykey1s);
        Line lnym3s      = Line.ofEndPoints(kpykey1s,kpyalkey3s);
        Line lnyalkey1s  = Line.ofEndPoints(kpyalkey3s,kpyalkey2s);
        Line lnyalkey2s  = Line.ofEndPoints(kpyalkey2s,kpyalkey1s);


        // AREAS 1st QUADRANT

        // Area aryoke     = Area.ofHyperLines(new HyperLine[]{lnymed,lnypin,lnyout1,lnyslot1,lnyslot2,lnyslot3,lnyslot4,lnyout2,lnytool1,lnytool2,lnyout3,lnysep,lnycool,lnym1,lnyblad1,lnyblad2,lnyblad3,lnym2,lnykey1,lnykey2,lnykey3,lnym3,lnyalkey1,lnyalkey2});
        Area aryoke_1     = Area.ofHyperLines(new HyperLine[]{lnymed,lnypin,lnyout1,lnyslot1,lnyslot2,lnyslot3,lnyslot4,lnyout2,lnytool1,lnytool2,lnyout3,lnysep1,lnysep2,lnysep3,lnycool,lnym1,lnyblad1,lnyblad2,lnyblad3,lnym2,lnykey1,lnykey2,lnykey3,lnym3,lnyalkey1,lnyalkey2});
        // Area aryokes    = Area.ofHyperLines(new HyperLine[]{lnyalkey2s,lnyalkey1s,lnym3s,lnykey3s,lnykey2s,lnykey1s,lnym2s,lnyblad3s,lnyblad2s,lnyblad1s,lnym1s,lnycools,lnyseps,lnyout3s,lnytool2s,lnytool1s,lnyout2s,lnyslot4s,lnyslot3s,lnyslot2s,lnyslot1s,lnyout1s,lnypins,lnymeds});
        Area aryokes_1    = Area.ofHyperLines(new HyperLine[]{lnyalkey2s,lnyalkey1s,lnym3s,lnykey3s,lnykey2s,lnykey1s,lnym2s,lnyblad3s,lnyblad2s,lnyblad1s,lnym1s,lnycools,lnysep3s,lnysep2s,lnysep1s,lnyout3s,lnytool2s,lnytool1s,lnyout2s,lnyslot4s,lnyslot3s,lnyslot2s,lnyslot1s,lnyout1s,lnypins,lnymeds});
        // Area arpad      = Area.ofHyperLines(new HyperLine[]{lnpmed,lnpalkey1,lnpalkey2,lnpm1,lnpkey1,lnpkey2,lnpkey3,lnpm2,lnpblad1,lnpblad2,lnpblad3,lnpm3,lnpm4,lnpcool,lnpeng,lnpcol1,lnpcol2,lnpident1,lnpident2});
        Area arpad_1      = Area.ofHyperLines(new HyperLine[]{lnpmed,lnpalkey1,lnpalkey2,lnpm1,lnpkey1,lnpkey2,lnpkey3,lnpm2,lnpblad1,lnpblad2,lnpblad3,lnpm3,lnpm4,lnpcool,lnpeng,lnpcol1,lnpfllt,lnpcol2,lnpident1,lnpident2});
        // Area arpads     = Area.ofHyperLines(new HyperLine[]{lnpident2s,lnpident1s,lnpcol2s,lnpcol1s,lnpengs,lnpcools,lnpm4s,lnpm3s,lnpblad3s,lnpblad2s,lnpblad1s,lnpm2s,lnpkey3s,lnpkey2s,lnpkey1s,lnpm1s,lnpalkey2s,lnpalkey1s,lnpmeds});
        Area arpads_1     = Area.ofHyperLines(new HyperLine[]{lnpident2s,lnpident1s,lnpcol2s,lnpfllts,lnpcol1s,lnpengs,lnpcools,lnpm4s,lnpm3s,lnpblad3s,lnpblad2s,lnpblad1s,lnpm2s,lnpkey3s,lnpkey2s,lnpkey1s,lnpm1s,lnpalkey2s,lnpalkey1s,lnpmeds});

        // AREAS 2nd QUADRANT
        Area aryoke_2 = aryoke_1.mirrorY();
        Area aryokes_2 = aryokes_1.mirrorY();
        Area arpad_2 = arpad_1.mirrorY();
        Area arpads_2 = arpads_1.mirrorY();

        // AREAS 3rd QUADRANT
        Area aryoke_3 = aryoke_2.mirrorX();
        Area aryokes_3 = aryokes_2.mirrorX();
        Area arpad_3 = arpad_2.mirrorX();
        Area arpads_3 = arpads_2.mirrorX();

        // AREAS 4th QUADRANT
        Area aryoke_4 = aryoke_1.mirrorX();
        Area aryokes_4 = aryokes_1.mirrorX();
        Area arpad_4 = arpad_1.mirrorX();
        Area arpads_4 = arpads_1.mirrorX();

        // ELEMENTS

        Element el1_1 = new Element("IY1_El1", aryoke_1);
        Element el1_2 = new Element("IY1_El2", aryokes_1);
        Element el1_3 = new Element("IY1_El3", arpad_1);
        Element el1_4 = new Element("IY1_El4", arpads_1);

        Element el2_1 = new Element("IY2_El1", aryoke_2);
        Element el2_2 = new Element("IY2_El2", aryokes_2);
        Element el2_3 = new Element("IY2_El3", arpad_2);
        Element el2_4 = new Element("IY2_El4", arpads_2);

        Element el3_1 = new Element("IY3_El1", aryoke_3);
        Element el3_2 = new Element("IY3_El2", aryokes_3);
        Element el3_3 = new Element("IY3_El3", arpad_3);
        Element el3_4 = new Element("IY3_El4", arpads_3);

        Element el4_1 = new Element("IY4_El1", aryoke_4);
        Element el4_2 = new Element("IY4_El2", aryokes_4);
        Element el4_3 = new Element("IY4_El3", arpad_4);
        Element el4_4 = new Element("IY4_El4", arpads_4);

        return new Element[]{el1_1, el1_2, el1_3, el1_4, el2_1, el2_2, el2_3, el2_4, el3_1, el3_2, el3_3, el3_4, el4_1, el4_2, el4_3, el4_4};

    }



    // HOLES

    public Element[] holes_yoke() {

        Point kpytie1 = Point.ofCartesian(xyoketie-ryoketie, yyoketie);
        Point kpytie2 = Point.ofCartesian(xyoketie+ryoketie, yyoketie);
        // Added point to define circumference in yoke/lines/yoke 1st octant
        Point kpytiecenter = Point.ofCartesian(xyoketie, yyoketie);

        Point kpytie1s   = Point.ofCartesian(kpytie1.getY(), kpytie1.getX());
        Point kpytie2s   = Point.ofCartesian(kpytie2.getY(), kpytie2.getX());
        // Added point to define circumference in yoke/lines/yoke 2nd octant
        Point kpytiecenters = Point.ofCartesian(kpytiecenter.getY(), kpytiecenter.getX());

        Point kpptie1 = Point.ofCartesian(xpadtie-rpadtie, ypadtie);
        Point kpptie2 = Point.ofCartesian(xpadtie+rpadtie, ypadtie);
        // Added point to define circumference in yoke/lines/pad 1st octant
        Point kpptiecenter = Point.ofCartesian(xpadtie, ypadtie);

        Point kpptie1s   = Point.ofCartesian(kpptie1.getY(),kpptie1.getX());
        Point kpptie2s   = Point.ofCartesian(kpptie2.getY(),kpptie2.getX());
        // Added point to define circumference in yoke/lines/pad 2nd octant
        Point kpptiecenters = Point.ofCartesian(kpptiecenter.getY(), kpptiecenter.getX());

        Circumference lnyoketie  = Circumference.ofCenterRadius(kpytiecenter,ryoketie);
        Circumference lnyoketies  = Circumference.ofCenterRadius(kpytiecenters,ryoketie);
        Circumference lnpadtie   = Circumference.ofCenterRadius(kpptiecenter, rpadtie);
        Circumference lnpadties   = Circumference.ofCenterRadius(kpptiecenters, rpadtie);

        Area aryoketie_1  = Area.ofHyperLines(new HyperLine[]{lnyoketie});
        Area aryoketies_1 = Area.ofHyperLines(new HyperLine[]{lnyoketies});
        Area arpadtie_1   = Area.ofHyperLines(new HyperLine[]{lnpadtie});
        Area arpadties_1  = Area.ofHyperLines(new HyperLine[]{lnpadties});

        Area aryoketie_2 = aryoketie_1.mirrorY();
        Area aryoketies_2 = aryoketies_1.mirrorY();
        Area arpadtie_2 = arpadtie_1.mirrorY();
        Area arpadties_2 = arpadties_1.mirrorY();

        Area aryoketie_3 = aryoketie_2.mirrorX();
        Area aryoketies_3 = aryoketies_2.mirrorX();
        Area arpadtie_3 = arpadtie_2.mirrorX();
        Area arpadties_3 = arpadties_2.mirrorX();

        Area aryoketie_4 = aryoketie_1.mirrorX();
        Area aryoketies_4 = aryoketies_1.mirrorX();
        Area arpadtie_4 = arpadtie_1.mirrorX();
        Area arpadties_4 = arpadties_1.mirrorX();

        Element ho1_1 = new Element("IY1_HOLE1_1", aryoketie_1);
        Element ho1_2 = new Element("IY1_HOLE2_1", aryoketies_1);
        Element ho1_3 = new Element("IY1_HOLE3_1", arpadtie_1);
        Element ho1_4 = new Element("IY1_HOLE4_1", arpadties_1);

        Element ho2_1 = new Element("IY2_HOLE1_1", aryoketie_2);
        Element ho2_2 = new Element("IY2_HOLE2_1", aryoketies_2);
        Element ho2_3 = new Element("IY2_HOLE3_1", arpadtie_2);
        Element ho2_4 = new Element("IY2_HOLE4_1", arpadties_2);

        Element ho3_1 = new Element("IY3_HOLE1_1", aryoketie_3);
        Element ho3_2 = new Element("IY3_HOLE2_1", aryoketies_3);
        Element ho3_3 = new Element("IY3_HOLE3_1", arpadtie_3);
        Element ho3_4 = new Element("IY3_HOLE4_1", arpadties_3);

        Element ho4_1 = new Element("IY4_HOLE1_1", aryoketie_4);
        Element ho4_2 = new Element("IY4_HOLE2_1", aryoketies_4);
        Element ho4_3 = new Element("IY4_HOLE3_1", arpadtie_4);
        Element ho4_4 = new Element("IY4_HOLE4_1", arpadties_4);

        return new Element[]{
                ho1_2, ho1_4, ho2_2, ho2_4};

//        return new Element[]{
//                ho1_1, ho1_2, ho1_3, ho1_4, ho2_1, ho2_2, ho2_3, ho2_4, ho3_1, ho3_2, ho3_3, ho3_4, ho4_1, ho4_2, ho4_3, ho4_4
//        };

    }

    public Element[] wedges(){

        //Wedge 1
        Point kp11_wedge = Point.ofCartesian(89.2050e-3, 30.7168e-3); //kp22
        Point kp14_wedge = Point.ofCartesian(106.6419e-3, 37.3421e-3); //kp23
        double r_in = Math.sqrt(Math.pow(kp11_wedge.getX(),2)+Math.pow(kp11_wedge.getY(),2));
        double r_out = Math.sqrt(Math.pow(kp14_wedge.getX(),2)+Math.pow(kp14_wedge.getY(),2));

        Point kp12_wedge_aux = Point.ofCartesian(90.1696e-3, 29.2393e-3); //kp11
        Point kp13_wedge_aux = Point.ofCartesian(108.7068e-3, 31.3153e-3); //kp14

        double phi_kp12 = kp12_wedge_aux.getPhi();
        double phi_kp13 = kp13_wedge_aux.getPhi();

        Point kp12_wedge = Point.ofCartesian(r_in*Math.cos(phi_kp12), r_in*Math.sin(phi_kp12));
        Point kp13_wedge = Point.ofCartesian(r_out*Math.cos(phi_kp13), r_out*Math.sin(phi_kp13));
//        Point kp13_wedge = Point.ofCartesian(56.7908e-3,16.6538e-3);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11_wedge = Arc.ofEndPointsCenter(kp12_wedge, kp11_wedge, kp0);
        Line ln12_wedge = Line.ofEndPoints(kp12_wedge, kp13_wedge);
        Arc ln13_wedge = Arc.ofEndPointsCenter(kp13_wedge, kp14_wedge, kp0);
        Line ln14_wedge = Line.ofEndPoints(kp11_wedge, kp14_wedge);

        //Wedge 2
        Point kp21_wedge = Point.ofCartesian(65.8958e-3, 35.7799e-3); //kp42
        Point kp24_wedge = Point.ofCartesian(82.9363e-3, 43.3669e-3); // kp43
        double r_in2 = Math.sqrt(Math.pow(kp21_wedge.getX(),2)+Math.pow(kp21_wedge.getY(),2));
        double r_out2 = Math.sqrt(Math.pow(kp24_wedge.getX(),2)+Math.pow(kp24_wedge.getY(),2));

        Point kp22_wedge_aux = Point.ofCartesian(69.3076e-3, 30.9361e-3); // kp31
        Point kp23_wedge_aux = Point.ofCartesian(87.8299e-3, 33.1413e-3); // kp34

        double phi_kp22 = kp22_wedge_aux.getPhi();
        double phi_kp23 = kp23_wedge_aux.getPhi();

        Point kp22_wedge = Point.ofCartesian(r_in2*Math.cos(phi_kp22), r_in2*Math.sin(phi_kp22));
        Point kp23_wedge = Point.ofCartesian(r_out2*Math.cos(phi_kp23), r_out2*Math.sin(phi_kp23));

        Arc ln21_wedge = Arc.ofEndPointsCenter(kp22_wedge, kp21_wedge, kp0);
        Line ln22_wedge = Line.ofEndPoints(kp22_wedge, kp23_wedge);
        Arc ln23_wedge = Arc.ofEndPointsCenter(kp23_wedge, kp24_wedge, kp0);
        Line ln24_wedge = Line.ofEndPoints(kp21_wedge, kp24_wedge);

        // Areas and elements
        Area ar1_wedgeCOil1Q1p = Area.ofHyperLines(new HyperLine[] {ln11_wedge, ln12_wedge, ln13_wedge, ln14_wedge});
        Area ar1_wedgeCOil1Q1n = ar1_wedgeCOil1Q1p.mirrorY().rotate(- Math.PI/2);
        Area ar1_wedgeCOil1Q2p = ar1_wedgeCOil1Q1p.mirrorY();
        Area ar1_wedgeCOil1Q2n = ar1_wedgeCOil1Q1p.rotate(Math.PI/2);
        Area ar1_wedgeCOil1Q3p = ar1_wedgeCOil1Q2n.rotate(Math.PI/2);
        Area ar1_wedgeCOil1Q3n = ar1_wedgeCOil1Q2p.rotate(Math.PI/2);
        Area ar1_wedgeCOil1Q4p = ar1_wedgeCOil1Q1p.mirrorX();
        Area ar1_wedgeCOil1Q4n = ar1_wedgeCOil1Q1p.rotate(- Math.PI/2);

//        ar1_wedgeCOil1Q1.translate(dx,dy);
//        ar1_wedgeCOil1Q2.translate(dx,dy);
//        ar1_wedgeCOil1Q3.translate(dx,dy);
//        ar1_wedgeCOil1Q4.translate(dx,dy);
        Element el1_wedgeCOil1Q1p = new Element("Q1p_El1_wedge", ar1_wedgeCOil1Q1p);
        Element el1_wedgeCOil1Q1n = new Element("Q1n_El1_wedge", ar1_wedgeCOil1Q1n);
        Element el1_wedgeCOil1Q2p = new Element("Q2p_El1_wedge", ar1_wedgeCOil1Q2p);
        Element el1_wedgeCOil1Q2n = new Element("Q2n_El1_wedge", ar1_wedgeCOil1Q2n);
        Element el1_wedgeCOil1Q3p = new Element("Q3p_El1_wedge", ar1_wedgeCOil1Q3p);
        Element el1_wedgeCOil1Q3n = new Element("Q3n_El1_wedge", ar1_wedgeCOil1Q3n);
        Element el1_wedgeCOil1Q4p = new Element("Q4p_El1_wedge", ar1_wedgeCOil1Q4p);
        Element el1_wedgeCOil1Q4n = new Element("Q4n_El1_wedge", ar1_wedgeCOil1Q4n);

        Area ar2_wedgeCOil1Q1p = Area.ofHyperLines(new HyperLine[] {ln21_wedge, ln22_wedge, ln23_wedge, ln24_wedge});
        Area ar2_wedgeCOil1Q1n = ar2_wedgeCOil1Q1p.mirrorY().rotate(- Math.PI/2);
        Area ar2_wedgeCOil1Q2p = ar2_wedgeCOil1Q1p.mirrorY();
        Area ar2_wedgeCOil1Q2n = ar2_wedgeCOil1Q1p.rotate(Math.PI/2);
        Area ar2_wedgeCOil1Q3p = ar2_wedgeCOil1Q2n.rotate(Math.PI/2);
        Area ar2_wedgeCOil1Q3n = ar2_wedgeCOil1Q2p.rotate(Math.PI/2);
        Area ar2_wedgeCOil1Q4p = ar2_wedgeCOil1Q1p.mirrorX();
        Area ar2_wedgeCOil1Q4n = ar2_wedgeCOil1Q1p.rotate(- Math.PI/2);
//        ar2_wedgeCOil1Q1.translate(dx,dy);
//        ar2_wedgeCOil1Q2.translate(dx,dy);   Necessary??????? (activated in MB)
//        ar2_wedgeCOil1Q3.translate(dx,dy);
//        ar2_wedgeCOil1Q4.translate(dx,dy);
        Element el2_wedgeCOil1Q1p = new Element("Q1p_El2_wedge", ar2_wedgeCOil1Q1p);
        Element el2_wedgeCOil1Q1n = new Element("Q1n_El2_wedge", ar2_wedgeCOil1Q1n);
        Element el2_wedgeCOil1Q2p = new Element("Q2p_El2_wedge", ar2_wedgeCOil1Q2p);
        Element el2_wedgeCOil1Q2n = new Element("Q2n_El2_wedge", ar2_wedgeCOil1Q2n);
        Element el2_wedgeCOil1Q3p = new Element("Q3p_El2_wedge", ar2_wedgeCOil1Q3p);
        Element el2_wedgeCOil1Q3n = new Element("Q3n_El2_wedge", ar2_wedgeCOil1Q3n);
        Element el2_wedgeCOil1Q4p = new Element("Q4p_El2_wedge", ar2_wedgeCOil1Q4p);
        Element el2_wedgeCOil1Q4n = new Element("Q4n_El2_wedge", ar2_wedgeCOil1Q4n);

// /        return new Element[] {
//                el1_wedgeCOil1Q1n,el1_wedgeCOil1Q2n,
//                el2_wedgeCOil1Q1n,el2_wedgeCOil1Q2n};

        return new Element[] {
                el1_wedgeCOil1Q1p,el1_wedgeCOil1Q1n,el1_wedgeCOil1Q2p,el1_wedgeCOil1Q2n,el1_wedgeCOil1Q3p,el1_wedgeCOil1Q3n,el1_wedgeCOil1Q4p,el1_wedgeCOil1Q4n,
                el2_wedgeCOil1Q1p,el2_wedgeCOil1Q1n,el2_wedgeCOil1Q2p,el2_wedgeCOil1Q2n,el2_wedgeCOil1Q3p,el2_wedgeCOil1Q3n,el2_wedgeCOil1Q4p,el2_wedgeCOil1Q4n,
        };

//        return new Element[] {
//                el1_wedgeCOil1Q1,
//                el2_wedgeCOil1Q1,
//                el3_wedgeCOil1Q1,
//                el4_wedgeCOil1Q1,
//        };
    }
//
//    public Element[] insulation1(Coil_OLD coil, String coilName, double dx, double dy){
//
//
//        HyperLine blockEdge = coil.getWindings()[0][3].getBlocks()[0].getHyperLines()[1];
//        int noOfPoints = 2;
//        Point[] coord = blockEdge.extractPoints(noOfPoints);
//        Point kp13_foil = coord[1]; //w11
//
//        blockEdge = coil.getWindings()[0][2].getBlocks()[0].getHyperLines()[0];
//        coord = blockEdge.extractPoints(noOfPoints);
//        Point kp14_foil = coord[0]; //w12
//
//        blockEdge = coil.getWindings()[0][1].getBlocks()[0].getHyperLines()[2];
//        coord = blockEdge.extractPoints(noOfPoints);
//        Point kp11_foil = coord[1]; //w14
//
//        blockEdge = coil.getWindings()[0][0].getBlocks()[0].getHyperLines()[1];
//        coord = blockEdge.extractPoints(noOfPoints);
//        Point kp12_foil = coord[1]; //w13
//
//        Point kp0 = Point.ofCartesian(0,0);
////        Arc ln11_foil = Arc.ofEndPointsCenter(kp12_foil, kp11_foil, kp0);
//        Line ln12_foil = Line.ofEndPoints(kp12_foil, kp13_foil);
////        Arc ln13_foil = Arc.ofEndPointsCenter(kp13_foil, kp14_foil, kp0);
//        Line ln14_foil = Line.ofEndPoints(kp11_foil, kp14_foil);
//
//
//
////        Area ar1_foilIntCOil1Q1p = Area.ofHyperLines(new HyperLine[] {ln11_foil, ln12_foil, ln13_foil, ln14_foil});
//        Area ar1_foilIntCOil1Q1p = Area.ofHyperLines(new HyperLine[] {ln12_foil, ln14_foil});
//
//        Area ar1_foilIntCOil1Q1n = ar1_foilIntCOil1Q1p.mirrorY().rotate(- Math.PI/2);
//        Area ar1_foilIntCOil1Q2p = ar1_foilIntCOil1Q1p.mirrorY();
//        Area ar1_foilIntCOil1Q2n = ar1_foilIntCOil1Q1p.rotate(Math.PI/2);
//        Area ar1_foilIntCOil1Q3p = ar1_foilIntCOil1Q2n.rotate(Math.PI/2);
//        Area ar1_foilIntCOil1Q3n = ar1_foilIntCOil1Q2p.rotate(Math.PI/2);
//        Area ar1_foilIntCOil1Q4p = ar1_foilIntCOil1Q1p.mirrorX();
//        Area ar1_foilIntCOil1Q4n = ar1_foilIntCOil1Q1p.rotate(- Math.PI/2);
////        Area ar1_foilIntCOil1Q2 = ar1_foilIntCOil1Q1.translate(-dx,-dy).mirrorY().translate(dx,dy);  Necessary????
////        Area ar1_foilIntCOil1Q3 = ar1_foilIntCOil1Q1.translate(-dx,-dy).mirrorX().translate(dx,dy);
////        Area ar1_foilIntCOil1Q4 = ar1_foilIntCOil1Q3.translate(-dx,-dy).mirrorY().translate(dx,dy);
//        Element el1_foilIntCOil1Q1p = new Element(coilName+"Q1p_El1Int_foil", ar1_foilIntCOil1Q1p);
//        Element el1_foilIntCOil1Q1n = new Element(coilName+"Q1n_El1Int_foil", ar1_foilIntCOil1Q1n);
//        Element el1_foilIntCOil1Q2p = new Element(coilName+"Q2p_El1Int_foil", ar1_foilIntCOil1Q2p);
//        Element el1_foilIntCOil1Q2n = new Element(coilName+"Q2n_El1Int_foil", ar1_foilIntCOil1Q2n);
//        Element el1_foilIntCOil1Q3p = new Element(coilName+"Q3p_El1Int_foil", ar1_foilIntCOil1Q3p);
//        Element el1_foilIntCOil1Q3n = new Element(coilName+"Q3n_El1Int_foil", ar1_foilIntCOil1Q3n);
//        Element el1_foilIntCOil1Q4p = new Element(coilName+"Q4p_El1Int_foil", ar1_foilIntCOil1Q4p);
//        Element el1_foilIntCOil1Q4n = new Element(coilName+"Q4n_El1Int_foil", ar1_foilIntCOil1Q4n);
//
//
//
//
//        return new Element[] {el1_foilIntCOil1Q1p,el1_foilIntCOil1Q1n,el1_foilIntCOil1Q2p,el1_foilIntCOil1Q2n, el1_foilIntCOil1Q3p,el1_foilIntCOil1Q3n,el1_foilIntCOil1Q4p,el1_foilIntCOil1Q4n};
////        return new Element[] {el1_foilIntCOil1Q1};
//
//    }
//
//    public Element[] insulation2(Coil_OLD coil, String coilName, double dx, double dy){
//
//
//        HyperLine blockEdge = coil.getWindings()[0][3].getBlocks()[0].getHyperLines()[2];
//        int noOfPoints = 2;
//        Point[] coord = blockEdge.extractPoints(noOfPoints);
//        Point kp11_foil2 = coord[1]; //w11
//
//        blockEdge = coil.getWindings()[0][0].getBlocks()[0].getHyperLines()[1];
//        coord = blockEdge.extractPoints(noOfPoints);
//        Point kp13_foil2 = coord[0]; //w13
//
//        blockEdge = coil.getWindings()[2][0].getBlocks()[0].getHyperLines()[1];
//        coord = blockEdge.extractPoints(noOfPoints);
//        Point kp41_foil2 = coord[1]; //w41
//
//        blockEdge = coil.getWindings()[2][3].getBlocks()[0].getHyperLines()[1];
//        coord = blockEdge.extractPoints(noOfPoints);
//        Point kp43_foil2 = coord[1]; //w41. before 0 0 0 1 1 w13
//
//        Point kp0 = Point.ofCartesian(0,0);
////        Arc ln11_foil = Arc.ofEndPointsCenter(kp12_foil, kp11_foil, kp0);
//        Line ln12_foil2 = Line.ofEndPoints(kp41_foil2, kp11_foil2);
////        Arc ln13_foil = Arc.ofEndPointsCenter(kp13_foil, kp14_foil, kp0);
//        Line ln14_foil2 = Line.ofEndPoints(kp43_foil2, kp13_foil2);
//
//
//
////        Area ar1_foilIntCOil1Q1p = Area.ofHyperLines(new HyperLine[] {ln11_foil, ln12_foil, ln13_foil, ln14_foil});
//        Area ar2_foilIntCOil1Q1 = Area.ofHyperLines(new HyperLine[] {ln12_foil2, ln14_foil2});
//
//        Area ar2_foilIntCOil1Q2 = ar2_foilIntCOil1Q1.rotate(Math.PI/2);
//        Area ar2_foilIntCOil1Q3 = ar2_foilIntCOil1Q1.mirrorY();
//        Area ar2_foilIntCOil1Q4 = ar2_foilIntCOil1Q1.rotate(- Math.PI/2);
//        Element el2_foilIntCOil1Q1 = new Element(coilName+"Q1p_El2Int_foil", ar2_foilIntCOil1Q1);
//        Element el2_foilIntCOil1Q2 = new Element(coilName+"Q2p_El2Int_foil", ar2_foilIntCOil1Q2);
//        Element el2_foilIntCOil1Q3 = new Element(coilName+"Q3p_El2Int_foil", ar2_foilIntCOil1Q3);
//        Element el2_foilIntCOil1Q4 = new Element(coilName+"Q4p_El2Int_foil", ar2_foilIntCOil1Q4);
//
//
//
//        return new Element[] {el2_foilIntCOil1Q1,el2_foilIntCOil1Q2,el2_foilIntCOil1Q3,el2_foilIntCOil1Q4};
////        return new Element[] {el1_foilIntCOil1Q1};
//
//    }


    // Mirrorx;
    // Mirrory;

    // kpcryoin1 = [0.,rcryo1-off];
    // kpcryoin2 = [rcryo1,-off];
    // kpcryoin3 = [0.,-rcryo1-off];

    // kpcryoout1 = [0.,rcryo2-off];
    // kpcryoout2 = [rcryo2,-off];
    // kpcryoout3 = [0.,-rcryo2-off];

    // lncryoin = HyperLine(kpcryoin1,kpcryoin3,"Arc",kpcryoin2);
    // lncryoout = HyperLine(kpcryoout1,kpcryoout3,"Arc",kpcryoout2);
    // lncryotop = HyperLine(kpcryoin1,kpcryoout1,"Line");
    // lncryobottom = HyperLine(kpcryoin3,kpcryoout3,"Line");

    // arcryo = HyperArea(lncryoout,lncryotop,lncryoin,lncryobottom,BHiron1);

    // kpvesselin1 = [0.,rvessel1];
    // kpvesselin2 = [rvessel1,0];
    // kpvesselin3 = [0.,-rvessel1];
    // kpvesselout1 = [0.,rvessel2];
    // kpvesselout2 = [rvessel2,0];
    // kpvesselout3 = [0.,-rvessel2];

    // lnvesselin = HyperLine(kpvesselin1,kpvesselin3,"Arc",kpvesselin2);
    // lnvesselout = HyperLine(kpvesselout1,kpvesselout3,"Arc",kpvesselout2);
    // lnvesseltop = HyperLine(kpvesselin1,kpvesselout1,"Line");
    // lnvesselbottom = HyperLine(kpvesselin3,kpvesselout3,"Line");

    // arvessel = HyperArea(lnvesselout,lnvesseltop,lnvesselin,lnvesselbottom,BHiron5);
}
