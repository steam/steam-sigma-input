package input.HL_LHC.D1;


import input.HL_LHC.MQXF.Magnet_MQXF_1ep;
import input.UtilsUserInput;
import model.domains.Domain;
import model.domains.database.*;
import model.geometry.basic.*;
import model.geometry.coil.Cable;
import model.geometry.coil.Coil;
import model.geometry.coil.Pole;
import model.geometry.coil.Winding;
import model.materials.database.MatDatabase;
import model.geometry.Element;

import java.io.FileNotFoundException;


/**
 * Created by STEAM on 13/07/2016.
 */
public class Magnet_D1 extends UtilsUserInput {

    private final Domain[] domains;

    public Magnet_D1() throws FileNotFoundException {
        String bhCurveDatabasePath = Magnet_MQXF_1ep.class.getClassLoader().getResource("bh-curve-database.txt").getFile();

        domains = new Domain[]{
                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()),
                new CoilDomain("C0", MatDatabase.MAT_COIL, coil()),
//                new WedgeDomain("wedges", MatDatabase.MAT_COIL, wedges()),
                new IronDomain("ironYoke", bhCurveDatabasePath, "BH_SIGMA", iron_yoke()),
                new HoleDomain("holes_yoke", MatDatabase.MAT_AIR, holes_yoke()),
        };
    }

    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        // POINTS

        double r = 1.0;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        Element el2 = new Element("AIR_El2", ar2);
        Element el3 = new Element("AIR_El3", ar3);
        Element el4 = new Element("AIR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        return new Element[]{el1, el2, el3, el4};
    }

    public Element[] airFarField() {
        // POINTS

        double r = 1;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});
        Area ar2_far = ar1_far.mirrorY();
        Area ar3_far = ar2_far.mirrorX();
        Area ar4_far = ar1_far.mirrorX();

        Element el1_far = new Element("FAR_El1", ar1_far);
        Element el2_far = new Element("FAR_El2", ar2_far);
        Element el3_far = new Element("FAR_El3", ar3_far);
        Element el4_far = new Element("FAR_El4", ar4_far);

        return new Element[]{el1_far, el2_far, el3_far, el4_far};
    }

    public Coil coil() {
        Point kp11 = Point.ofCartesian(68.1808e-3, 31.3993e-3);
        Point kp12 = Point.ofCartesian(74.8380e-3, 01.3423e-3);
        Point kp13 = Point.ofCartesian(90.2176e-3, 01.3423e-3);
        Point kp14 = Point.ofCartesian(82.8873e-3, 35.8995e-3);

        Point kp21 = Point.ofCartesian(53.9078e-3, 52.1827e-3);
        Point kp22 = Point.ofCartesian(66.1731e-3, 34.9806e-3);
        Point kp23 = Point.ofCartesian(79.9963e-3, 41.7226e-3);
        Point kp24 = Point.ofCartesian(66.0862e-3, 61.5753e-3);

        Point kp31 = Point.ofCartesian(37.1343e-3, 65.0078e-3);
        Point kp32 = Point.ofCartesian(47.8402e-3, 57.6350e-3);
        Point kp33 = Point.ofCartesian(57.2345e-3, 69.8120e-3);
        Point kp34 = Point.ofCartesian(44.9367e-3, 78.2613e-3);

        Point kp41 = Point.ofCartesian(18.6153e-3, 72.6114e-3);
        Point kp42 = Point.ofCartesian(24.8121e-3, 70.6179e-3);
        Point kp43 = Point.ofCartesian(30.5731e-3, 84.8778e-3);
        Point kp44 = Point.ofCartesian(23.4741e-3, 87.2034e-3);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, kp0);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp21, kp24);

        Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, kp0);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, kp0);
        Line ln34 = Line.ofEndPoints(kp31, kp34);

        Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, kp0);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, kp0);
        Line ln44 = Line.ofEndPoints(kp41, kp44);

        // positive winding cross section, pole 1:
        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});

        // negative winding cross section, pole 1:
        Area ha11n = ha11p.mirrorY();
        Area ha12n = ha12p.mirrorY();
        Area ha13n = ha13p.mirrorY();
        Area ha14n = ha14p.mirrorY();

        Cable cableD1 = new CableD1();

        // windings pole 1:
        Winding w11 = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1, -1}, 19, 9, cableD1);
        Winding w12 = Winding.ofAreas(new Area[]{ha12p, ha12n}, new int[]{+1, -1}, 13, 6, cableD1);
        Winding w13 = Winding.ofAreas(new Area[]{ha13p, ha13n}, new int[]{+1, -1}, 8, 4, cableD1);
        Winding w14 = Winding.ofAreas(new Area[]{ha14p, ha14n}, new int[]{+1, -1}, 4, 2, cableD1);

        // windings pole 2:
        Winding w21 = w11.mirrorX().reverseWindingDirection();
        Winding w22 = w12.mirrorX().reverseWindingDirection();
        Winding w23 = w13.mirrorX().reverseWindingDirection();
        Winding w24 = w14.mirrorX().reverseWindingDirection();

        Winding[][] windings = {{w11, w12, w13, w14}, {w24, w23, w22, w21}};

        Pole p1 = Pole.ofWindings(new Winding[]{w11, w12, w13, w14});
        Pole p2 = Pole.ofWindings(new Winding[]{w21, w22, w23, w24});

        return Coil.ofPoles(new Pole[]{p1, p2});
    }

    public Element[] iron_yoke() {
        double deg2rad = Math.PI / 180;
        double r_int = 0.111;
        double r_ext = 0.2748;

        Point kp1 = Point.ofPolar(r_int, 0 * deg2rad);
        Point kp2 = Point.ofPolar(r_ext, 0 * deg2rad);
        Point kp3 = Point.ofPolar(r_ext, 90 * deg2rad);
        Point kp4 = Point.ofPolar(r_int, 90 * deg2rad);

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln1 = Arc.ofEndPointsCenter(kp1, kp4, kp0);
        Line ln2 = Line.ofEndPoints(kp1, kp2);
        Arc ln3 = Arc.ofEndPointsCenter(kp2, kp3, kp0);
        Line ln4 = Line.ofEndPoints(kp4, kp3);

        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3, ln4});
        Area ar2 = ar1.mirrorY();
        Area ar3 = ar2.mirrorX();
        Area ar4 = ar1.mirrorX();

        Element el1 = new Element("IY_El1", ar1);
        Element el2 = new Element("IY_El2", ar2);
        Element el3 = new Element("IY_El3", ar3);
        Element el4 = new Element("IY_El4", ar4);

        return new Element[]{el1, el2, el3, el4};
//        return new Element[]{el1};
    }

    public Element[] holes_yoke() {

        // VARIABLES
        double mm = 0.001;

        // radius of holes_yoke
        double x1 = 0 * mm;
        double y1 = 190 * mm;
        double r1 = 30 * mm;

        double x2 = 156 * mm;
        double y2 = 109 * mm;
        double r2 = 14 * mm;

        double x3 = 184 * mm;
        double y3 = 49 * mm;
        double r3 = 14 * mm;


        // POINTS
        Point kp1 = Point.ofCartesian(x1, y1);
        Point kp2 = Point.ofCartesian(x2, y2);
        Point kp3 = Point.ofCartesian(x3, y3);

        Point kp1_1 = Point.ofCartesian(x1, y1 + r1);
        Point kp1_3 = Point.ofCartesian(x1 + r1, y1);
        Point kp1_2 = Point.ofCartesian(x1, y1 - r1);
        Arc ln1_1 = Arc.ofThreePoints(kp1_1, kp1_3, kp1_2);
        Line ln1_2 = Line.ofEndPoints(kp1_1, kp1_2);

        // LINES
//        Circumference ln1 = Circumference.ofCenterRadius(kp1, r1);
        Circumference ln2 = Circumference.ofCenterRadius(kp2, r2);
        Circumference ln3 = Circumference.ofCenterRadius(kp3, r3);

        // AREAS
//        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1});
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1_1, ln1_2});
        Area ar2 = Area.ofHyperLines(new HyperLine[]{ln2});
        Area ar3 = Area.ofHyperLines(new HyperLine[]{ln3});

        // MIRRORED AREAS
        Area ar1_2 = ar1.mirrorY();
        Area ar1_3 = ar1_2.mirrorX();
        Area ar1_4 = ar1.mirrorX();

        Area ar2_2 = ar2.mirrorY();
        Area ar2_3 = ar2_2.mirrorX();
        Area ar2_4 = ar2.mirrorX();

        Area ar3_2 = ar3.mirrorY();
        Area ar3_3 = ar3_2.mirrorX();
        Area ar3_4 = ar3.mirrorX();

        // ELEMENTS
        Element ho1 = new Element("IY_HOLE1", ar1);
        Element ho2 = new Element("IY_HOLE2", ar2);
        Element ho3 = new Element("IY_HOLE3", ar3);

        // MIRRORED ELEMENTS
        Element ho1_2 = new Element("IY_HOLE1_2", ar1_2);
        Element ho1_3 = new Element("IY_HOLE1_3", ar1_3);
        Element ho1_4 = new Element("IY_HOLE1_4", ar1_4);

        Element ho2_2 = new Element("IY_HOLE2_2", ar2_2);
        Element ho2_3 = new Element("IY_HOLE2_3", ar2_3);
        Element ho2_4 = new Element("IY_HOLE2_4", ar2_4);

        Element ho3_2 = new Element("IY_HOLE3_2", ar3_2);
        Element ho3_3 = new Element("IY_HOLE3_3", ar3_3);
        Element ho3_4 = new Element("IY_HOLE3_4", ar3_4);

        return new Element[]{ho1, ho2, ho3, ho1_2, ho1_3, ho1_4, ho2_2, ho2_3, ho2_4, ho3_2, ho3_3, ho3_4};
//        return new Element[] {ho1, ho2, ho3};
    }

    public Element[] wedges() {
        //Wedge 1
        Point kp11_wedge = Point.ofCartesian(66.1731e-3, 34.9806e-3);
        Point kp14_wedge = Point.ofCartesian(79.9963e-3, 41.7226e-3);
        double r_in = Math.sqrt(Math.pow(kp11_wedge.getX(), 2) + Math.pow(kp11_wedge.getY(), 2));
        double r_out = Math.sqrt(Math.pow(kp14_wedge.getX(), 2) + Math.pow(kp14_wedge.getY(), 2));

        Point kp12_wedge_aux = Point.ofCartesian(68.1808e-3, 31.3993e-3);
        Point kp13_wedge_aux = Point.ofCartesian(82.8873e-3, 35.8995e-3);

        double phi_kp12 = kp12_wedge_aux.getPhi();
        double phi_kp13 = kp13_wedge_aux.getPhi();

        Point kp12_wedge = Point.ofCartesian(r_in * Math.cos(phi_kp12), r_in * Math.sin(phi_kp12));
        Point kp13_wedge = Point.ofCartesian(r_out * Math.cos(phi_kp13), r_out * Math.sin(phi_kp13));

        Point kp0 = Point.ofCartesian(0, 0);

        Arc ln11_wedge = Arc.ofEndPointsCenter(kp12_wedge, kp11_wedge, kp0);
        Line ln12_wedge = Line.ofEndPoints(kp12_wedge, kp13_wedge);
        Arc ln13_wedge = Arc.ofEndPointsCenter(kp13_wedge, kp14_wedge, kp0);
        Line ln14_wedge = Line.ofEndPoints(kp11_wedge, kp14_wedge);

        Area ar1_q1_wedge = Area.ofHyperLines(new HyperLine[]{ln11_wedge, ln12_wedge, ln13_wedge, ln14_wedge});
        Element el1_q1_wedge = new Element("El1_q1_wedge", ar1_q1_wedge);

        //Wedge 2
        Point kp21_wedge = Point.ofCartesian(47.8402e-3, 57.6350e-3);
        Point kp24_wedge = Point.ofCartesian(57.2345e-3, 69.8120e-3);
        double r_in2 = Math.sqrt(Math.pow(kp21_wedge.getX(), 2) + Math.pow(kp21_wedge.getY(), 2));
        double r_out2 = Math.sqrt(Math.pow(kp24_wedge.getX(), 2) + Math.pow(kp24_wedge.getY(), 2));

        Point kp22_wedge_aux = Point.ofCartesian(53.9078e-3, 52.1827e-3);
        Point kp23_wedge_aux = Point.ofCartesian(66.0862e-3, 61.5753e-3);

        double phi_kp22 = kp22_wedge_aux.getPhi();
        double phi_kp23 = kp23_wedge_aux.getPhi();

        Point kp22_wedge = Point.ofCartesian(r_in2 * Math.cos(phi_kp22), r_in2 * Math.sin(phi_kp22));
        Point kp23_wedge = Point.ofCartesian(r_out2 * Math.cos(phi_kp23), r_out2 * Math.sin(phi_kp23));

        Arc ln21_wedge = Arc.ofEndPointsCenter(kp22_wedge, kp21_wedge, kp0);
        Line ln22_wedge = Line.ofEndPoints(kp22_wedge, kp23_wedge);
        Arc ln23_wedge = Arc.ofEndPointsCenter(kp23_wedge, kp24_wedge, kp0);
        Line ln24_wedge = Line.ofEndPoints(kp21_wedge, kp24_wedge);

        Area ar2_q1_wedge = Area.ofHyperLines(new HyperLine[]{ln21_wedge, ln22_wedge, ln23_wedge, ln24_wedge});
        Element el2_q1_wedge = new Element("El2_q1_wedge", ar2_q1_wedge);

        //Wedge 3
        Point kp31_wedge = Point.ofCartesian(24.8121e-3, 70.6179e-3);
        Point kp34_wedge = Point.ofCartesian(30.5731e-3, 84.8778e-3);
        double r_in3 = Math.sqrt(Math.pow(kp31_wedge.getX(), 2) + Math.pow(kp31_wedge.getY(), 2));
        double r_out3 = Math.sqrt(Math.pow(kp34_wedge.getX(), 2) + Math.pow(kp34_wedge.getY(), 2));

        Point kp32_wedge_aux = Point.ofCartesian(37.1343e-3, 65.0078e-3);
        Point kp33_wedge_aux = Point.ofCartesian(44.9367e-3, 78.2613e-3);

        double phi_kp32 = kp32_wedge_aux.getPhi();
        double phi_kp33 = kp33_wedge_aux.getPhi();

        Point kp32_wedge = Point.ofCartesian(r_in3 * Math.cos(phi_kp32), r_in3 * Math.sin(phi_kp32));
        Point kp33_wedge = Point.ofCartesian(r_out3 * Math.cos(phi_kp33), r_out3 * Math.sin(phi_kp33));

        Arc ln31_wedge = Arc.ofEndPointsCenter(kp32_wedge, kp31_wedge, kp0);
        Line ln32_wedge = Line.ofEndPoints(kp32_wedge, kp33_wedge);
        Arc ln33_wedge = Arc.ofEndPointsCenter(kp33_wedge, kp34_wedge, kp0);
        Line ln34_wedge = Line.ofEndPoints(kp31_wedge, kp34_wedge);

        Area ar3_q1_wedge = Area.ofHyperLines(new HyperLine[]{ln31_wedge, ln32_wedge, ln33_wedge, ln34_wedge});
        Element el3_q1_wedge = new Element("El3_q1_wedge", ar3_q1_wedge);

        Area ar1_q2_wedge = ar1_q1_wedge.mirrorY();
        Area ar2_q2_wedge = ar2_q1_wedge.mirrorY();
        Area ar3_q2_wedge = ar3_q1_wedge.mirrorY();
        Area ar1_q3_wedge = ar1_q2_wedge.mirrorX();
        Area ar2_q3_wedge = ar2_q2_wedge.mirrorX();
        Area ar3_q3_wedge = ar3_q2_wedge.mirrorX();
        Area ar1_q4_wedge = ar1_q1_wedge.mirrorX();
        Area ar2_q4_wedge = ar2_q1_wedge.mirrorX();
        Area ar3_q4_wedge = ar3_q1_wedge.mirrorX();

        Element el1_q2_wedge = new Element("El1_q2_wedge", ar1_q2_wedge);
        Element el2_q2_wedge = new Element("El2_q2_wedge", ar2_q2_wedge);
        Element el3_q2_wedge = new Element("El3_q2_wedge", ar3_q2_wedge);
        Element el1_q3_wedge = new Element("El1_q3_wedge", ar1_q3_wedge);
        Element el2_q3_wedge = new Element("El2_q3_wedge", ar2_q3_wedge);
        Element el3_q3_wedge = new Element("El3_q3_wedge", ar3_q3_wedge);
        Element el1_q4_wedge = new Element("El1_q4_wedge", ar1_q4_wedge);
        Element el2_q4_wedge = new Element("El2_q4_wedge", ar2_q4_wedge);
        Element el3_q4_wedge = new Element("El3_q4_wedge", ar3_q4_wedge);

        return new Element[]{el1_q1_wedge, el2_q1_wedge, el3_q1_wedge,
                el1_q2_wedge, el2_q2_wedge, el3_q2_wedge,
                el1_q3_wedge, el2_q3_wedge, el3_q3_wedge,
                el1_q4_wedge, el2_q4_wedge, el3_q4_wedge};
    }

}
