package input.LHC.MB;

import model.geometry.coil.Cable;
import model.materials.database.MatDatabase;

/**
 * Created by STEAM on 25/05/2016.
 */
public class CableIL_MB extends Cable {

    public CableIL_MB() {
        this.setCableParameters();
    }

    public void setCableParameters() {

        this.label = "cable_IL_MB";

        //Insulation
        this.wInsulNarrow = 1.55e-4; // [m];
        this.wInsulWide = 1.35e-4; // [m];
        //Filament
        this.dFilament = 7.0e-6; // [m]
        //Strand
        this.dstrand = 1.065e-3; // [m];
        this.fracCu = 1.65 /(1+1.65);
        this.fracSc = 1/(1+1.65);
        this.RRR = 150;
        this.TupRRR = 295; // [K];
        this.Top = 1.9; // [K];
        //Transient
        this.Rc = 100e-6; // [ohm];
        this.Ra = 100e-6; // ; // [ohm];
        this.fRhoEff = 1; // [1];
        this.lTp = 18e-3; // [m];
        //Cable
        this.wBare = 1.541e-2; // [m];
        this.hInBare = 1.736e-3; // [m];
        this.hOutBare = 2.064e-3; // [m];
        this.noOfStrands = 28;
        this.noOfStrandsPerLayer = 14;
        this.noOfLayers = 2;
        this.lTpStrand = 0.115; // [m];
        this.wCore = 0; // [m];
        this.hCore = 0; // [m];
        this.thetaTpStrand = Math.atan2((wBare-dstrand),(lTpStrand/2));
        this.C1 = 92073.5; // [A];
        this.C2 = -6869.4; // [A/T];
        this.fracHe = 0.0; //[%]
        this.fracFillInnerVoids = 1; //[-]
        this.fractFillOuterVoids = 1; //[-]

        this.criticalSurfaceFit = CriticalSurfaceFitEnum.Ic_NbTi_GSI;
        this.insulationMaterial = MatDatabase.MAT_KAPTON;
        this.materialInnerVoids = MatDatabase.MAT_VOID;
        this.materialOuterVoids = MatDatabase.MAT_VOID;
        this.materialCore = MatDatabase.MAT_VOID;
        this.resitivityCopperFit = ResitivityCopperFitEnum.rho_Cu_CUDI;
    }


}
