package input.Others.MQ;

import model.domains.Domain;
import model.domains.database.*;
import model.geometry.Element;
import model.geometry.basic.*;
import model.geometry.coil.*;
import model.materials.database.MatDatabase;
import input.UtilsUserInput;

/**
 * Created by STEAM on 25/01/2018.
 */

public class Input_MQ extends UtilsUserInput {

//        public int[] quadrantsRequired = {1};
//        public BoundaryConditions xAxisBC = BoundaryConditions.Neumann;
//        public BoundaryConditions yAxisBC = BoundaryConditions.Neumann;

    private final Domain[] domains;

    // VARIABLES

    // include-file of the variables for modified MB dipole
    // Christine Voellinger,Martin Aleksa, Bernhard Auchmann - 31.08.2000
    // Hole added by Martin Aleksa                           - 15.11.2000
    // Correction of the Nose and Nick by Nikolai Schwerg    - 25.10.2004
    // Checked by Bernhard Auchmann and Per Hagen            - 31.03.2011


    // referring to:
    // yoke			    LHCMB__A0136|0|C
    // inner yoke		LHCMB__A0139|0|B
    // outer yoke		LHCMB__A0140|0|B
    // insert		    LHCMB__A0148|1|E
    // collar		    LHCMB__A0099|0|C
    // coil sheet		LHCMB__A0021|0|B (design study by Christine Voellinger, not production)

    // GENERAL VARIABLES

    // Design variables added by Per HAGEN (for easier to deal with warm measurements)
//    double mm = 0.001;
//    double deg = Math.PI / 180;
    //shrink  = 0.998;			// yoke/insert shrink
//    double DCONT = 0.0;
//    double shrink = 1.0 * (1 - DCONT);
    //cshrink = 0.9973;			// collar shrink
//    double DCCONT = 0.0;
//    double cshrink = 1.0 * (1 - DCCONT);
    //csshrink = 0.996;			// coil sheet shrink
//    double DCSCONT = 0.0;
//    double csshrink = 1.0 * (1 - DCSCONT);



//    double BEAMD = 97.00;

//   Changed to 56 mm >
// see:      http://jwenning.home.cern.ch/jwenning/documents/LHC/Powering/LHC-design-Vol_1_Chapter_8-magnets.pdf
// page:     4


//    double BEAMD = 56.00;



//    double beamd = BEAMD * mm;

    // YOKE SPECIFIC VARIABLES

    // outer shape

//    double RYOKE = 226.00;
//    double ryoke = RYOKE * mm * shrink;





//    double ALEL1 = 83.1;
//    double alel1 = ALEL1 * mm * shrink;
//    double BLEL1 = 100.00;
//    double blel1 = BLEL1 * mm * shrink;
//    double ALEL2 = 86.85;
//   double alel2 = ALEL2 * mm * shrink;
//    double D1 = 17.8;
//    double d1 = D1 * mm * shrink;
//    double D2 = 14.5;
//    double d2 = D2 * mm * shrink;

    // houd

//    double HOUDW = 27.00;
//    double houdw = HOUDW * mm * shrink;
//    double HOUDH = 6.00;
//    double houdh = HOUDH * mm * shrink;
//    double INSWI = 65.00;
//    double inswi = INSWI * mm * shrink;

//    double houdsh = beamd - houdw - inswi;


    // nose
    // defining the given values from drawing:

    // ___o<//////HX1//////>       o : keypoint
    //    /                :
    //   /_____o<////HX2//->
    //         /           : ^
    //        /            : |
    //       /             : HY
    //      /______________o v
    //                       ^
    // 		      | RNOSE
    //
    // the variable PHINOSEU has been dumped!


//    double RNOSE = 256;
//    double rnose = RNOSE * mm * shrink;
//    double PHINOS = 77;
//    double phinos = PHINOS * deg;
//    double HX1 = 25.00;
//    double hx1 = HX1 * mm * shrink;
//    double HX2 = 19.04;
//    double hx2 = HX2 * mm * shrink;
//    double HY = 270.89 - RNOSE;
//    double hy = HY * mm * shrink;


    // nick
    // buildDefinitions from drawing:
    // Given are the Coordinates of the boar in the middle
    // of the nick, the radius of the boar and the angle.

    //           /
    //         /
    //    /\   |
    //  /   \  |
    // /      0<//- XNICK
    //        ^
    //        | YNICK

//    double XNICK = 202.95;
//    double xnick = XNICK * mm * shrink;
//    double YNICK = 342.00 / 2;
//    double ynick = YNICK * mm * shrink;
//    double PHINICK = 60;
//    double phinick = PHINICK * deg;
//    double RNICK = 3;
//    double rnick = RNICK * mm * shrink;

    // the point in the corner of the nick is given by:
    //   [xnick-rnick, ynick - c]
    // with c the hypothenuse of the triangle R,c, phinick/2:
    //   c = rnick * Math.tan(beta)
    // beta the opposite angle
    //   beta = 90 - phinick/2
//    Point kpTEST = Point.ofCartesian(xnick - rnick, ynick - rnick * Math.tan((Math.PI - phinick) / 2));
    // The x-coorinate of this point gives with the circle
    // equation the coordinates of kpnick_2
    //   kpnick_2 = [xnick-rnick, Math.sqrt(ryoke**2 - (xnick-rnick)**2)];

    // The coordinates of kpnick_1 are given by a line and the circle
    // by solving the equations:
    //   y = m * x + n
    //   y = Math.sqrt(R**2 - x**2)
    // With:
    //   R = ryoke
    //   m = 1/Math.tan(phinick)
//    double nick_m = 1 / Math.tan(phinick);
    //   n = y0 - m * x0
//    double nick_n = ynick - rnick * Math.tan((Math.PI - phinick) / 2) - nick_m * (xnick - rnick);
    //   with:
    //     x0 = xnick-rnick
    //     y0 = ynick - c
    // Equation:
    //   x**2 + 2*m*n/(1+m**2) * x + (n**2 - R**2)/(1+m**2) == 0
    // Solution:
    //   x = - p/2 +- Math.sqrt((p/2)**2-q)
    //   with:
    //     p = 2*m*n/(1+m**2)
//    double nick_p = 2 * nick_n * nick_m / (1 + nick_m * nick_m);
    //     q = (n**2 - R**2)/(1+m**2)
    //   double nick_q = (nick_n * nick_n - ryoke * ryoke) / (1 + nick_m * nick_m);
    //   The Solution with positive sign is used:
//    double nick_x = -nick_p / 2 + Math.sqrt((nick_p / 2) * (nick_p / 2) - nick_q);

    // busbar


    double mm=0.001;
    double Pi=3.14159265;
    double deg=Pi/180;
    double DCONT = 0.0;
    double shrink = 1.0*(1.-DCONT);
    double cshrink = 1.0*(1.-DCONT);
    double BEAMD = 97.00;
    double beamd=BEAMD*mm;

    double BBR = 244.00;
    double bbr = BBR * mm;
    double BBW = 51.00;
    double bbw = BBW * mm;
    double PHIM = 53.6;
    double phim = PHIM * deg;

//    double bush = Math.sqrt(ryoke * ryoke - bbw * bbw / 4) - bbr;
//    double gammah = Math.atan(bbw / (2 * ryoke));

    // holesYoke
    // middlepoints of holesYoke
    double LH1X = 17.00;
    double lh1x = LH1X * mm;
    double LH1Y = 90.00;
    double lh1y = LH1Y * mm;
    double LH2X = 57.00;
    double lh2x = LH2X * mm;
    double LH2Y = 220.00;
    double lh2y = LH2Y * mm;
    double LH3X = 90.32;
    double lh3x = LH3X * mm;
    double LH3Y = 176.00;
    double lh3y = LH3Y * mm;
    double LH4X = 180.2;
    double lh4x = LH4X * mm;
    double LH4Y = 120.2;
    double lh4y = LH4Y * mm;
    double LHHX = 0.00;
    double lhhx = LHHX * mm;
    double LHHY = 179.6;
    double lhhy = LHHY * mm;

    // radius of holesYoke
    double LH1R = 2.5;
    double lh1r = LH1R * mm * shrink;
    double LH2R = 15.00;
    double lh2r = LH2R * mm * shrink;
    double LH3R = 8.8;
    double lh3r = LH3R * mm * shrink;
    double LH4R = 8.8;
    double lh4r = LH4R * mm * shrink;
    double LHHR = 30.00;
    double lhhr = LHHR * mm * shrink;

    // other parameters of holesYoke
    double PHIHCO = 30.00;
    double phihco = PHIHCO * deg;
//    double linsh1 = Math.sqrt(1 - ((houdw + houdsh - d1) / alel1) * ((houdw + houdsh - d1) / alel1)) * blel1;    // point on ellipse

    // MODIFIED INSERT SPECIFIC VARIABLES

    double LH5X = 32.1;
    double lh5x = LH5X * mm * shrink;
    // double LH5DY = 22.7;
    double LH5R = 8.1;
    double lh5r = LH5R * mm * shrink;

    double LINSPHI = 9.00;
    double linsphi = LINSPHI * deg;
    double LINSPARANG = 41.8;
    double linsparang = LINSPARANG * deg;
    double LINSNH = 8.2;
    double linsnh = LINSNH * mm * shrink;
    double LINSCUT = 56.;
    double linscut = LINSCUT * mm * shrink;

//    double linsh = blel1 * Math.sin(linsparang) - linsnh;
//    double linsw = beamd - d2 - alel2 * Math.cos(linsparang);

//    double yinscut1 = Math.sqrt(1 - ((beamd - d2 - linscut) / alel2) * ((beamd - d2 - linscut) / alel2)) * blel1;    // point on ellipse
//    double yinscut2 = (beamd - houdw - linscut) * Math.tan(linsphi) + blel1 + houdh;

//    double linsh2 = Math.sqrt(1 - ((houdw + houdsh - d2) / alel2) * ((houdw + houdsh - d2) / alel2)) * blel1;

    // point on ellipse

//    double du1a = (d2 - beamd + lh5x);
//    double du1b = alel2 * alel2 * alel2 * alel2 * (blel1 * blel1 - 2 * lh5r * lh5r);
//    double du1 = -blel1 * blel1 * blel1 * blel1 * du1a * du1a - du1b;
//    double du2 = alel2 * alel2 * blel1 * blel1 * (du1a * du1a - lh5r * lh5r);
//    double du3a = (alel2 * alel2 * alel2 * alel2 - alel2 * alel2 * du1a * du1a + blel1 * blel1 * du1a * du1a);
//    double du3 = du3a * du3a;
//    double du4 = (alel2 * alel2 * alel2 * alel2 + alel2 * alel2 * du1a * du1a - blel1 * blel1 * du1a * du1a);
//    double du5 = (alel2 * alel2 * (alel2 - blel1) * (alel2 + blel1));

    // du1 = -(blel1**4*(-beamd+d2+lh5x)**2)-alel2**4*(blel1**2-2*lh5r**2);
    // du2 = alel2**2*blel1**2*((-beamd+d2+lh5x)**2-lh5r**2);
    // du3 = (alel2**4-alel2**2*(-beamd+d2+lh5x)**2+blel1**2*(-beamd+d2+lh5x)**2)**2;
    // du4 = (alel2**4+alel2**2*(-beamd+d2+lh5x)**2-blel1**2*(-beamd+d2+lh5x)**2);
    // du5 = (alel2**2*(alel2-blel1)*(alel2+blel1));

//    double dxvar = Math.sqrt((du1 + du2 + Math.sqrt(blel1 * blel1 * blel1 * blel1 * (du3 - 2 * alel2 * alel2 * du4 * lh5r * lh5r + alel2 * alel2 * alel2 * alel2 * lh5r * lh5r * lh5r * lh5r))) / du5) / Math.sqrt(2);

//    double lh5y = (blel1 * blel1 * dxvar * (beamd - d2 - lh5x)) / (alel2 * alel2 * Math.sqrt(-dxvar * dxvar + lh5r * lh5r));

//    double dyvar = Math.sqrt(lh5r * lh5r - dxvar * dxvar);


    // COLLAR SPECIFIC VARIABLES

    // outer shape

    double CUSH1 = 92.00;
    double cush1 = CUSH1 * mm * cshrink;
    double CUSH2 = 47.5;
    double cush2 = CUSH2 * mm * cshrink;
    double CN1X = 179.95;
    double cn1x = CN1X * mm * cshrink;

    // upper structure (us)

    double USHOUT = 92.00;
    double ushout = USHOUT * mm * cshrink;
    double USHIN = 93.1;
    double ushin = USHIN * mm * cshrink;
    double USHTOP = 98.9;
    double ushtop = USHTOP * mm * cshrink;
    double USPHI = 30.00;
    double usphi = USPHI * deg;
    double USDOUT = 52.00;
    double usdout = USDOUT * mm * cshrink;
    double USDIN = 21.00;
    double usdin = USDIN * mm * cshrink;


    // lower structure (ls)

    double LSR1 = 15.00;
    double lsr1 = LSR1 * mm * cshrink;
    double LSR2 = 44.88;
    double lsr2 = LSR2 * mm * cshrink;
    double LSR3 = 60.98;
    double lsr3 = LSR3 * mm * cshrink;
    double LSPHI1 = 52.5;
    double lsphi1 = LSPHI1 * deg;
    double LSPHI2 = 19.01;
    double lsphi2 = LSPHI2 * deg;
    double LSPHI3 = 48.67;
    double lsphi3 = LSPHI3 * deg;
    double LSHHELP1 = 12.44;
    double lshhelp1 = LSHHELP1 * mm * cshrink;
    double LSHHELP2 = 13.2;
    double lshhelp2 = LSHHELP2 * mm * cshrink;
    double LSHHELP3 = 18.12;
    double lshhelp3 = LSHHELP3 * mm * cshrink;
    double LSHMID = 30.3;
    double lshmid = LSHMID * mm * cshrink;

    double delta = lshhelp2 - lshhelp1;
    double lsdI = delta * Math.cos(lsphi2) + Math.sqrt(delta * Math.cos(lsphi2) * delta * Math.cos(lsphi2) + lsr1 * lsr1 - delta * delta);    // Cosine-theorem

    double lsdIIa = lshhelp1 * Math.cos(Math.PI - lsphi2) + Math.sqrt(lshhelp1 * Math.cos(Math.PI - lsphi2) * lshhelp1 * Math.cos(Math.PI - lsphi2) + lsr2 * lsr2 - lshhelp1 * lshhelp1);    // Cosine-theorem

    double lsdIIb = lshhelp3 * Math.cos(Math.PI - lsphi3) + Math.sqrt(lshhelp3 * Math.cos(Math.PI - lsphi3) * lshhelp3 * Math.cos(Math.PI - lsphi3) + lsr2 * lsr2 - lshhelp3 * lshhelp3);

    double lsdIII = lshhelp3 * Math.cos(Math.PI - lsphi3) + Math.sqrt(lshhelp3 * Math.cos(Math.PI - lsphi3) * lshhelp3 * Math.cos(Math.PI - lsphi3) + lsr3 * lsr3 - lshhelp3 * lshhelp3);

    // arcs and holesYoke

    double XARC1 = 178.00;
    double xarc1 = XARC1 * mm * cshrink;
    double RARC1 = 7.01;
    double rarc1 = RARC1 * mm; // shrinked scale
    double RARC2 = 11.01;
    double rarc2 = RARC2 * mm; // shrinked scale

    double XHOLE1 = 156.00;
    double xhole1 = XHOLE1 * mm * cshrink;
    double XHOLE2 = 39.00;
    double xhole2 = XHOLE2 * mm * cshrink;
    double YHOLE = 57.00;
    double yhole = YHOLE * mm * cshrink;

    double DHOLE1 = 10.00;
    double dhole1 = DHOLE1 * mm * cshrink;
    double DHOLE2 = 10.02;
    double dhole2 = DHOLE2 * mm * cshrink;
    double rhole1 = dhole1 / 2;
    double rhole2 = dhole2 / 2;


//------------------------------------------------From Roxie - Dimitri ---------------------------------------------------------------------------


// ------------------------------------------Variables -----------------------------------------------------------------


// GENERAL VARIABLES

// YOKE SPECIFIC VARIABLES

// BEAMTUNNEL


    double RBT = 90.00;
    double rbt = RBT*mm*shrink;
    double PHIBT = 45.00;
    double phibt = PHIBT*deg;
    double HBARBT = 7.00;
    double hbarbt = HBARBT*mm*shrink;
    double DBARBT = 4.00;
    double dbarbt = DBARBT*mm*shrink;
// Per HAGEN 13.08.2010: DBARBT changed from 3.00 to 4.00


//             pin holes 8 mm according to drawing LHCMQ_M_004|0|B


// OUTER FORM VARIABLES

    double RYOK = 226.00;
    double ryok = RYOK*mm*shrink;

// busbar and corners for fermeture

    double RCORN = 225.00;
    double rcorn = RCORN*mm*shrink;
    double DCORN_1 = 9.5;
    double dcorn_1 = DCORN_1*mm*shrink;
    double DCORN_2 = 19.5;
    double dcorn_2 = DCORN_2*mm*shrink;
    double PHIBAR = 30.00;
    double phibar = PHIBAR*deg;

    double DBAR = 51.00;
    double dbar = DBAR*mm*shrink;
    double HBAR = 25.00;
    double hbar = HBAR*mm*shrink;

    double psibar = Math.asin(dbar/(2*rcorn));
    double psicorn_1 = Math.asin((dbar/2+dcorn_1)/rcorn);
    double psicorn_2 = Math.asin((dbar/2+dcorn_2)/rcorn);

// screw bar

    double PHISCR = 45.00;
    double phiscr = PHISCR*deg;
    double DSCR = 16.00;
    double dscr = DSCR*mm*shrink;
    double HSCR = 17.00;
    double hscr = HSCR*mm*shrink;
    double psiscr = Math.asin((dscr/2)/ryok);

// area of filling factor 1/2 / mesh-aid keypoints

    double YAR05 = 220.00;

    double yar05 = YAR05*mm*shrink;




    double xar05 = Math.sqrt(rcorn*rcorn-yar05*yar05);

    double h = 1.0*mm*shrink;
    double d1 = 5.00*mm*shrink;

    double xar05h_1 = xar05;


    double yar05h_1 = yar05-h;
    double xar05h_2 = xar05-d1;
    double yar05h_2 = yar05;

    //double xar05h_2 = xar05;



// holes

    double YH1 = 180.00;
    double yh1 = YH1*mm*shrink;
    double RH1 = 30.00;
    double rh1 = RH1*mm*shrink;
    double XH2 = 90.00;
    double xh2 = XH2*mm*shrink;
    double YH2 = 150.00;
    double yh2 = YH2*mm*shrink;
    double RH2 = 10.25;
    double rh2 = RH2*mm*shrink;
    double XH3 = 125.00;
    double xh3 = XH3*mm*shrink;
    double YH3 = 125.00;
    double yh3 = YH3*mm*shrink;
    double RH3 = 6.1;
    double rh3 = RH3*mm*shrink;

// COLLAR SPECIFIC VARIABLES

// UPPER STRUCTURE (us)

    double USR = 87.00;
    double usr = USR*mm*cshrink;
    double USBH1 = 8.5;
    double usbh1 = USBH1*mm*cshrink;
    double USBH2 = 7.5;
    double usbh2 = USBH2*mm*cshrink;
    double USBW = 2.5;
    double usbw = USBW*mm*cshrink;
    double USPHI1 = 25.00;
    double usphi1 = USPHI1*deg;
    double USPHI2 = 65.00;
    double usphi2 = USPHI2*deg;
    double USB1D = 4.65;
    double usb1d = USB1D*mm*cshrink;
    double USFLATY = 86.05;
    double usflaty = USFLATY*mm*cshrink;
    double USB2D = 3.00;
    double usb2d = USB2D*mm*cshrink;

    double usbs = Math.sqrt(usr*usr-usbw*usbw);

    double uspsi1 = Math.asin(usb1d/usr);

    double usflatx = Math.sqrt(usr*usr-usflaty*usflaty);


// LOWER STRUCTURE (ls)

    double LSPOINT = 15.595;
    double lspoint = LSPOINT*mm*cshrink;
    double LSH = 28.00;
    double lsh = LSH*mm*cshrink;
    double LSPHI = 13.891;
    double lsphi = LSPHI*deg;
    double LSR = 59.995;
    double lsr = LSR*mm*cshrink;

// intersection of circle(x^2+y^2 = r^2) and line(y=kx+d)

    double d = lspoint;
    double k = 1/Math.tan(lsphi);
    double a = (1+k*k);
    double b = 2*k*d;
    double c = d*d-lsr*lsr;
    double lsux = ((-1)*b+Math.sqrt(b*b-4*a*c))/(2*a);
    double lsuy = Math.sqrt(lsr*lsr-lsux*lsux);

    double lsdx = (lsh-lspoint)*Math.tan(lsphi);

    double lsux45 = lsux*(Math.sqrt(2)/2);
    double lsuy45 = lsuy*(Math.sqrt(2)/2);

    double ls2x = lsuy45+lsux45;
    double ls2y = lsuy45-lsux45;
    double ls5x = lsuy45-lsux45;
    double ls5y = lsuy45+lsux45;

    double lsh45 = lsh*(Math.sqrt(2)/2);
    double lsdx45 = lsdx*(Math.sqrt(2)/2);

    double ls3x = lsh45+lsdx45;
    double ls3y = lsh45-lsdx45;
    double ls4x = lsh45-lsdx45;
    double ls4y = lsh45+lsdx45;


//--------------------------------------------------------------End: List of Variables from Roxie file: MQ_2D_Aperture_Variables.mod---------------------------------------------------------------



    public Input_MQ() {

        domains = new Domain[]{

                new AirDomain("airDomain", MatDatabase.MAT_AIR, air()),
                new AirFarFieldDomain("airFarFieldDomain", MatDatabase.MAT_AIR, airFarField()),
//                    new BoundaryConditionsDomain("BC", null, this.xAxisBC, this.yAxisBC, BoundaryConditions()),

//                new CoilDomain("CR", MatDatabase.MAT_COIL, coil().translate(97e-3, 0)),
                new CoilDomain("CR", MatDatabase.MAT_COIL, coil().translate(97e-3, 0)),
//                new CoilDomain("CL", MatDatabase.MAT_COIL, coil().translate(-97e-3, 0)),

                new WedgeDomain("wedgesCR", MatDatabase.MAT_COPPER, wedges()),
//                   new WedgeDomain("wedgesCL", MatDatabase.MAT_COPPER, wedgesCoil("CL", -97e-3, 0)),

                new InsulationDomain("InsulationCopperWedgesCR", MatDatabase.MAT_KAPTON, insulationCW()),


//                new InsulationDomain("foilCoil1", "poly1", fishBone(coil_ApR, coil_ApR_Name, 97e-3, 0)),
//                new InsulationDomain("foilCoil2", "poly2", fishBone(coil_ApL, coil_ApL_Name, -97e-3, 0)),




                //----------------------------------------------------------------------------


                new IronDomain("C", MatDatabase.MAT_IRON1, collar()),

                //----------------------------------------------------------------------------




                new IronDomain("IY", MatDatabase.MAT_IRON2, iron_yoke()),

//                new IronDomain("I", MatDatabase.MAT_IRON2, iron_insert()),
                new HoleDomain("HC", MatDatabase.MAT_AIR, holes_collar()),
                new HoleDomain("HY", MatDatabase.MAT_AIR, holes_yoke()),

//                new HoleDomain("HI", MatDatabase.MAT_AIR, holes_insert()),
        };
    }


    public Domain[] getDomains() {
        return domains.clone();
    }

    public Element[] air() {
        // POINTS

        double r = 1.0;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        // LINES
        Line ln1 = Line.ofEndPoints(kpc, kp1);
        Arc ln2 = Arc.ofEndPointsCenter(kp1, kp2, kpc);
        Line ln3 = Line.ofEndPoints(kp2, kpc);

        // AREAS
        Area ar1 = Area.ofHyperLines(new HyperLine[]{ln1, ln2, ln3});
        //   Area ar2 = ar1.mirrorY();
        //   Area ar3 = ar2.mirrorX();
        //   Area ar4 = ar1.mirrorX();

        // ELEMENTS
        Element el1 = new Element("AIR_El1", ar1);
        //   Element el2 = new Element("AIR_El2", ar2);
        //   Element el3 = new Element("AIR_El3", ar3);
        //   Element el4 = new Element("AIR_El4", ar4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1};
        //   Element[] quad2 = {el2};
        //   Element[] quad3 = {el3};
        //   Element[] quad4 = {el4};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
//        return new Element[]{el1, el4};

        return new Element[]{el1};
    }

    public Element[] airFarField() {
        // POINTS

        double r = 1;

        Point kpc = Point.ofCartesian(0, 0);
        Point kp1 = Point.ofCartesian(r, 0);
        Point kp2 = Point.ofCartesian(0, r);

        Point kp1_far = Point.ofCartesian(r * 1.05, 0);
        Point kp2_far = Point.ofCartesian(0, r * 1.05);

        // LINES
        Line ln1_far = Line.ofEndPoints(kp1, kp1_far);
        Arc ln2_far = Arc.ofEndPointsCenter(kp1_far, kp2_far, kpc);
        Line ln3_far = Line.ofEndPoints(kp2_far, kp2);
        Arc ln4_far = Arc.ofEndPointsCenter(kp2, kp1, kpc);

        Area ar1_far = Area.ofHyperLines(new HyperLine[]{ln1_far, ln2_far, ln3_far, ln4_far});
//        Area ar2_far = ar1_far.mirrorY();
//        Area ar3_far = ar2_far.mirrorX();
//        Area ar4_far = ar1_far.mirrorX();

        Element el1_far = new Element("FAR_El1", ar1_far);
//        Element el2_far = new Element("FAR_El2", ar2_far);
//        Element el3_far = new Element("FAR_El3", ar3_far);
//        Element el4_far = new Element("FAR_El4", ar4_far);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {el1_far};
//        Element[] quad2 = {el2_far};
//        Element[] quad3 = {el3_far};
//       Element[] quad4 = {el4_far};
//            Element[] elementsToBuild = quadrantsToBuild(quad1,quad2,quad3,quad4);
//        return new Element[]{el1_far,el4_far};
        return new Element[]{el1_far};
    }



    public Coil coil() {


        Point kp0 = Point.ofCartesian(0, 0);


//      first qauter of the right "tube"

        Point kp11 = Point.ofCartesian(139.5222e-3-beamd, 11.0877e-3);
        Point kp12 = Point.ofCartesian(140.7287e-3-beamd, 0.1378e-3);
        Point kp13 = Point.ofCartesian(156.0430e-3-beamd, 0.1378e-3);
        Point kp14 = Point.ofCartesian(154.7481e-3-beamd, 12.7316e-3);





        Point kp21 = Point.ofCartesian(134.8117e-3-beamd, 22.3833e-3);
        Point kp22 = Point.ofCartesian(138.9950e-3-beamd, 12.1936e-3);
        Point kp23 = Point.ofCartesian(153.7051e-3-beamd, 16.4534e-3);
        Point kp24 = Point.ofCartesian(148.9795e-3-beamd, 28.1974e-3);


        Point kp31 = Point.ofCartesian(122.4836e-3-beamd, 12.5449e-3);
        Point kp32 = Point.ofCartesian(124.9158e-3-beamd, 0.1373e-3);
        Point kp33 = Point.ofCartesian(140.2301e-3-beamd, 0.1373e-3);
        Point kp34 = Point.ofCartesian(137.6824e-3-beamd, 14.4225e-3);




        Point kp41 = Point.ofCartesian(119.7246e-3-beamd, 16.3806e-3);
        Point kp42 = Point.ofCartesian(121.3343e-3-beamd, 13.6815e-3);
        Point kp43 = Point.ofCartesian(134.6869e-3-beamd, 21.1808e-3);
        Point kp44 = Point.ofCartesian(132.8404e-3-beamd, 24.2865e-3);



        //      second qauter of the right "tube"



//        Point kp0 = Point.ofCartesian(-97e-3, 0);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);
//
        Arc ln21 = Arc.ofEndPointsCenter(kp22, kp21, kp0);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp21, kp24);



        Arc ln31 = Arc.ofEndPointsCenter(kp32, kp31, kp0);
        Line ln32 = Line.ofEndPoints(kp32, kp33);
        Arc ln33 = Arc.ofEndPointsCenter(kp33, kp34, kp0);
        Line ln34 = Line.ofEndPoints(kp31, kp34);


        Arc ln41 = Arc.ofEndPointsCenter(kp42, kp41, kp0);
        Line ln42 = Line.ofEndPoints(kp42, kp43);
        Arc ln43 = Arc.ofEndPointsCenter(kp43, kp44, kp0);
        Line ln44 = Line.ofEndPoints(kp41, kp44);
//
        Area ha11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area ha12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area ha13p = Area.ofHyperLines(new HyperLine[]{ln31, ln32, ln33, ln34});
        Area ha14p = Area.ofHyperLines(new HyperLine[]{ln41, ln42, ln43, ln44});


        Area ha11n = ha11p.mirrorY().rotate(-0.5*(Math.PI));
        Area ha12n = ha12p.mirrorY().rotate(-0.5*(Math.PI));
        Area ha13n = ha13p.mirrorY().rotate(-0.5*(Math.PI));
        Area ha14n = ha14p.mirrorY().rotate(-0.5*(Math.PI));




        Winding w11_R = Winding.ofAreas(new Area[]{ha11p, ha11n}, new int[]{+1,-1}, 7, 7, new Cable_MQ());
        Winding w12_R = Winding.ofAreas(new Area[]{ha12p, ha12n}, new int[]{+1,-1}, 7, 7, new Cable_MQ());
        Winding w13_R = Winding.ofAreas(new Area[]{ha13p, ha13n}, new int[]{+1,-1}, 8, 8, new Cable_MQ());
        Winding w14_R = Winding.ofAreas(new Area[]{ha14p, ha14n}, new int[]{+1,-1}, 2, 2, new Cable_MQ());

        // poles:
        Pole p1 = Pole.ofWindings(new Winding[]{w11_R, w12_R, w13_R, w14_R}).translate(0,0);


        Pole p2 = p1.mirrorY();

        Pole p3 = p1.mirrorY().mirrorX();
        Pole p4 = p1.mirrorX();


        // Coil:
        Coil c1 = Coil.ofPoles(new Pole[]{p1, p2});

        return c1;

    }

    //the new one, self written:
    public Element[] wedges(){


        Point kp0 = Point.ofCartesian(97e-3, 0);


//      first qauter of the right "tube"

        Point kp11 = Point.ofCartesian(121.3343e-3, 13.6815e-3);
        Point kp12 = Point.ofCartesian(0.12204587980103573, 0.01232942196220366);
        Point kp13 = Point.ofCartesian(0.1377456129546674, 0.014444909907937837);
        Point kp14 = Point.ofCartesian(0.1346869, 0.0211808);

        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);



        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);

        Point kp21 = Point.ofCartesian(0.138995, 0.012193600000000002);
        Point kp22 = Point.ofCartesian(0.139314082133646, 0.011033433088439131);
        Point kp23 = Point.ofCartesian(0.1546585097983222, 0.012711848239999568);
        Point kp24 = Point.ofCartesian(0.1537051, 0.0164534);


        Line ln21 = Line.ofEndPoints(kp21, kp22);
        Line ln22 = Line.ofEndPoints(kp22, kp23);
        Arc ln23 = Arc.ofEndPointsCenter(kp23, kp24, kp0);
        Line ln24 = Line.ofEndPoints(kp24, kp21);

//
        Area wedge_copper_11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area wedge_copper_12p = Area.ofHyperLines(new HyperLine[]{ln21, ln22, ln23, ln24});
        Area wedge_copper_11n = wedge_copper_11p.mirrorY().translate(2*beamd,0);
        Area wedge_copper_12n = wedge_copper_12p.mirrorY().translate(2*beamd,0);
        Area wedge_copper_21n = wedge_copper_11p.mirrorY().rotate(-0.5*(Math.PI)).translate(beamd,-beamd);
        Area wedge_copper_22n = wedge_copper_12p.mirrorY().rotate(-0.5*(Math.PI)).translate(beamd,-beamd);
        Area wedge_copper_31n = wedge_copper_11p.rotate(0.5*(Math.PI)).translate(beamd,-beamd);
        Area wedge_copper_32n = wedge_copper_12p.rotate(0.5*(Math.PI)).translate(beamd,-beamd);

        Element el1_wedgeCOil1_Q1_1 = new Element("Q1p_El1_wedge", wedge_copper_11p);
        Element el2_wedgeCOil1_Q1_1 = new Element("Q1p_El2_wedge", wedge_copper_12p);
        Element el3_wedgeCOil1_Q1_1 = new Element("Q1p_El3_wedge", wedge_copper_11n);
        Element el4_wedgeCOil1_Q1_1 = new Element("Q1p_El4_wedge", wedge_copper_12n);
        Element el5_wedgeCOil1_Q1_1 = new Element("Q1p_El5_wedge", wedge_copper_21n);
        Element el6_wedgeCOil1_Q1_1 = new Element("Q1p_El6_wedge", wedge_copper_22n);
        Element el7_wedgeCOil1_Q1_1 = new Element("Q1p_El7_wedge", wedge_copper_31n);
        Element el8_wedgeCOil1_Q1_1 = new Element("Q1p_El8_wedge", wedge_copper_32n);


        return new Element[] {el1_wedgeCOil1_Q1_1,el2_wedgeCOil1_Q1_1,el3_wedgeCOil1_Q1_1,el4_wedgeCOil1_Q1_1,el5_wedgeCOil1_Q1_1,el6_wedgeCOil1_Q1_1,el7_wedgeCOil1_Q1_1,el8_wedgeCOil1_Q1_1};


    }




    public Element[] insulationCW(){


        Point kp0 = Point.ofCartesian(97e-3, 0);


//      first qauter of the right "tube"

        Point kp11 = Point.ofCartesian(0.134228295598739, 0.0219583211983988);
        Point kp12 = Point.ofCartesian(0.1402301, 1.373E-4);
        Point kp13 = Point.ofCartesian(0.1407287, 1.378E-4);
        Point kp14 = Point.ofCartesian(0.1346303781820918, 0.022275963364863673);


        Arc ln11 = Arc.ofEndPointsCenter(kp12, kp11, kp0);
        Line ln12 = Line.ofEndPoints(kp12, kp13);
        Arc ln13 = Arc.ofEndPointsCenter(kp13, kp14, kp0);
        Line ln14 = Line.ofEndPoints(kp11, kp14);


        Area wedge_insulation_11p = Area.ofHyperLines(new HyperLine[]{ln11, ln12, ln13, ln14});
        Area wedge_insulation_11n = wedge_insulation_11p.mirrorY().translate(2*beamd,0);
        Area wedge_insulation_14n = wedge_insulation_11n.rotate(-0.5*(Math.PI)).translate(beamd,beamd);
        Area wedge_insulation_13n = wedge_insulation_11p.rotate(0.5*(Math.PI)).translate(beamd,-beamd);

        Element el1_insulationCWCoil1_Q1_1 = new Element("Q1p_El1_InsulationCW", wedge_insulation_11p);
        Element el3_insulationCWCoil1_Q1_1 = new Element("Q1p_El3_InsulationCW", wedge_insulation_11n);
        Element el4_insulationCWCoil1_Q1_1 = new Element("Q1p_El4_InsulationCW", wedge_insulation_14n);
        Element el2_insulationCWCoil1_Q1_1 = new Element("Q1p_El2_InsulationCW", wedge_insulation_13n);

        return new Element[] {el1_insulationCWCoil1_Q1_1,el2_insulationCWCoil1_Q1_1,el3_insulationCWCoil1_Q1_1,el4_insulationCWCoil1_Q1_1};



    }


// ----------------Creating of Points, etc. using the Variables from ROXIE file> MQ_2D_Aperture_Collar.mod---------------

    public Element[] collar() {


// KEYPOINTS

        Point kp0 = Point.ofCartesian(97e-3, 0);

        Point kplsr1 = Point.ofCartesian(beamd+lsr,0); //1st point is at 156,995 mm > check!
        Point kplsr2 = Point.ofCartesian(beamd+ls2x,ls2y);
        Point kplsr3 = Point.ofCartesian(beamd+ls3x,ls3y);
        Point kplsr4 = Point.ofCartesian(beamd+ls4x,ls4y);
        Point kplsr5 = Point.ofCartesian(beamd+ls5x,ls5y);

        Point kpusr1 = Point.ofCartesian(beamd+usr-usbh1,0);
        Point kpusr2 = Point.ofCartesian(beamd+usbs,usbw);


        Point kpusr3 = Point.ofCartesian(beamd+usr*Math.cos(usphi1-uspsi1),usr*Math.sin(usphi1-uspsi1));


        Point kpusr4 = Point.ofCartesian(beamd+usr*Math.cos(usphi1+uspsi1),usr*Math.sin(usphi1+uspsi1));
        Point kpusr5 = Point.ofCartesian(beamd+(usflaty+usflatx)*(Math.sqrt(2)/2),(usflaty-usflatx)*(Math.sqrt(2)/2));
        Point kpusr6 = Point.ofCartesian(beamd+(usflaty+usb2d)*(Math.sqrt(2)/2),(usflaty-usb2d)*(Math.sqrt(2)/2));

        Point kpusr7 = Point.ofCartesian(beamd+(usflaty-usb2d)*(Math.sqrt(2)/2),(usflaty+usb2d)*(Math.sqrt(2)/2));
        Point kpusr8 = Point.ofCartesian(beamd+(usflaty-usflatx)*(Math.sqrt(2)/2),(usflaty+usflatx)*(Math.sqrt(2)/2));
        Point kpusr9 = Point.ofCartesian(beamd+usr*Math.cos(usphi2-uspsi1),usr*Math.sin(usphi2-uspsi1));
        Point kpusr10 = Point.ofCartesian(beamd+usr*Math.cos(usphi2+uspsi1),usr*Math.sin(usphi2+uspsi1));
        Point kpusr11 = Point.ofCartesian(beamd+usbw, usbs);

        Point kplsl1 = Point.ofCartesian(beamd-lsr,0);
        Point kplsl2 = Point.ofCartesian(beamd-ls2x,ls2y);
        Point kplsl3 = Point.ofCartesian(beamd-ls3x,ls3y);
        Point kplsl4 = Point.ofCartesian(beamd-ls4x,ls4y);
        Point kplsl5 = Point.ofCartesian(beamd-ls5x,ls5y);

        Point kpusl1 = Point.ofCartesian(beamd-usr+usbh1,0);
        Point kpusl2 = Point.ofCartesian(beamd-usbs,usbw);
        Point kpusl3 = Point.ofCartesian(beamd-usr*Math.cos(usphi1-uspsi1),usr*Math.sin(usphi1-uspsi1));
        Point kpusl4 = Point.ofCartesian(beamd-usr*Math.cos(usphi1+uspsi1),usr*Math.sin(usphi1+uspsi1));
        Point kpusl5 = Point.ofCartesian(beamd-(usflaty+usflatx)*(Math.sqrt(2)/2),(usflaty-usflatx)*(Math.sqrt(2)/2));
        Point kpusl6 = Point.ofCartesian(beamd-(usflaty+usb2d)*(Math.sqrt(2)/2),(usflaty-usb2d)*(Math.sqrt(2)/2));
        Point kpusl7 = Point.ofCartesian(beamd-(usflaty-usb2d)*(Math.sqrt(2)/2),(usflaty+usb2d)*(Math.sqrt(2)/2));
        Point kpusl8 = Point.ofCartesian(beamd-(usflaty-usflatx)*(Math.sqrt(2)/2),(usflaty+usflatx)*(Math.sqrt(2)/2));
        Point kpusl9 = Point.ofCartesian(beamd-usr*Math.cos(usphi2-uspsi1),usr*Math.sin(usphi2-uspsi1));
        Point kpusl10 = Point.ofCartesian(beamd-usr*Math.cos(usphi2+uspsi1),usr*Math.sin(usphi2+uspsi1));

        Point kpusl11 = Point.ofCartesian(beamd-usbw,usbs);

//       --------------helping point ------------Dimitri------------------------
        Point kpusl12 = Point.ofCartesian(97e-3,beamd-usbw);
        Point kpusl13 = Point.ofCartesian(97e-3,28.037e-3);



//    kpbt2 = [beamd+(rbt+dbarbt)*Cos(phibt),(rbt-dbarbt)*Sin(phibt)];
//    kpbt3 = [beamd+(rbt-dbarbt)*Cos(phibt),(rbt+dbarbt)*Sin(phibt)];
//    kpbt4 = [beamd+(rbt-dbarbt)*Cos(phibt+Pi/2),(rbt+dbarbt)*Sin(phibt+Pi/2)];
//    kpbt5 = [beamd+(rbt+dbarbt)*Cos(phibt+Pi/2),(rbt-dbarbt)*Sin(phibt+Pi/2)];




// LINES

        Line     lnusr1 = Line.ofEndPoints(kplsr1,kpusr1);


        Line     lnusr2 = Line.ofEndPoints(kpusr1,kpusr2);




        //Line     lnusr3 = Line.ofEndPoints(kpusr3,kpusr2);
        Arc lnusr3 = Arc.ofEndPointsCenter(kpusr3, kpusr2, kp0);

        //Line     lnusr4 = Line.ofEndPoints(kpusr3,kpusr4);
        Arc lnusr4 = Arc.ofEndPointsCenter(kpusr3, kpusr4, kp0);

        //Line     lnusr5 = Line.ofEndPoints(kpusr5,kpusr4);
        Arc lnusr5 = Arc.ofEndPointsCenter(kpusr5, kpusr4, kp0);


        Line     lnusr6 = Line.ofEndPoints(kpusr5,kpusr6);
        //Arc lnusr6 = Arc.ofEndPointsCenter(kpusr5, kpusr6, kp0);

        //Line     lnusr7 = Line.ofEndPoints(kpusr6,kpusr7);
        Arc lnusr7 = Arc.ofEndPointsCenter(kpusr6, kpusr7, kp0);

        Line     lnusr8 = Line.ofEndPoints(kpusr7,kpusr8);
        //Arc lnusr8 = Arc.ofEndPointsCenter(kpusr7, kpusr8, kp0);

        //Line     lnusr9 = Line.ofEndPoints(kpusr9,kpusr8);
        Arc lnusr9 = Arc.ofEndPointsCenter(kpusr9, kpusr8, kp0);

        //Line     lnusr10 = Line.ofEndPoints(kpusr9,kpusr10);
        Arc lnusr10 = Arc.ofEndPointsCenter(kpusr9, kpusr10, kp0);

        //Line     lnusr11 = Line.ofEndPoints(kpusr11,kpusr10);
        Arc lnusr11 = Arc.ofEndPointsCenter(kpusr11, kpusr10, kp0);

        //       Line     lnlsr1 = Line.ofEndPoints(kplsr2,kplsr1);




        Arc lnlsr1 = Arc.ofEndPointsCenter(kplsr2, kplsr1, kp0);


        Line     lnlsr2 = Line.ofEndPoints(kplsr3,kplsr2);
        Line     lnlsr3 = Line.ofEndPoints(kplsr4,kplsr3);
        Line     lnlsr4 = Line.ofEndPoints(kplsr5,kplsr4);

        Line     lnusl1 = Line.ofEndPoints(kplsl1,kpusl1);
        Line     lnusl2 = Line.ofEndPoints(kpusl1,kpusl2);
        Line     lnusl3 = Line.ofEndPoints(kpusl2,kpusl3);
        Line     lnusl4 = Line.ofEndPoints(kpusl4,kpusl3);
        Line     lnusl5 = Line.ofEndPoints(kpusl4,kpusl5);
        Line     lnusl6 = Line.ofEndPoints(kpusl5,kpusl6);
        Line     lnusl7 = Line.ofEndPoints(kpusl7,kpusl6);
        Line     lnusl8 = Line.ofEndPoints(kpusl7,kpusl8);
        Line     lnusl9 = Line.ofEndPoints(kpusl8,kpusl9);
        Line     lnusl10 = Line.ofEndPoints(kpusl10,kpusl9);
        Line     lnusl11 = Line.ofEndPoints(kpusl10,kpusl11);

        Line     lnlsl1 = Line.ofEndPoints(kplsl1,kplsl2);
        Line     lnlsl2 = Line.ofEndPoints(kplsl3,kplsl2);
        Line     lnlsl3 = Line.ofEndPoints(kplsl4,kplsl3);

//        Area arcollar_mid = Area.ofHyperLines(new HyperLine[]{lnardiv_r,lnusr9,lnusr10,lnusr11,lnusmid,lnusl11,lnusl10,lnusl9,lnardiv_l,lnlsmid});



        Line     lnlsl4 = Line.ofEndPoints(kplsl5,kplsl4);

        Line     lnusmid = Line.ofEndPoints(kpusr11,kpusl11);



//        Line     lnlsmid = Line.ofEndPoints(kplsl5,kplsr5);

        Arc lnlsmid = Arc.ofEndPointsCenter(kplsl5, kplsr5, kp0);
        ////////////////////////////////////
        Line lnardiv_r = Line.ofEndPoints(kplsr5,kpusr8);
//        Line lnardiv_l = Line.ofEndPoints(kplsl5,kpusl8);


//       --------------helping line ------------Dimitri------------------------

        Line     lnardiv_l = Line.ofEndPoints(kpusl12,kpusl13);


//lnel_y_c_r0 = HyperLine(kpusr1,kpusr2,"CornerIn");
//lnel_y_c_r1 = Line(kpusr3,kpusr4);
//lnel_y_c_r2a = Line(kpusr6,kpbt2);
//lnbt2 = HyperLine(kpbt3,kpbt2,"Bar",hbarbt);
//lnel_y_c_r2b = Line(kpusr7,kpbt3);
//lnel_y_c_r3 = Line(kpusr10,kpusr9);
//lnel_y_c_mid = Line(kpusl11,kpusr11);
//lnel_y_c_l3 = Line(kpusl10,kpusl9);
//lnel_y_c_l2a = Line(kpusl7,kpbt4);
//lnbt4 = HyperLine(kpbt5,kpbt4,"Bar",hbarbt);
//lnel_y_c_l2b = Line(kpusl6,kpbt5);
//lnel_y_c_l1 = Line(kpusl3,kpusl4);
//lnel_y_c_l0 = HyperLine(kpusl1,kpusl2,"CornerIn");



// AREAS


        Area arcollar_r = Area.ofHyperLines(new HyperLine[]{lnusr1,lnusr2,lnusr3,lnusr4,lnusr5,lnusr6,lnusr7,lnusr8,lnardiv_r,lnlsr4,lnlsr3,lnlsr2,lnlsr1});

        Area arcollar_mid_r = Area.ofHyperLines(new HyperLine[]{lnardiv_r,lnusr9,lnusr10,lnusr11,lnusmid,lnusl11,lnusl10,lnusl9,lnardiv_l,lnlsmid});

        Area arcollar_l = arcollar_r.mirrorY().translate(2*97e-3,0);
        Area arcollar_mid_l = arcollar_mid_r.mirrorY().translate(2*97e-3,0);

        Element el1_1 = new Element("C_El1", arcollar_r);
        Element el1_2 = new Element("C_El2", arcollar_mid_r);
        Element el1_3 = new Element("C_El3", arcollar_l);
        Element el1_4 = new Element("C_El4", arcollar_mid_l);

        Element[] quad1 = {el1_1};
        Element[] quad2 = {el1_2};
        Element[] quad3 = {el1_3};
        Element[] quad4 = {el1_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return new Element[]{el1_1, el1_2, el1_3, el1_4};


//        return new Element[]{el1_1, el1_2, el1_3, el1_4};
//        return new Element[]{el1_1, el1_4};
//         return new Element[] { el1_1 };
    }












    //---------------------------added: 15.08.2018-----------------------------


    public Element[] iron_yoke() {


        // include-file for the yoke of the MQ quadrupole magnet/Bernhard Auchmann - 01.08.00
        // referring to: LHCMQ_M_0004|0|B


        // KEYPOINTS

        Point kp0 = Point.ofCartesian(0, 0);
        Point kp0_1 = Point.ofCartesian(97e-3, 0);


//        kpbt1 = [beamd+rbt,0];
        Point kpbt1 = Point.ofCartesian(beamd+rbt,0);


        Point kpbt2 = Point.ofCartesian(beamd+(rbt+dbarbt)*Math.cos(phibt),(rbt-dbarbt)*Math.sin(phibt));
        Point kpbt3 = Point.ofCartesian(beamd+(rbt-dbarbt)*Math.cos(phibt),(rbt+dbarbt)*Math.sin(phibt));
        Point kpbt4 = Point.ofCartesian(beamd+(rbt-dbarbt)*Math.cos(phibt+Math.PI/2),(rbt+dbarbt)*Math.sin(phibt+Math.PI/2));
        Point kpbt5 = Point.ofCartesian(beamd+(rbt+dbarbt)*Math.cos(phibt+Math.PI/2),(rbt-dbarbt)*Math.sin(phibt+Math.PI/2));
        Point kpbt6 = Point.ofCartesian(beamd-rbt,0);

        Point kpyoke_1 = Point.ofCartesian(ryok,0);



        Point kpyoke_2 = Point.ofPolar(ryok, Math.PI/2-phibar-psicorn_1);
//       Point kpyoke_2 = Point.ofCartesian(ryok @ Pi/2-phibar-psicorn_1);



//       Point kpyoke_3 = [rcorn @ Pi/2-phibar-psicorn_1];
        Point kpyoke_3 = Point.ofPolar(rcorn, Math.PI/2-phibar-psicorn_1);

//       Point kpyoke_4 = [rcorn @ Pi/2-phibar-psibar];
        Point kpyoke_4 = Point.ofPolar(rcorn, Math.PI/2-phibar-psibar);

//       Point kpyoke_5 = [rcorn @ Pi/2-phibar+psibar];
        Point kpyoke_5 = Point.ofPolar(rcorn, Math.PI/2-phibar+psibar);


//       Point kpyoke_6 = [rcorn @ Pi/2-phibar+psicorn_2];
        Point kpyoke_6 = Point.ofPolar(rcorn, Math.PI/2-phibar+psicorn_2);


//       Point kpyoke_7 = [ryok @ Pi/2-phibar+psicorn_2];
        Point kpyoke_7 = Point.ofPolar(ryok, Math.PI/2-phibar+psicorn_2);


        Point kpyoke_8 = Point.ofCartesian(xar05,yar05);
        Point kpyoke_9 = Point.ofCartesian(0,ryok);
        Point kpyoke_10 = Point.ofCartesian(0,yar05);
        Point kpyoke_11 = Point.ofCartesian(0,yh1+rh1);
        Point kpyoke_11a = Point.ofCartesian(rh1,yh1);
        Point kpyoke_12 = Point.ofCartesian(0,yh1-rh1);
        Point kpyoke_13 = Point.ofCartesian(0,0);

        Point kph2_1 = Point.ofCartesian(xh2,yh2+rh2);
        Point kph2_2 = Point.ofCartesian(xh2,yh2-rh2);

        Point kph3_1 = Point.ofCartesian(xh3,yh3+rh3);
        Point kph3_2 = Point.ofCartesian(xh3,yh3-rh3);

        Point kpar05h_1 = Point.ofCartesian(xar05h_1,yar05h_1);
        Point kpar05h_2 = Point.ofCartesian(xar05h_2,yar05h_2);

//         Point kpbar_1 = [ryok @ Pi/4-psiscr];
        Point kpbar_1 = Point.ofPolar(ryok, Math.PI/4-psiscr);




//         Point kpbar_2 = [ryok @ Pi/4+psiscr];
        Point kpbar_2 = Point.ofPolar(ryok, Math.PI/4+psiscr);




//          -- LINES

//          lnyoke_1 = HyperLine(kpbar_1,kpyoke_1,"Arc",ryok);
        Arc lnyoke_1 = Arc.ofEndPointsCenter(kpbar_1, kpyoke_1, kp0);
//         Line  lnyoke_1 = Line.ofEndPoints(kpbar_1,kpyoke_1);




//          lnyoke_2 = HyperLine(kpbar_1,kpbar_2,"Bar",hscr);
        Arc lnyoke_2 = Arc.ofEndPointsCenter(kpbar_1, kpbar_2, kp0);
//          Line  lnyoke_2 = Line.ofEndPoints(kpbar_1,kpbar_2);


//          lnyoke_3 = HyperLine(kpyoke_2,kpbar_2,"Arc",ryok);
        Arc lnyoke_3 = Arc.ofEndPointsCenter(kpyoke_2, kpbar_2, kp0);
//         Line  lnyoke_3 = Line.ofEndPoints(kpyoke_2,kpbar_2);





        //          lnyoke_4 = Line(kpyoke_3,kpyoke_2);
        Line  lnyoke_4 = Line.ofEndPoints(kpyoke_3,kpyoke_2);



//          lnyoke_5 = HyperLine(kpyoke_4,kpyoke_3,"Arc",rcorn);
        Arc lnyoke_5 = Arc.ofEndPointsCenter(kpyoke_4, kpyoke_3, kp0);
//          Line  lnyoke_5 = Line.ofEndPoints(kpyoke_4,kpyoke_3);



//          lnyoke_6 = HyperLine(kpyoke_4,kpyoke_5,"Bar",hbar,0.65);
        Arc lnyoke_6 = Arc.ofEndPointsCenter(kpyoke_4, kpyoke_5, kp0);
//          Line  lnyoke_6 = Line.ofEndPoints(kpyoke_4,kpyoke_5);


//          lnyoke_7 = HyperLine(kpyoke_6,kpyoke_5,"Arc",rcorn);
        Arc lnyoke_7 = Arc.ofEndPointsCenter(kpyoke_6, kpyoke_5, kp0);
//         Line  lnyoke_7 = Line.ofEndPoints(kpyoke_6,kpyoke_5);


//          lnyoke_8 = Line(kpyoke_7,kpyoke_6);
        Line  lnyoke_8 = Line.ofEndPoints(kpyoke_7,kpyoke_6);



//          lnyoke_9 = HyperLine(kpyoke_8,kpyoke_7,"Arc",ryok);
        Arc lnyoke_9 = Arc.ofEndPointsCenter(kpyoke_8, kpyoke_7, kp0);
//           Line  lnyoke_9 = Line.ofEndPoints(kpyoke_8,kpyoke_7);


//          lnyoke_10 = Line(kpyoke_10,kpar05h_2,0.5);
        Line  lnyoke_10 = Line.ofEndPoints(kpyoke_10,kpar05h_2);


//          lnyoke_11 = Line(kpyoke_11,kpyoke_10);
        Line  lnyoke_11 = Line.ofEndPoints(kpyoke_11,kpyoke_10);

//          lnyoke_12 = HyperLine(kpyoke_11,kpyoke_12,"Arc",kpyoke_11a);
//          Arc lnyoke_12 = Arc.ofEndPointsCenter(kpyoke_11, kpyoke_12, kpyoke_11a);
        Line  lnyoke_12 = Line.ofEndPoints(kpyoke_11,kpyoke_12);


//          lnyoke_13 = Line(kpyoke_12,kpyoke_13,0.6);
        Line  lnyoke_13 = Line.ofEndPoints(kpyoke_12,kpyoke_13);


//          lnyoke_14 = Line(kpyoke_13,kpbt6);
        Line  lnyoke_14 = Line.ofEndPoints(kpyoke_13,kpbt6);


//          lnar05_1 = HyperLine(kpyoke_9,kpyoke_8,"Arc",ryok,0.5);
//          Arc lnar05_1 = Arc.ofEndPointsCenter(kpyoke_9, kpyoke_8, kp0);
        Line  lnar05_1 = Line.ofEndPoints(kpyoke_8,kpyoke_9);



//          lnar05_2 = Line(kpyoke_10,kpyoke_9);
        Line  lnar05_2 = Line.ofEndPoints(kpyoke_10,kpyoke_9);


//          lnar05h_1 = Line(kpyoke_8,kpar05h_1);
        Line  lnar05h_1 = Line.ofEndPoints(kpyoke_8,kpar05h_1);

//          lnar05h_2 = Line(kpar05h_2,kpar05h_1);
        Line  lnar05h_2 = Line.ofEndPoints(kpar05h_2,kpar05h_1);


//          lnbt1 = HyperLine(kpbt2,kpbt1,"Arc",rbt);
        Arc lnbt1 = Arc.ofEndPointsCenter(kpbt2, kpbt1, kp0_1);
//          Line  lnbt1 = Line.ofEndPoints(kpbt2,kpbt1);



//          lnbt2 = HyperLine(kpbt3,kpbt2,"Bar",hbarbt);
        Arc lnbt2 = Arc.ofEndPointsCenter(kpbt3, kpbt2, kp0_1);
//         Line  lnbt2 = Line.ofEndPoints(kpbt3,kpbt2);


//          lnbt3 = HyperLine(kpbt4,kpbt3,"Arc",rbt);
        Arc lnbt3 = Arc.ofEndPointsCenter(kpbt4, kpbt3, kp0_1);
//          Line  lnbt3 = Line.ofEndPoints(kpbt4,kpbt3);


//          lnbt4 = HyperLine(kpbt5,kpbt4,"Bar",hbarbt);
        Arc lnbt4 = Arc.ofEndPointsCenter(kpbt5, kpbt4, kp0_1);
//         Line  lnbt4 = Line.ofEndPoints(kpbt5,kpbt4);


//          lnbt5 = HyperLine(kpbt6,kpbt5,"Arc",rbt);
        Arc lnbt5 = Arc.ofEndPointsCenter(kpbt5, kpbt6, kp0_1);
//          Line  lnbt5 = Line.ofEndPoints(kpbt6,kpbt5);


//          lnyoke_15 = Line(kpbt1,kpyoke_1);
        Line  lnyoke_15 = Line.ofEndPoints(kpbt1,kpyoke_1);

//          lnh2 = HyperLine(kph2_1,kph2_2,"Circle");
        Line  lnh2 = Line.ofEndPoints(kph2_1,kph2_2);

//          lnh3 = HyperLine(kph3_1,kph3_2,"Circle");
        Line  lnh3 = Line.ofEndPoints(kph3_1,kph3_2);



//          -- AREAS

//          aryoke = HyperArea(lnyoke_1,lnyoke_2,lnyoke_3,lnyoke_4,lnyoke_5,lnyoke_6,lnyoke_7,lnyoke_8,lnyoke_9,lnar05h_1,lnar05h_2,lnyoke_10,lnyoke_11,lnyoke_12,lnyoke_13,lnyoke_14,lnbt5,lnbt4,lnbt3,lnbt2,lnbt1,lnyoke_15,BHiron2);
        Area aryoke = Area.ofHyperLines(new HyperLine[]{lnyoke_1,lnyoke_2,lnyoke_3,lnyoke_4,lnyoke_5,lnyoke_6,lnyoke_7,lnyoke_8,lnyoke_9,lnar05h_1,lnar05h_2,lnyoke_10,lnyoke_11,lnyoke_12,lnyoke_13,lnyoke_14,lnbt5,lnbt4,lnbt3,lnbt2,lnbt1,lnyoke_15});

//          ar05 = HyperArea(lnyoke_10,lnar05h_2,lnar05h_1,lnar05_1,lnar05_2,BHiron2);
        Area ar05 = Area.ofHyperLines(new HyperLine[]{lnyoke_10,lnar05h_2,lnar05h_1,lnar05_1,lnar05_2});


//          arh2 = HyperArea(lnh2,BH_air);
//          HyperHoleOf(arh2,aryoke);

//          arh3 = HyperArea(lnh3,BH_air);
//          HyperHoleOf(arh3,aryoke);



        Element el2_1 = new Element("IY_El1", aryoke);
        Element el2_2 = new Element("IY_El2", ar05);
//        Element el1_3 = new Element("IY_El3", aryoke_l);
//        Element el1_4 = new Element("IY_El4", aryoke_mid_l);

        Element[] quad1 = {el2_1};
        Element[] quad2 = {el2_2};
//        Element[] quad3 = {el1_3};
//        Element[] quad4 = {el1_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);

        return new Element[]{el2_1, el2_2};
//         return new Element[]{el2_2};


//        return new Element[]{el1_1, el1_2, el1_3, el1_4};
//        return new Element[]{el1_1, el1_4};
//         return new Element[] { el1_1 };
    }





    public Element[] holes_collar() {

        // POINTS
        Point kph2_1 = Point.ofCartesian(169.99725e-3, 28.90892e-3);
        Point kph2_2 = Point.ofCartesian(166.06724e-3, 37.33762e-3);


        Point kph3_1 = Point.ofCartesian(154.66489e-3, 53.42171e-3);
        Point kph3_2 = Point.ofCartesian(150.42189e-3, 57.66441e-3);


        Point kph4_1 = Point.ofCartesian(134.33790e-3, 69.06721e-3);
        Point kph4_2 = Point.ofCartesian(125.90890e-3, 72.99751e-3);

        Point kph5_1 = Point.ofCartesian(175.50001e-3, 2.5e-3);
        Point kph5_2 = Point.ofCartesian(183.96400e-3, 0);

        Point kph6_1 = Point.ofCartesian(99.5e-3, 78.46410e-3);
        Point kph6_2 = Point.ofCartesian(97e-3, 78.46410e-3);
        Point kph6_3 = Point.ofCartesian(97e-3, usbs);


        Point kp0 = Point.ofCartesian(97e-3, 0);

        Point kplsr1 = Point.ofCartesian(beamd+lsr,0); //erster Punkt bei 156,995 mm > passt
        Point kplsr2 = Point.ofCartesian(beamd+ls2x,ls2y);
        Point kplsr3 = Point.ofCartesian(beamd+ls3x,ls3y);
        Point kplsr4 = Point.ofCartesian(beamd+ls4x,ls4y);
        Point kplsr5 = Point.ofCartesian(beamd+ls5x,ls5y);

        Point kpusr1 = Point.ofCartesian(beamd+usr-usbh1,0);
        Point kpusr2 = Point.ofCartesian(beamd+usbs,usbw);


        Point kpusr3 = Point.ofCartesian(beamd+usr*Math.cos(usphi1-uspsi1),usr*Math.sin(usphi1-uspsi1));


        Point kpusr4 = Point.ofCartesian(beamd+usr*Math.cos(usphi1+uspsi1),usr*Math.sin(usphi1+uspsi1));
        Point kpusr5 = Point.ofCartesian(beamd+(usflaty+usflatx)*(Math.sqrt(2)/2),(usflaty-usflatx)*(Math.sqrt(2)/2));
        Point kpusr6 = Point.ofCartesian(beamd+(usflaty+usb2d)*(Math.sqrt(2)/2),(usflaty-usb2d)*(Math.sqrt(2)/2));

        Point kpusr7 = Point.ofCartesian(beamd+(usflaty-usb2d)*(Math.sqrt(2)/2),(usflaty+usb2d)*(Math.sqrt(2)/2));
        Point kpusr8 = Point.ofCartesian(beamd+(usflaty-usflatx)*(Math.sqrt(2)/2),(usflaty+usflatx)*(Math.sqrt(2)/2));
        Point kpusr9 = Point.ofCartesian(beamd+usr*Math.cos(usphi2-uspsi1),usr*Math.sin(usphi2-uspsi1));
        Point kpusr10 = Point.ofCartesian(beamd+usr*Math.cos(usphi2+uspsi1),usr*Math.sin(usphi2+uspsi1));
        Point kpusr11 = Point.ofCartesian(beamd+usbw, usbs);



        // LINES


        //-----------------------------------------


        Line     lnusr1 = Line.ofEndPoints(kplsr1,kpusr1);


        Line     lnusr2 = Line.ofEndPoints(kpusr1,kpusr2);

        //Line     lnusr3 = Line.ofEndPoints(kpusr3,kpusr2);
        Arc lnusr3 = Arc.ofEndPointsCenter(kpusr3, kpusr2, kp0);

        //Line     lnusr4 = Line.ofEndPoints(kpusr3,kpusr4);


        //Line     lnusr5 = Line.ofEndPoints(kpusr5,kpusr4);
        Arc lnusr5 = Arc.ofEndPointsCenter(kpusr5, kpusr4, kp0);


        Line     lnusr6 = Line.ofEndPoints(kpusr5,kpusr6);
        //Arc lnusr6 = Arc.ofEndPointsCenter(kpusr5, kpusr6, kp0);

        //Line     lnusr7 = Line.ofEndPoints(kpusr6,kpusr7);
        Arc lnusr7 = Arc.ofEndPointsCenter(kpusr6, kpusr7, kp0);

        Line     lnusr8 = Line.ofEndPoints(kpusr7,kpusr8);
        //Arc lnusr8 = Arc.ofEndPointsCenter(kpusr7, kpusr8, kp0);

        //Line     lnusr9 = Line.ofEndPoints(kpusr9,kpusr8);
        Arc lnusr9 = Arc.ofEndPointsCenter(kpusr9, kpusr8, kp0);

        //Line     lnusr10 = Line.ofEndPoints(kpusr9,kpusr10);
        Arc lnusr10 = Arc.ofEndPointsCenter(kpusr9, kpusr10, kp0);

        //Line     lnusr11 = Line.ofEndPoints(kpusr11,kpusr10);
        Arc lnusr11 = Arc.ofEndPointsCenter(kpusr11, kpusr10, kp0);

        //       Line     lnlsr1 = Line.ofEndPoints(kplsr2,kplsr1);




        Arc lnlsr1 = Arc.ofEndPointsCenter(kplsr2, kplsr1, kp0);

        //-----------------------------------------






// first hole
        Line     lnh2 = Line.ofEndPoints(kph2_1,kph2_2);
        Line     lnh5 = Line.ofEndPoints(kph2_1,kpusr3);
        Line     lnh6 = Line.ofEndPoints(kph2_2,kpusr4);
        //Line     lnh7 = Line.ofEndPoints(kpusr4,kpusr3);

        Arc lnh7 = Arc.ofEndPointsCenter(kpusr3, kpusr4, kp0);

// second hole

        Line     lnh8 = Line.ofEndPoints(kph3_1,kph3_2);
        Line     lnh9 = Line.ofEndPoints(kph3_1,kpusr6);
        Line     lnh10 = Line.ofEndPoints(kph3_2,kpusr7);
        //Line     lnh11 = Line.ofEndPoints(kpusr7,kpusr6);
        Arc lnh11 = Arc.ofEndPointsCenter(kpusr6, kpusr7, kp0);

// third hole

        Line     lnh12 = Line.ofEndPoints(kph4_1,kph4_2);
        Line     lnh13 = Line.ofEndPoints(kph4_1,kpusr9);
        Line     lnh14 = Line.ofEndPoints(kph4_2,kpusr10);
        //Line     lnh15 = Line.ofEndPoints(kpusr10,kpusr9);
        Arc lnh15 = Arc.ofEndPointsCenter(kpusr10, kpusr9, kp0);

// fourth hole

        Line     lnh16 = Line.ofEndPoints(kph5_1,kpusr1);
        Line     lnh17 = Line.ofEndPoints(kph5_1,kpusr2);
        Line     lnh18 = Line.ofEndPoints(kpusr2,kph5_2);
        Line     lnh19 = Line.ofEndPoints(kph5_2,kpusr1);

// fifth hole

        Line     lnh20 = Line.ofEndPoints(kph6_1,kph6_2);
        Line     lnh21 = Line.ofEndPoints(kph6_2,kph6_3);
        Line     lnh22 = Line.ofEndPoints(kph6_3,kpusr11);
        //Line     lnh23 = Line.ofEndPoints(kpusr11,kph6_1);
        Arc lnh23 = Arc.ofEndPointsCenter(kpusr11, kph6_1, kp0);



        //       Circumference lnh2 = Circumference.ofDiameterEndPoints(kph2_1, kph2_2);
        //       Circumference lnh3 = Circumference.ofDiameterEndPoints(kph3_1, kph3_2);
        //       Circumference lnh4 = Circumference.ofDiameterEndPoints(kph4_1, kph4_2);



        // AREAS FIRST QUADRANT
        Area arh1_1 = Area.ofHyperLines(new HyperLine[]{lnh2, lnh5, lnh6, lnh7 });
        Area arh2_1 = Area.ofHyperLines(new HyperLine[]{lnh8, lnh9, lnh10, lnh11 });
        Area arh3_1 = Area.ofHyperLines(new HyperLine[]{lnh12, lnh13, lnh14, lnh15});
        Area arh4_1 = Area.ofHyperLines(new HyperLine[]{lnh16, lnh17, lnh18, lnh19});
        Area arh5_1 = Area.ofHyperLines(new HyperLine[]{lnh20, lnh21, lnh22, lnh23});

        //mirroring of the holes

        Area arh1_2 = arh1_1.mirrorY().translate(2*97e-3,0);
        Area arh2_2 = arh2_1.mirrorY().translate(2*97e-3,0);
        Area arh3_2 = arh3_1.mirrorY().translate(2*97e-3,0);
        Area arh4_2 = arh4_1.mirrorY().translate(2*97e-3,0);
        Area arh5_2 = arh5_1.mirrorY().translate(2*97e-3,0);


        // AREAS OTHER QUADRANTS
        //       Area arh1_2 = arh1_1.mirrorY();
        //      Area arh1_3 = arh1_2.mirrorX();
        //      Area arh1_4 = arh1_1.mirrorX();

        //       Area arh2_2 = arh2_1.mirrorY();
        //       Area arh2_3 = arh2_2.mirrorX();
//        Area arh2_4 = arh2_1.mirrorX();

        //       Area arh3_2 = arh3_1.mirrorY();
        //       Area arh3_3 = arh3_2.mirrorX();
        //       Area arh3_4 = arh3_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element ho1_1 = new Element("C_HOLE1_1", arh1_1);
        Element ho2_1 = new Element("C_HOLE2_1", arh2_1);
        Element ho3_1 = new Element("C_HOLE3_1", arh3_1);
        Element ho4_1 = new Element("C_HOLE4_1", arh4_1);
//        Element ho5_1 = new Element("C_HOLE5_1", arh5_1);


        Element ho1_2 = new Element("C_HOLE1_2", arh1_2);
        Element ho2_2 = new Element("C_HOLE2_2", arh2_2);
        Element ho3_2 = new Element("C_HOLE3_2", arh3_2);
        Element ho4_2 = new Element("C_HOLE4_2", arh4_2);
//        Element ho5_2 = new Element("C_HOLE5_2", arh5_2);

        // ELEMENTS OTHER QUADRANTS
        //      Element ho1_2 = new Element("IY_HOLE1_2", arh1_2);
        //      Element ho1_3 = new Element("IY_HOLE1_3", arh1_3);
        //      Element ho1_4 = new Element("IY_HOLE1_4", arh1_4);

        //      Element ho2_2 = new Element("IY_HOLE2_2", arh2_2);
        //      Element ho2_3 = new Element("IY_HOLE2_3", arh2_3);
        //      Element ho2_4 = new Element("IY_HOLE2_4", arh2_4);

        //      Element ho3_2 = new Element("IY_HOLE3_2", arh3_2);
        //      Element ho3_3 = new Element("IY_HOLE3_3", arh3_3);
        //      Element ho3_4 = new Element("IY_HOLE3_4", arh3_4);

//        return new Element[]{
//                ho1_1, ho2_1, ho3_1,
//                ho1_2, ho2_2, ho3_2,
//                ho1_3, ho2_3, ho3_3,
//                ho1_4, ho2_4, ho3_4,
//        };

//        return new Element[]{
//                ho1_1, ho2_1, ho3_1,
//                ho1_4, ho2_4, ho3_4,
//        };

        // ELEMENTS DISTRIBUTED OVER QUADRANTS

        Element[] quad1 = {ho1_1, ho2_1, ho3_1, ho4_1, ho1_2, ho2_2, ho3_2, ho4_2};
//        Element[] quad1 = {ho1_1, ho2_1, ho3_1, ho4_1, ho5_1, ho1_2, ho2_2, ho3_2, ho4_2, ho5_2};


//        Element[] quad1 = {ho1_1, ho2_1};
//        Element[] quad2 = {ho1_2, ho2_2, ho3_2};
//        Element[] quad3 = {ho1_3, ho2_3, ho3_3};
//        Element[] quad4 = {ho1_4, ho2_4, ho3_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
//        return new Element[]{ho1_1, ho2_1, ho3_1, ho1_4, ho2_4, ho3_4};
//        return new Element[]{ho1_1, ho2_1, ho3_1, ho4_1, ho5_1, ho1_2, ho2_2, ho3_2, ho4_2, ho5_2};

        return new Element[]{ho1_1, ho2_1, ho3_1, ho4_1, ho1_2, ho2_2, ho3_2, ho4_2};


//         return new Element[] {ho1_1, ho2_1, ho3_1};
    }



//----------------------------added: 17.08.2018----------------------------------------

    public Element[] holes_yoke() {


        // POINTS


        // KEYPOINTS

        Point kp0 = Point.ofCartesian(0, 0);
        Point kp0_1 = Point.ofCartesian(97e-3, 0);


//      first hole
        //-----------------------------------------------------------------
        Point kpbar_1 = Point.ofPolar(ryok, Math.PI/4-psiscr);

        Point kpbar_2 = Point.ofPolar(ryok, Math.PI/4+psiscr);

        Point kpbar_1_1= Point.ofCartesian(153.34219e-3, 142.02818e-3);
        Point kpbar_2_1 = Point.ofCartesian(142.02818e-3, 153.34219e-3);

        //-----------------------------------------------------------------

//      second hole
        //-----------------------------------------------------------------
        Point kpyoke_4 = Point.ofPolar(rcorn, Math.PI/2-phibar-psibar);

        Point kpyoke_5 = Point.ofPolar(rcorn, Math.PI/2-phibar+psibar);

        Point kpyoke_4_1 = Point.ofCartesian(121.35904e-3, 159.19934e-3);
        Point kpyoke_5_1 = Point.ofCartesian(77.19155e-3, 184.69934e-3);

        //-----------------------------------------------------------------


        //      third hole
        //-----------------------------------------------------------------
        Point kpyoke_11 = Point.ofCartesian(0,yh1+rh1);
        Point kpyoke_11a = Point.ofCartesian(rh1,yh1);
        Point kpyoke_11b = Point.ofCartesian(0,yh1);
        Point kpyoke_12 = Point.ofCartesian(0,yh1-rh1);

        //-----------------------------------------------------------------


        //      fourth hole
        //-----------------------------------------------------------------

        Point kph2_1 = Point.ofCartesian(xh2,yh2+rh2);
        Point kph2_2 = Point.ofCartesian(xh2,yh2-rh2);

        //-----------------------------------------------------------------





        //      fifth hole
        //-----------------------------------------------------------------

        Point kph3_1 = Point.ofCartesian(xh3,yh3+rh3);
        Point kph3_2 = Point.ofCartesian(xh3,yh3-rh3);

        //-----------------------------------------------------------------


        //      sixth hole
        //-----------------------------------------------------------------

        Point kph6_1 = Point.ofCartesian(163.46800e-3,60.81120e-3);
        Point kph6_2 = Point.ofCartesian(168.41766e-3,65.76104e-3);
        Point kph6_3 = Point.ofCartesian(162.76066e-3,71.41784e-3);
        Point kph6_4 = Point.ofCartesian(157.81099e-3,66.46800e-3);
        //-----------------------------------------------------------------



        //      seventh hole
        //-----------------------------------------------------------------

        Point kph7_1 = Point.ofCartesian(36.1888e-3,66.468e-3);
        Point kph7_2 = Point.ofCartesian(31.23905e-3,71.41775e-3);
        Point kph7_3 = Point.ofCartesian(25.58225e-3,65.76095e-3);
        Point kph7_4 = Point.ofCartesian(30.53200e-3,60.81120e-3);
        //-----------------------------------------------------------------


//        Point kph3_1 = Point.ofCartesian(154.66489e-3, 53.42171e-3);
//        Point kph3_2 = Point.ofCartesian(150.42189e-3, 57.66441e-3);


        Point kph4_1 = Point.ofCartesian(134.33790e-3, 69.06721e-3);
        Point kph4_2 = Point.ofCartesian(125.90890e-3, 72.99751e-3);

        Point kph5_1 = Point.ofCartesian(175.50001e-3, 2.5e-3);
        Point kph5_2 = Point.ofCartesian(183.96400e-3, 0);



        Point kplsr1 = Point.ofCartesian(beamd+lsr,0); //erster Punkt bei 156,995 mm > passt
        Point kplsr2 = Point.ofCartesian(beamd+ls2x,ls2y);
        Point kplsr3 = Point.ofCartesian(beamd+ls3x,ls3y);
        Point kplsr4 = Point.ofCartesian(beamd+ls4x,ls4y);
        Point kplsr5 = Point.ofCartesian(beamd+ls5x,ls5y);

        Point kpusr1 = Point.ofCartesian(beamd+usr-usbh1,0);
        Point kpusr2 = Point.ofCartesian(beamd+usbs,usbw);


        Point kpusr3 = Point.ofCartesian(beamd+usr*Math.cos(usphi1-uspsi1),usr*Math.sin(usphi1-uspsi1));


        Point kpusr4 = Point.ofCartesian(beamd+usr*Math.cos(usphi1+uspsi1),usr*Math.sin(usphi1+uspsi1));
        Point kpusr5 = Point.ofCartesian(beamd+(usflaty+usflatx)*(Math.sqrt(2)/2),(usflaty-usflatx)*(Math.sqrt(2)/2));
        Point kpusr6 = Point.ofCartesian(beamd+(usflaty+usb2d)*(Math.sqrt(2)/2),(usflaty-usb2d)*(Math.sqrt(2)/2));

        Point kpusr7 = Point.ofCartesian(beamd+(usflaty-usb2d)*(Math.sqrt(2)/2),(usflaty+usb2d)*(Math.sqrt(2)/2));
        Point kpusr8 = Point.ofCartesian(beamd+(usflaty-usflatx)*(Math.sqrt(2)/2),(usflaty+usflatx)*(Math.sqrt(2)/2));
        Point kpusr9 = Point.ofCartesian(beamd+usr*Math.cos(usphi2-uspsi1),usr*Math.sin(usphi2-uspsi1));
        Point kpusr10 = Point.ofCartesian(beamd+usr*Math.cos(usphi2+uspsi1),usr*Math.sin(usphi2+uspsi1));
        Point kpusr11 = Point.ofCartesian(beamd+usbw, usbs);



        // LINES


        //first hole
        //-----------------------------------------------------------------

        Arc lnh_1 = Arc.ofEndPointsCenter(kpbar_1, kpbar_2, kp0);

        Line  lnh_2 = Line.ofEndPoints(kpbar_1,kpbar_1_1);
        Line  lnh_3 = Line.ofEndPoints(kpbar_1_1,kpbar_2_1);
        Line  lnh_4 = Line.ofEndPoints(kpbar_2_1,kpbar_2);

        //-----------------------------------------------------------------


        //second hole
        //-----------------------------------------------------------------
        Arc lnh_5 = Arc.ofEndPointsCenter(kpyoke_4, kpyoke_5, kp0);
        Line  lnh_6 = Line.ofEndPoints(kpyoke_4,kpyoke_4_1);
        Line  lnh_7 = Line.ofEndPoints(kpyoke_4_1,kpyoke_5_1);
        Line  lnh_8 = Line.ofEndPoints(kpyoke_5_1,kpyoke_5);

        //-----------------------------------------------------------------


        //third hole
        //-----------------------------------------------------------------

        Arc lnh_9 = Arc.ofEndPointsCenter(kpyoke_11, kpyoke_12, kpyoke_11b);
        Line  lnh_11 = Line.ofEndPoints(kpyoke_11,kpyoke_12);


        //-----------------------------------------------------------------

        //fourth hole
        //-----------------------------------------------------------------

        // Arc lnh_12 = Arc.ofEndPointsRadius(kph2_1, kph2_2, 0.5*(160.25001e-3-139.75e-3));

        Circumference c1 = Circumference.ofDiameterEndPoints(kph2_1,kph2_2);

        //-----------------------------------------------------------------



        //fifth hole
        //-----------------------------------------------------------------

        Circumference c2 = Circumference.ofDiameterEndPoints(kph3_1,kph3_2);

        //-----------------------------------------------------------------



        //sixth hole
        //-----------------------------------------------------------------

        Line  lnh_12 = Line.ofEndPoints(kph6_1,kph6_2);
        Line  lnh_13 = Line.ofEndPoints(kph6_2,kph6_3);
        Line  lnh_14 = Line.ofEndPoints(kph6_3,kph6_4);
        Arc lnh_15 = Arc.ofEndPointsCenter(kph6_4, kph6_1, kp0_1);


        //Line  lnh_15 = Line.ofEndPoints(kph6_4,kph6_1);

        //-----------------------------------------------------------------


        //sixth hole
        //-----------------------------------------------------------------

        Line  lnh_16 = Line.ofEndPoints(kph7_1,kph7_2);
        Line  lnh_17 = Line.ofEndPoints(kph7_2,kph7_3);
        Line  lnh_18 = Line.ofEndPoints(kph7_3,kph7_4);
        Arc lnh_19 = Arc.ofEndPointsCenter(kph7_4, kph7_1, kp0_1);

        //Line  lnh_19 = Line.ofEndPoints(kph7_4,kph7_1);

        //-----------------------------------------------------------------



        //-----------------------------------------------------------------
        // Holes FIRST QUADRANT
        Area arh1_1 = Area.ofHyperLines(new HyperLine[]{lnh_1, lnh_2, lnh_3, lnh_4 });
        Area arh2_1 = Area.ofHyperLines(new HyperLine[]{lnh_5, lnh_6, lnh_7, lnh_8 });
        Area arh3_1 = Area.ofHyperLines(new HyperLine[]{lnh_9, lnh_11 });
        Area arh4_1 = Area.ofHyperLines(new HyperLine[]{c1});
        Area arh5_1 = Area.ofHyperLines(new HyperLine[]{c2});
        Area arh6_1 = Area.ofHyperLines(new HyperLine[]{lnh_12, lnh_13, lnh_14, lnh_15});
        Area arh7_1 = Area.ofHyperLines(new HyperLine[]{lnh_16, lnh_17, lnh_18, lnh_19});
        //-----------------------------------------------------------------



        //mirroring of the holes

        Area arh1_2 = arh1_1.mirrorY().translate(2*97e-3,0);
        Area arh2_2 = arh2_1.mirrorY().translate(2*97e-3,0);
        Area arh3_2 = arh3_1.mirrorY().translate(2*97e-3,0);
        Area arh4_2 = arh4_1.mirrorY().translate(2*97e-3,0);



        // AREAS OTHER QUADRANTS
        //       Area arh1_2 = arh1_1.mirrorY();
        //      Area arh1_3 = arh1_2.mirrorX();
        //      Area arh1_4 = arh1_1.mirrorX();

        //       Area arh2_2 = arh2_1.mirrorY();
        //       Area arh2_3 = arh2_2.mirrorX();
//        Area arh2_4 = arh2_1.mirrorX();

        //       Area arh3_2 = arh3_1.mirrorY();
        //       Area arh3_3 = arh3_2.mirrorX();
        //       Area arh3_4 = arh3_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element ho1_1 = new Element("IY_HOLE1_1", arh1_1);
        Element ho2_1 = new Element("IY_HOLE2_1", arh2_1);
        Element ho3_1 = new Element("IY_HOLE3_1", arh3_1);
        Element ho4_1 = new Element("IY_HOLE4_1", arh4_1);
        Element ho5_1 = new Element("IY_HOLE5_1", arh5_1);
        Element ho6_1 = new Element("IY_HOLE6_1", arh6_1);
        Element ho7_1 = new Element("IY_HOLE7_1", arh7_1);


//        Element ho1_2 = new Element("IY_HOLE1_2", arh1_2);
        //       Element ho2_2 = new Element("IY_HOLE2_2", arh2_2);
        //       Element ho3_2 = new Element("IY_HOLE3_2", arh3_2);
        //       Element ho4_2 = new Element("IY_HOLE4_2", arh4_2);
        //Element ho5_2 = new Element("IY_HOLE5_2", arh5_2);

        // ELEMENTS OTHER QUADRANTS
        //      Element ho1_2 = new Element("IY_HOLE1_2", arh1_2);
        //      Element ho1_3 = new Element("IY_HOLE1_3", arh1_3);
        //      Element ho1_4 = new Element("IY_HOLE1_4", arh1_4);

        //      Element ho2_2 = new Element("IY_HOLE2_2", arh2_2);
        //      Element ho2_3 = new Element("IY_HOLE2_3", arh2_3);
        //      Element ho2_4 = new Element("IY_HOLE2_4", arh2_4);

        //      Element ho3_2 = new Element("IY_HOLE3_2", arh3_2);
        //      Element ho3_3 = new Element("IY_HOLE3_3", arh3_3);
        //      Element ho3_4 = new Element("IY_HOLE3_4", arh3_4);

//        return new Element[]{
//                ho1_1, ho2_1, ho3_1,
//                ho1_2, ho2_2, ho3_2,
//                ho1_3, ho2_3, ho3_3,
//                ho1_4, ho2_4, ho3_4,
//        };

//        return new Element[]{
//                ho1_1, ho2_1, ho3_1,
//                ho1_4, ho2_4, ho3_4,
//        };

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {ho1_1, ho2_1, ho3_1, ho4_1, ho5_1, ho6_1, ho7_1};
//        Element[] quad1 = {ho1_1, ho2_1, ho3_1, ho4_1, ho1_2, ho2_2, ho3_2, ho4_2};
//        Element[] quad1 = {ho1_1, ho2_1};
//        Element[] quad2 = {ho1_2, ho2_2, ho3_2};
//        Element[] quad3 = {ho1_3, ho2_3, ho3_3};
//        Element[] quad4 = {ho1_4, ho2_4, ho3_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
//        return new Element[]{ho1_1, ho2_1, ho3_1, ho1_4, ho2_4, ho3_4};
        return new Element[]{ho1_1, ho2_1, ho3_1, ho4_1, ho5_1, ho6_1, ho7_1};
//        return new Element[]{ho1_1, ho2_1, ho3_1, ho4_1, ho1_2, ho2_2, ho3_2, ho4_2};

//         return new Element[] {ho1_1, ho2_1, ho3_1};
    }




    public Element[] holes_insert() {

        // POINTS
        Point kph1_1 = Point.ofCartesian(lh1x, lh1y + lh1r);
        Point kph1_2 = Point.ofCartesian(lh1x, lh1y - lh1r);

        // LINES
        Circumference lnh1 = Circumference.ofDiameterEndPoints(kph1_1, kph1_2);

        // AREAS FIRST QUADRANT
        Area arh_1 = Area.ofHyperLines(new HyperLine[]{lnh1});

        // AREAS OTHER QUADRANTS
        Area arh_2 = arh_1.mirrorY();
        Area arh_3 = arh_2.mirrorX();
        Area arh_4 = arh_1.mirrorX();

        // ELEMENTS FIRST QUADRANT
        Element ho_1 = new Element("II_HOLE1_1", arh_1);

        // ELEMENTS OTHER QUADRANTS
        Element ho_2 = new Element("II_HOLE1_2", arh_2);
        Element ho_3 = new Element("II_HOLE1_3", arh_3);
        Element ho_4 = new Element("II_HOLE1_4", arh_4);

        // ELEMENTS DISTRIBUTED OVER QUADRANTS
        Element[] quad1 = {ho_1};
        Element[] quad2 = {ho_2};
        Element[] quad3 = {ho_3};
        Element[] quad4 = {ho_4};

//            Element[] elementsToBuild = quadrantsToBuild(quad1, quad2, quad3, quad4);
        return new Element[]{ho_1, ho_4};


//        return new Element[]{ho_1, ho_2, ho_3, ho_4};
//        return new Element[]{ho_1, ho_4};
//        return new Element[]{ho_1};
    }




    public Element[] BoundaryConditions(){
        double length = 10;
        double eps  = 1e-4;

        //POINTS
        Point kpx1 = Point.ofCartesian(-length, 0-eps);
        Point kpx2 = Point.ofCartesian(length, 0-eps);
        Point kpx3 = Point.ofCartesian(length, 0+eps);
        Point kpx4 = Point.ofCartesian(-length, 0+eps);

        Point kpy1 = Point.ofCartesian(0-eps, -length);
        Point kpy2 = Point.ofCartesian(0+eps, -length);
        Point kpy3 = Point.ofCartesian(0+eps, length);
        Point kpy4 = Point.ofCartesian(0-eps, length);

        // LINES
        Line lnx1 = Line.ofEndPoints(kpx2, kpx1);
        Line lnx2 = Line.ofEndPoints(kpx2, kpx3);
        Line lnx3 = Line.ofEndPoints(kpx3, kpx4);
        Line lnx4 = Line.ofEndPoints(kpx1, kpx4);

        Line lny1 = Line.ofEndPoints(kpy2, kpy1);
        Line lny2 = Line.ofEndPoints(kpy2, kpy3);
        Line lny3 = Line.ofEndPoints(kpy3, kpy4);
        Line lny4 = Line.ofEndPoints(kpy1, kpy4);

        // AREAS
        Area xBC_Sel = Area.ofHyperLines(new HyperLine[]{lnx1, lnx2, lnx3, lnx4});
        Area yBC_Sel = Area.ofHyperLines(new HyperLine[]{lny1, lny2, lny3, lny4});

        // ELEMENTS
        Element xBC = new Element("xBC", xBC_Sel);
        Element yBC = new Element("yBC", yBC_Sel);

        return new Element[]{xBC, yBC};
    }

}
