# Working with steam-sigma-input project
The goal of this documentation is to provide a brief introduction on how to setup and execute a model input with the STEAM-SIGMA project.
### 0. Install necessary software
Below please find a sequence of steps needed to install software required to run STEAM-SIGMA. Please note that these steps are dedicated to the Microsoft Windows operating system; only tested for Windows 7.
We suggest to create a directory (e.g. C:/steam-sigma) to store the necessary files.
- (Required) Download a steam-sigma JAR (Java Archive) from: https://cernbox.cern.ch/index.php/s/vPtWPG7ToaB05eK We suggest to put the jar inside as C:/steam-sigma/steam-sigma.jar

- (Required) Download a material database libary (MaterialLibrary.zip) from: https://cernbox.cern.ch/index.php/s/HTyShIDGWfigb31 We suggest to unzip the file into the steam-sigma directory as C:/steam-sigma/MaterialLibrary/V0.1
<i>Please note that we are providing the material properties as a compiled C code in the form of dlls (dynamically linked libraries). In case of any errors related to the material library, please contact us as the dlls might need to be recompiled in order to suit the architecture of your PC.</i>

- (Required) Download and install latest Java SDK (Software Development Kit) from: https://www.oracle.com/technetwork/java/javase/downloads/index.html

- (Required) Download and install a Java IDE (Integrated Development Environment). We do recommend the use of IntelliJ IDEA from JET BRAINS:
https://www.jetbrains.com/idea/download/#section=mac

- (Suggested) In order to conveniently synchronise local copy of the project with the remote repository, the use of git is suggested:
https://git-scm.com/download/win

IntelliJ is integrated with git, for more details please refer to https://www.jetbrains.com/help/idea/using-git-integration.html 

### 1. Getting the steam-sigma-input project

- Download code from: https://gitlab.cern.ch/steam/steam-sigma-input by clicking the icon ![](images/download_logo.PNG) and put to the directory C:/steam-sigma as C:/steam-sigma/steam-sigma-input

- Or clone project from the gitlab repository in order to maintain the link wit the repository and allow for a seamless synchronization with the gitlab repository
  - Open Git bash
  - Go to the folder created in point 0, where the project shall be cloned
  - Execute the following command
  ```
  git clone https://gitlab.cern.ch/steam/steam-sigma-input
  ```

### 2. Opening the steam-sigma-input project with IntelliJ

In order to open a new project from existing sources we follow the documentation given in
https://www.jetbrains.com/help/pycharm/importing-project-from-existing-source-code.html

Figure below shows the project view in the IntelliJ
![](images/opened_project.PNG)


### 3. Loading the steam-sigma dependency into the steam-sigma-input project with IntelliJ

The steam-sigma project is provided as a jar file downloaded in point 1. The jar has to be imported to the project for the code to work. The process of loading a jar with IntelliJ is discussed in detail here: https://www.jetbrains.com/help/idea/library.html

1. Go to File -> Project structure
2. Then select Libraries under Project Settings
3. Click green plus button and under New Project Library select Java
4. From the opened window choose the steam-sigma.jar file downloaded in the first point of the documentation and click OK.
5. Select all modules and click OK and then OK.

### 4. Running the steam-sigma-input project
Make sure that SDK is selected for the project: https://www.jetbrains.com/help/idea/configuring-intellij-platform-plugin-sdk.html
Go to src/main/resources and open steam-sigma-config.json

```
{
  "COMSOLBatchPath": "C:\\Program Files\\COMSOL\\COMSOL53a\\Multiphysics\\bin\\win64\\",
  "ExternalCFunLibPath": "U:\\STEAM\\MaterialsLibrary",
  "OutputModelPath": "C:\\Output\\D2.mph"
}
```

Table below summarizes the description of the config

Field | Description
---------- | -----------
COMSOLBatchPath | an absolute path to the directory with COMSOL batch files. The config above contains the default COMSOL installation directory.
ExternalCFunLibPath | an absolute path to a directory with dlls containing the C functions with material properties used by COMSOL models. <i>Please refer to the directory created in point 1. The default location should be "C:\\steam-sigma\\MaterialsLibrary"</i>
OutputModelPath | An absolute path to the COMSOL model file to be created with the STEAM-SIGMA project. If the output directory does not exist, it will be created (cf code below).

The project source code is organised as follows:
- src
  - main
    - java
      - input
      - main
        - Main.java
    - resources
      - steam-sigma-config.json

Package input contains all the available classes with input cable parameters, magnet geometry, etc. A particular model to be created is defined in the Main.java file. In the code below a T0 magnet is being constructed (cf line Magnet_T0 magnet = new Magnet_T0(); )

```
import comsol.MagnetMPH;
import config.ConfigSigma;
import input.Others.T0.Magnet_T0;
import model.domains.Domain;
import server.SigmaServer;
import server.TxtSigmaServer;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        // LVL 1 ******************************************************************************
        // Parse config file
        ConfigSigma cfg = ConfigSigma.parseFileJSON(args[0]);

        // Create output directory if not exsisting
        String parentDirectory = new File(cfg.getOutputModelPath()).getParent();
        if (!new File(parentDirectory).isDirectory()) {
            new File(parentDirectory).mkdir();
        }

        // Magnet construction in Java
        Magnet_T0 magnet = new Magnet_T0();

        // LVL 2 ******************************************************************************
        // Magnet construction in COMSOL
        SigmaServer srv = new TxtSigmaServer(cfg.getOutputModelPath(), cfg.getComsolBatchPath());
        Domain[] domains = magnet.getDomains();
        srv.connect(cfg.getComsolBatchPath());
        MagnetMPH m = new MagnetMPH(cfg, srv);
        m.buildMPH(domains);
        srv.disconnect();
    }
}

```

For the list of available input files, please browse src/main/java/input/... For instance, to create an FCC cos-theta dipole magnet please substitute line
```
 Magnet_T0 magnet = new Magnet_T0();
```
with
```
 Magnet_FCC_cosTheta_v22b_37_optd7f8_SA magnet = new Magnet_FCC_cosTheta_v22b_37_optd7f8_SA();
```
After updating the steam-sigma-config.json with your local settings and selecting the model to be constructed, the project is ready to be executed.

1. To this end, in the main menu select Run and then Edit Configurations...
2. In a prompt window press a green + in the top left corner and choose Application.
3. Select
  - Set Name: Main
  - To find the Main class please type main in the search window (appears after clicking ... button on the right): main
  - Program arguments: full path to your steam-sigma-config.json in between "", i.e., "C:\steam-sigma\steam-sigma-input\src\main\resources\steam-sigma-config.json":
  - Use classpath of module: steam-sigma-input_main

The configuration window should look as below:
![](images/edit_configuration.PNG)
  - Select OK
  
To execute the code, please select from the main menu Run and then Run 'Main'. The code shall be executed and the model created.

For the D2 magnet, the expected output looks as follows:
```
COMSOL: Initialize model nodes
COMSOL: Initialize model global definitions
COMSOL: Initialize model domains
COMSOL: Build domain airDomain
COMSOL: Build domain airFarFieldDomain
COMSOL: Build domain C0
COMSOL: Build domain ironYoke
COMSOL: Build CLIQ equations
COMSOL: Build default mesh
COMSOL: Save model
COMSOL: Compiling C:\Output\T0.java into C:\Output\T0.class
COMSOL: Opening C:\Output\T0.class and saving as C:\Output\T0.mph
*******************************************
***COMSOL 5.3.1.201 progress output file***
*******************************************
Wed Aug 29 11:36:19 CEST 2018
COMSOL Multiphysics 5.3a (Build: 201) starting in batch mode
Running: C:\Output\T0.class
---------- Current Progress: 100 % - Updating selections
Memory: 391/406 708/721
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 744/747 983/986
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
           Current Progress:   0 % - AIR_El1
Memory: 754/754 993/993
           Current Progress:   7 % - Resolving domains
Memory: 752/754 990/993
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 722/754 961/993
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
           Current Progress:   7 % - AIR_El2
Memory: 737/754 975/993
-          Current Progress:  14 % - Resolving domains
Memory: 738/754 977/993
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 742/754 979/993
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
-          Current Progress:  14 % - AIR_El3
Memory: 743/754 980/993
--         Current Progress:  21 % - Resolving domains
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 746/754 984/993
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
--         Current Progress:  22 % - AIR_El4
Memory: 756/756 995/995
--         Current Progress:  28 % - Resolving domains
Memory: 757/757 995/995
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 758/760 996/998
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
--         Current Progress:  29 % - FAR_El1
Memory: 757/760 995/998
---        Current Progress:  36 % - Resolving domains
Memory: 758/760 996/998
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 757/760 995/998
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---        Current Progress:  36 % - FAR_El2
Memory: 766/766 1003/1003
----       Current Progress:  43 % - Resolving domains
Memory: 765/766 1003/1003
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 767/768 1005/1005
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
----       Current Progress:  43 % - FAR_El3
Memory: 774/776 1012/1014
-----      Current Progress:  50 % - Resolving domains
Memory: 776/777 1014/1016
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
---------- Current Progress: 100 % - Compiling equations
Memory: 775/777 1014/1016
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
Running on 2 x Intel64 Family 6 Model 79 Stepping 1, GenuineIntel.
Using 2 sockets with 16 cores in total on pcte224462.
Available memory: 130.99 GB.
-----      Current Progress:  51 % - FAR_El4
Memory: 692/777 932/1016
-----      Current Progress:  57 % - Resolving domains
Memory: 694/777 934/1016
-----      Current Progress:  58 % - C0_P0_W0_B0_HT0_WB
Memory: 695/777 935/1016
-----      Current Progress:  59 % - C0_P0_W0_B0_HT0_NB
------     Current Progress:  61 % - C0_P0_W0_B1_HT1_WB
Memory: 696/777 935/1016
------     Current Progress:  62 % - C0_P0_W0_B1_HT1_NB
Memory: 697/777 937/1016
------     Current Progress:  64 % - C0_P0_W1_B0_HT2_WB
Memory: 696/777 935/1016
------     Current Progress:  65 % - C0_P0_W1_B0_HT2_NB
Memory: 697/777 937/1016
------     Current Progress:  67 % - C0_P0_W1_B1_HT3_WB
Memory: 699/777 938/1016
------     Current Progress:  68 % - C0_P0_W1_B1_HT3_NB
Memory: 701/777 941/1016
-------    Current Progress:  70 % - C0_P0_W2_B0_HT4_WB
Memory: 702/777 943/1016
-------    Current Progress:  71 % - C0_P0_W2_B0_HT4_NB
-------    Current Progress:  72 % - C0_P0_W2_B1_HT5_WB
Memory: 705/777 945/1016
-------    Current Progress:  74 % - C0_P0_W2_B1_HT5_NB
           Current Progress:   0 % - C0_P0_W0
Memory: 707/777 947/1016
---------  Current Progress:  92 % - Resolving domains
           Current Progress:   0 % - C0_P0_W1
Memory: 709/777 949/1016
---------  Current Progress:  92 % - Resolving domains
           Current Progress:   0 % - C0_P0_W2
Memory: 714/777 955/1016
---------  Current Progress:  92 % - Resolving domains
Memory: 715/777 956/1016
           Current Progress:   0 % - C0_P1_W0_B0_HT6
Memory: 732/777 971/1016
           Current Progress:   3 % - C0_P1_W0_B0_HT6_WB
Memory: 733/777 972/1016
-          Current Progress:  10 % - C0_P1_W0_B0_HT6_NB
Memory: 733/777 973/1016
-          Current Progress:  17 % - C0_P1_W0_B1_HT7
--         Current Progress:  20 % - C0_P1_W0_B1_HT7_WB
Memory: 734/777 974/1016
--         Current Progress:  27 % - C0_P1_W0_B1_HT7_NB
---        Current Progress:  33 % - C0_P1_W1_B0_HT8
Memory: 735/777 975/1016
---        Current Progress:  37 % - C0_P1_W1_B0_HT8_WB
----       Current Progress:  43 % - C0_P1_W1_B0_HT8_NB
-----      Current Progress:  50 % - C0_P1_W1_B1_HT9
-----      Current Progress:  53 % - C0_P1_W1_B1_HT9_WB
Memory: 737/777 977/1016
------     Current Progress:  60 % - C0_P1_W1_B1_HT9_NB
Memory: 739/777 979/1016
------     Current Progress:  67 % - C0_P1_W2_B0_HT10
Memory: 736/777 979/1016
-------    Current Progress:  70 % - C0_P1_W2_B0_HT10_WB
Memory: 739/777 979/1016
-------    Current Progress:  77 % - C0_P1_W2_B0_HT10_NB
--------   Current Progress:  83 % - C0_P1_W2_B1_HT11
--------   Current Progress:  87 % - C0_P1_W2_B1_HT11_WB
---------  Current Progress:  93 % - C0_P1_W2_B1_HT11_NB
           Current Progress:   0 % - C0_P1_W0
Memory: 739/777 980/1016
---------  Current Progress:  92 % - Resolving domains
           Current Progress:   0 % - C0_P1_W1
Memory: 740/777 980/1016
---------  Current Progress:  92 % - Resolving domains
           Current Progress:   0 % - C0_P1_W2
---------  Current Progress:  92 % - Resolving domains
           Current Progress:   0 % - C0
---------  Current Progress:  92 % - Resolving domains
           Current Progress:   0 % - IY_El1_hl1
Memory: 932/932 1171/1171
           Current Progress:   2 % - Compiling equations
Memory: 936/938 1174/1176
           Current Progress:   2 % - IY_El1_hl2
Memory: 934/938 1172/1176
           Current Progress:   3 % - Compiling equations
           Current Progress:   3 % - IY_El1_hl3
           Current Progress:   5 % - Compiling equations
           Current Progress:   5 % - IY_El1_hl4
           Current Progress:   6 % - Compiling equations
           Current Progress:   6 % - IY_El1
--         Current Progress:  20 % - Resolving domains
--         Current Progress:  21 % - IY_El2_hl1
--         Current Progress:  23 % - Compiling equations
Memory: 935/938 1173/1176
--         Current Progress:  23 % - IY_El2_hl2
--         Current Progress:  24 % - Compiling equations
--         Current Progress:  24 % - IY_El2_hl3
--         Current Progress:  26 % - Compiling equations
--         Current Progress:  26 % - IY_El2_hl4
--         Current Progress:  27 % - Compiling equations
Memory: 940/940 1177/1177
--         Current Progress:  27 % - IY_El2
----       Current Progress:  41 % - Resolving domains
----       Current Progress:  42 % - IY_El3_hl1
----       Current Progress:  44 % - Compiling equations
Memory: 940/940 1178/1178
----       Current Progress:  44 % - IY_El3_hl2
----       Current Progress:  45 % - Compiling equations
Memory: 943/943 1180/1180
----       Current Progress:  45 % - IY_El3_hl3
----       Current Progress:  47 % - Compiling equations
Memory: 939/943 1177/1180
----       Current Progress:  47 % - IY_El3_hl4
Memory: 940/943 1177/1180
----       Current Progress:  48 % - Compiling equations
Memory: 943/943 1181/1181
----       Current Progress:  48 % - IY_El3
------     Current Progress:  62 % - Resolving domains
------     Current Progress:  64 % - IY_El4_hl1
------     Current Progress:  65 % - Compiling equations
------     Current Progress:  65 % - IY_El4_hl2
------     Current Progress:  67 % - Compiling equations
Memory: 945/945 1183/1183
------     Current Progress:  67 % - IY_El4_hl3
------     Current Progress:  68 % - Compiling equations
------     Current Progress:  68 % - IY_El4_hl4
-------    Current Progress:  70 % - Compiling equations
Memory: 907/945 1154/1185
-------    Current Progress:  70 % - IY_El4
Memory: 908/945 1154/1185
--------   Current Progress:  84 % - Resolving domains
Memory: 911/945 1157/1185
--------   Current Progress:  85 % - Form Union
Memory: 909/945 1157/1185
---------  Current Progress:  99 % - Resolving domains
Memory: 926/945 1170/1185
---------- Current Progress: 100 % - Updating selections
           Current Progress:   0 % - Free triangular
Memory: 919/945 1143/1185
Number of vertex elements: 121
Number of boundary elements: 868
Number of elements: 5320
Minimum element quality: 0.4871
-----      Current Progress:  59 % - Finalizing mesh
Memory: 923/945 1145/1185
-----      Current Progress:  59 % - Mapped
Number of vertex elements: 125
Number of boundary elements: 944
Number of elements: 5672
Minimum element quality: 0.4871
---------- Current Progress: 100 % - Creating mapped mesh
Memory: 928/945 1150/1185
Class run time: 13 s.
Saving model: C:\Output\T0_Model.mph
Save time: 0 s.
Total time: 13 s.
---------- Current Progress: 100 % - Done
Memory: 933/945 1155/1185
Renaming file: From T0_Model.mph to T0.mph
COMSOL: Done!
Elapsed time 21.579 [s]

```

A detailed description on how to run a project is available under the following link: https://www.jetbrains.com/help/idea/running-applications.html

### 5. Table with verification of model inputs
